# Script for calculating norms and bio data 
# 
# Author: Jasmine Benn�hr
###############################################################################

getwd()
setwd("C:\\#workspace\\R")

library(reshape)
#library(gridExtra)

# read-in original data, set path and filename
library(readxl)
x <- read_excel("//192.168.11.2/cut-e/science & research/Normen/new norms/UAE_2017/verbal + numerical admin_20170208dca/numerical admin UAENG.xlsx")

x <-read.csv(file=paste(".\\_data\\allProjects_LLi.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
x <-read.csv(file=paste(".\\_data\\numerical_admin_MEEL.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
x <-read.csv(file=paste(".\\_data\\numerical_admin_MES.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
x <-read.csv(file=paste(".\\_data\\verbal_admin_MEEL.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
x <-read.csv(file=paste(".\\_data\\verbal_admin_MES.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
x <-read.csv(file=paste(".\\_data\\original.csv"), header=TRUE, sep=";", quote="", dec=",", comment.char="" )
names(x)

### pivot table

pivot_prep <- cbind(as.numeric(x$Part_ID),as.character(x$Task_FieldName),x$Task_FieldValue)

### pivot table
# pivot table: score decimal is taken as the value for score_name
pivot_prep <- cbind(as.numeric(x$Part_ID),as.character(x$score_name),x$score_decimal)
colnames(pivot_prep)=c("Part_ID","score_name","value")
pivot_prep <- as.data.frame(pivot_prep)

attach(pivot_prep)

class(Part_ID)
class(x$Part_ID)
class(score_name)
class(value)

pivot <- cast(pivot_prep, Part_ID ~ score_name)
detach(pivot_prep)
attach(pivot)
names(pivot)
###

## write/ save xlsx-files begin###############################################
#getwd() #make sure you have the correct directory

#first make function usable by executing
save.xlsx <- function (file, ...)
{
	require(xlsx, quietly = TRUE)
	objects <- list(...)
	fargs <- as.list(match.call(expand.dots = TRUE))
	objnames <- as.character(fargs)[-c(1, 2)]
	nobjects <- length(objects)
	for (i in 1:nobjects) {
		if (i == 1)
			write.xlsx(objects[[i]], file, sheetName = objnames[i], row.names=FALSE, showNA=FALSE)
		else write.xlsx(objects[[i]], file, sheetName = objnames[i], row.names=FALSE,
					append = TRUE, showNA=FALSE)
	}
	print(paste("Workbook", file, "has", nobjects, "worksheets."))
}
#################################################################		

# colours

c.blue="#1D3061"

# bio data general

# age
date <- Sys.Date()
current_year <- 1900 + as.POSIXlt(date)$year 
current_year <- current_year-1

bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
		"languages", "country")

# education time
EduTime_id <- ""
EduTime_id[1] <- "1 year"
EduTime_id[2] <- "2 years"
EduTime_id[3] <- "3 years"
EduTime_id[4] <- "4 years"
EduTime_id[5] <- "5 years"
EduTime_id[6] <- "6 years"
EduTime_id[7] <- "7 years"
EduTime_id[8] <- "8 years"
EduTime_id[9] <- "9 years"
EduTime_id[10] <- "10 years"

# experience time
ExpTime_id <- ""
ExpTime_id[1] <- "no relevant experience"
ExpTime_id[2] <- "casual and vacation work only"
ExpTime_id[3] <- "<1 year"
ExpTime_id[4] <- "1-2 years"
ExpTime_id[5] <- "3-5 years"
ExpTime_id[6] <- "6-10 years"
ExpTime_id[7] <- ">10 years"

# management experience
ExpManage_id <- ""
ExpManage_id[1] <- "none"
ExpManage_id[2] <- "<2 years"		
ExpManage_id[3] <- "2-4 years"
ExpManage_id[4] <- "5 years or more"

# experience area
ExpArea_id <- ""
#_id[0] <- "- Please select -"
ExpArea_id[1] <- "Call Center"
ExpArea_id[2] <- "Catering"
ExpArea_id[3] <- "Construction"
ExpArea_id[4] <- "Consulting"
ExpArea_id[5] <- "Education"
ExpArea_id[6] <- "Finances, Banking & Insurance"
ExpArea_id[7] <- "Consumer goods"
ExpArea_id[8] <- "Government and Public Sector"
ExpArea_id[9] <- "Health and Social work"
ExpArea_id[10] <- "Internet/New technologies"
ExpArea_id[11] <- "Manufacturing"
ExpArea_id[12] <- "Marketing"
ExpArea_id[13] <- "Tourism"
ExpArea_id[14] <- "Retail"
ExpArea_id[15] <- "Sales"
ExpArea_id[16] <- "Science & Research"
ExpArea_id[17] <- "Telecommunication"
ExpArea_id[18] <- "Transport & Logistics"
ExpArea_id[19] <- "Other"
ExpArea_id[20] <- "Media and Advertising"
ExpArea_id[21] <- "Services"
ExpArea_id[22] <- "Oil & Gas"

# language
Lang_id <- ""
#_id[0] <- "- Please select -"
Lang_id[1] <- "German"
Lang_id[2] <- "English"
Lang_id[3] <- "French"
Lang_id[4] <- "Italian"
Lang_id[5] <- "Turkish"
Lang_id[6] <- "Norwegian"
Lang_id[7] <- "Romanian"
Lang_id[8] <- "Dutch"
Lang_id[9] <- "Spanish"
Lang_id[10] <- "Portuguese"
Lang_id[11] <- "Swedish"
Lang_id[12] <- "Japanese"
Lang_id[13] <- "Polish"
Lang_id[14] <- "Czech"
Lang_id[15] <- "Russian"
Lang_id[16] <- "Danish"
Lang_id[17] <- "Finnish"
Lang_id[18] <- "Greek"
Lang_id[19] <- "Simpl. Chinese"
Lang_id[20] <- "Trad. Chinese"
Lang_id[21] <- "Arabic"
Lang_id[22] <- "Slovenian"
Lang_id[23] <- "Hungarian"
Lang_id[24] <- "Serbian"
Lang_id[25] <- "Croatian"
Lang_id[26] <- "Catalan"
Lang_id[27] <- "US English"
Lang_id[28] <- "Portuguese (BRA)"
Lang_id[29] <- "Slovak"
Lang_id[30] <- "Hindi"
Lang_id[31] <- "Korean"
Lang_id[32] <- "Thai"
Lang_id[33] <- "Filipino"
Lang_id[34] <- "Vietnamese"
Lang_id[35] <- "Indonesian"
Lang_id[36] <- "Albanian"
Lang_id[37] <- "Latvian"
Lang_id[38] <- "Lithuanian"

# country
country_id <- ""
#country_id[0] <- "- Please select -"
country_id[1] <- "Afghanistan"
country_id[2] <- "Albania"
country_id[3] <- "Algeria"
country_id[4] <- "Andorra"
country_id[5] <- "Angola"
country_id[6] <- "Argentina"
country_id[7] <- "Armenia"
country_id[8] <- "Australia"
country_id[9] <- "Austria"
country_id[10] <- "Azerbaijan"
country_id[11] <- "Bahamas"
country_id[12] <- "Bahrain"
country_id[13] <- "Bangladesh"
country_id[14] <- "Barbados"
country_id[15] <- "Belarus"
country_id[16] <- "Belgium"
country_id[17] <- "Belize"
country_id[18] <- "Benin"
country_id[19] <- "Bermuda"
country_id[20] <- "Bhutan"
country_id[21] <- "Bolivia"
country_id[22] <- "Bosnia-Herzegovina"
country_id[23] <- "Botswana"
country_id[24] <- "Brazil"
country_id[25] <- "Bulgaria"
country_id[26] <- "Burkina Faso"
country_id[27] <- "Burundi"
country_id[28] <- "Cambodia"
country_id[29] <- "Cameroon"
country_id[30] <- "Canada"
country_id[31] <- "Cape Verde"
country_id[32] <- "Cayman Islands"
country_id[33] <- "Central African Republic"
country_id[34] <- "Chad"
country_id[35] <- "Chile"
country_id[36] <- "China"
country_id[37] <- "Colombia"
country_id[38] <- "Congo"
country_id[39] <- "Costa Rica"
country_id[40] <- "Croatia"
country_id[41] <- "Cyprus"
country_id[42] <- "Czech Republic"
country_id[43] <- "Denmark"
country_id[44] <- "Djibouti"
country_id[45] <- "Ecuador"
country_id[46] <- "Egypt"
country_id[47] <- "El Salvador"
country_id[48] <- "Equatorial Guinea"
country_id[49] <- "Eritrea"
country_id[50] <- "Estonia"
country_id[51] <- "Ethiopia"
country_id[52] <- "Fiji"
country_id[53] <- "Finland"
country_id[54] <- "France"
country_id[55] <- "France (European Territory)"
country_id[56] <- "Gabon"
country_id[57] <- "Gambia"
country_id[58] <- "Georgia"
country_id[59] <- "Germany"
country_id[60] <- "Ghana"
country_id[61] <- "Greece"
country_id[62] <- "Guam"
country_id[63] <- "Guinea"
country_id[64] <- "Guinea Bissau"
country_id[65] <- "Guyana"
country_id[66] <- "Honduras"
country_id[67] <- "Hong Kong"
country_id[68] <- "Hungary"
country_id[69] <- "Iceland"
country_id[70] <- "India"
country_id[71] <- "Indonesia"
country_id[72] <- "Iran"
country_id[73] <- "Iraq"
country_id[74] <- "Ireland"
country_id[75] <- "Israel"
country_id[76] <- "Italy"
country_id[77] <- "Republic of C�te d'Ivoire"
country_id[78] <- "Japan"
country_id[79] <- "Jordan"
country_id[80] <- "Kazakhstan"
country_id[81] <- "Kenya"
country_id[82] <- "Kuwait"
country_id[83] <- "Kyrgyzstan"
country_id[84] <- "Laos"
country_id[85] <- "Latvia"
country_id[86] <- "Lebanon"
country_id[87] <- "Lesotho"
country_id[88] <- "Liberia"
country_id[89] <- "Libya"
country_id[90] <- "Liechtenstein"
country_id[91] <- "Lithuania"
country_id[92] <- "Luxembourg"
country_id[93] <- "Macedonia"
country_id[94] <- "Madagascar"
country_id[95] <- "Malawi"
country_id[96] <- "Malaysia"
country_id[97] <- "Maldives"
country_id[98] <- "Mali"
country_id[99] <- "Malta"
country_id[100] <- "Mauritania"
country_id[101] <- "Mexico"
country_id[102] <- "Moldova"
country_id[103] <- "Monaco"
country_id[104] <- "Mongolia"
country_id[105] <- "Morocco"
country_id[106] <- "Mozambique"
country_id[107] <- "Namibia"
country_id[108] <- "Nepal"
country_id[109] <- "Netherlands"
country_id[110] <- "Netherlands Antilles"
country_id[111] <- "New Zealand"
country_id[112] <- "Nicaragua"
country_id[113] <- "Niger"
country_id[114] <- "Nigeria"
country_id[115] <- "North Korea"
country_id[116] <- "Norway"
country_id[117] <- "Oman"
country_id[118] <- "Pakistan"
country_id[119] <- "Panama"
country_id[120] <- "Paraguay"
country_id[121] <- "Peru"
country_id[122] <- "Philippines"
country_id[123] <- "Poland"
country_id[124] <- "Portugal"
country_id[126] <- "Qatar"
country_id[127] <- "Re�nion"
country_id[128] <- "Romania"
country_id[129] <- "Russian Federation"
country_id[130] <- "Rwanda"
country_id[131] <- "Saint Tome and Principe"
country_id[132] <- "Samoa"
country_id[133] <- "Saudi Arabia"
country_id[134] <- "Senegal"
country_id[135] <- "Seychelles"
country_id[136] <- "Sierra Leone"
country_id[137] <- "Singapore"
country_id[138] <- "Slovakia"
country_id[139] <- "Slovenia"
country_id[140] <- "Somalia"
country_id[141] <- "South Africa"
country_id[142] <- "South Korea"
country_id[143] <- "Spain"
country_id[144] <- "Sri Lanka"
country_id[145] <- "Sudan"
country_id[146] <- "Suriname"
country_id[147] <- "Swaziland"
country_id[148] <- "Sweden"
country_id[149] <- "Switzerland"
country_id[150] <- "Syria"
country_id[151] <- "Tajikistan"
country_id[152] <- "Taiwan"
country_id[153] <- "Tanzania"
country_id[154] <- "Thailand"
country_id[155] <- "Togo"
country_id[156] <- "Tunisia"
country_id[157] <- "Turkey"
country_id[158] <- "Turkmenistan"
country_id[159] <- "Uganda"
country_id[160] <- "UK"
country_id[161] <- "Ukraine"
country_id[162] <- "United Arab Emirates"
country_id[163] <- "Uruguay"
country_id[164] <- "US"
country_id[165] <- "Uzbekistan"
country_id[166] <- "Vatican City"
country_id[167] <- "Venezuela"
country_id[168] <- "Vietnam"
country_id[169] <- "Virgin Islands (British)"
country_id[170] <- "Yemen"
country_id[171] <- "Yugoslavia"
country_id[172] <- "Zaire"
country_id[173] <- "Zambia"
country_id[174] <- "Zimbabwe"
country_id[175] <- "Antigua and Barbuda"
country_id[176] <- "Anguilla"
country_id[177] <- "Antartica"
country_id[178] <- "American Samoa"
country_id[179] <- "Aruba"
country_id[180] <- "Brunei Darussalam"
country_id[181] <- "Bouvet Island"
country_id[182] <- "Cocos (Keeling) Islands"
country_id[183] <- "Cook Islands"
country_id[184] <- "Cuba"
country_id[185] <- "Christmas Island"
country_id[186] <- "Dominica"
country_id[187] <- "Dominican Republic"
country_id[188] <- "Western Sahara"
country_id[189] <- "Falkland Islands"
country_id[190] <- "Micronesia"
country_id[191] <- "Faroe Islands"
country_id[192] <- "Grenada"
country_id[193] <- "French Guyana"
country_id[194] <- "Gibraltar"
country_id[195] <- "Greenland"
country_id[196] <- "Guadeloupe"
country_id[197] <- "S. Georgia & S. Sandwich Islands"
country_id[198] <- "Guatemala"
country_id[199] <- "Heard and McDonald Islands"
country_id[200] <- "Haiti"
country_id[201] <- "British Indian Ocean Territory"
country_id[202] <- "Jamaica"
country_id[203] <- "Kiribati"
country_id[204] <- "Comoros"
country_id[205] <- "Saint Kitts & Nevis Anguilla"
country_id[206] <- "Saint Lucia"
country_id[207] <- "Marshall Islands"
country_id[208] <- "Macau"
country_id[209] <- "Northern Mariana Islands"
country_id[210] <- "Martinique"
country_id[211] <- "Montserrat"
country_id[212] <- "Mauritius"
country_id[213] <- "New Caledonia"
country_id[214] <- "Norfolk Island"
country_id[215] <- "Nauru"
country_id[216] <- "Neutral Zone"
country_id[217] <- "Niue"
country_id[218] <- "Polynesia (French)"
country_id[219] <- "Papua New Guinea"
country_id[220] <- "Saint Pierre and Miquelon"
country_id[221] <- "Pitcairn Island"
country_id[222] <- "Palau"
country_id[223] <- "Solomon Islands"
country_id[224] <- "Saint Helena"
country_id[225] <- "Svalbard and Jan Mayen Islands"
country_id[226] <- "Slovak Republic"
country_id[227] <- "San Marino"
country_id[228] <- "Former USSR"
country_id[229] <- "Turks and Caicos Islands"
country_id[230] <- "French Southern Territories"
country_id[231] <- "Tokelau"
country_id[232] <- "Tonga"
country_id[233] <- "East Timor"
country_id[234] <- "Trinidad and Tobago"
country_id[235] <- "Tuvalu"
country_id[236] <- "USA Minor Outlying Islands"
country_id[237] <- "Saint Vincent & Grenadines"
country_id[238] <- "Virgin Islands (USA)"
country_id[239] <- "Vanuatu"
country_id[240] <- "Wallis and Futuna Islands"
country_id[241] <- "Mayotte"
country_id[242] <- "Myanmar"

# data conversion from factors to numericals
# shapes (management)
Part_ID <- as.numeric(as.character(pivot$Part_ID))
X102_consistencyraw <- as.numeric(as.character(pivot$'102_consistencyraw'))
X102_pointdistribution <- as.numeric(as.character(pivot$'102_pointdistribution'))
X102_shapes_01_directing <- as.numeric(as.character(pivot$'102_scalesraw_1'))
X102_shapes_02_persuasive <- as.numeric(as.character(pivot$'102_scalesraw_2'))
X102_shapes_03_socially_confident <- as.numeric(as.character(pivot$'102_scalesraw_3'))
X102_shapes_04_sociable <- as.numeric(as.character(pivot$'102_scalesraw_4'))
X102_shapes_05_agreeable <- as.numeric(as.character(pivot$'102_scalesraw_5'))
X102_shapes_06_behavioral <- as.numeric(as.character(pivot$'102_scalesraw_6'))
X102_shapes_07_prudent <- as.numeric(as.character(pivot$'102_scalesraw_7'))
X102_shapes_08_focused_on_results <- as.numeric(as.character(pivot$'102_scalesraw_8'))
X102_shapes_09_systematic <- as.numeric(as.character(pivot$'102_scalesraw_9'))
X102_shapes_10_conscientious <- as.numeric(as.character(pivot$'102_scalesraw_10'))
X102_shapes_11_analytical <- as.numeric(as.character(pivot$'102_scalesraw_11'))
X102_shapes_12_conceptual <- as.numeric(as.character(pivot$'102_scalesraw_12'))
X102_shapes_13_imaginative <- as.numeric(as.character(pivot$'102_scalesraw_13'))
X102_shapes_14_open_to_change <- as.numeric(as.character(pivot$'102_scalesraw_14'))
X102_shapes_15_autonomous <- as.numeric(as.character(pivot$'102_scalesraw_15'))
X102_shapes_16_achieving <- as.numeric(as.character(pivot$'102_scalesraw_16'))
X102_shapes_17_competitive <- as.numeric(as.character(pivot$'102_scalesraw_17'))
X102_shapes_18_energetic <- as.numeric(as.character(pivot$'102_scalesraw_18'))
X102_shapes_testtime <- as.numeric(as.character(pivot$'102_testtime'))

# shapes (basic)
Part_ID <- as.numeric(as.character(pivot$Part_ID))
X103_consistencyraw <- as.numeric(as.character(pivot$'103_consistencyraw'))
X103_pointdistribution <- as.numeric(as.character(pivot$'103_pointdistribution'))
X103_shapes_01_Professional_Challenge <- as.numeric(as.character(pivot$'103_scalesraw_1'))
X103_shapes_02_Identification <- as.numeric(as.character(pivot$'103_scalesraw_2'))
X103_shapes_03_Conscientiousness <- as.numeric(as.character(pivot$'103_scalesraw_3'))
X103_shapes_04_Creativity <- as.numeric(as.character(pivot$'103_scalesraw_4'))
X103_shapes_05_Circumspection <- as.numeric(as.character(pivot$'103_scalesraw_5'))
X103_shapes_06_Fun_at_Work <- as.numeric(as.character(pivot$'103_scalesraw_6'))
X103_shapes_07_Striving_for_Harmony <- as.numeric(as.character(pivot$'103_scalesraw_7'))
X103_shapes_08_Sociable_Skills <- as.numeric(as.character(pivot$'103_scalesraw_8'))
X103_shapes_09_Cooperation <- as.numeric(as.character(pivot$'103_scalesraw_9'))
X103_shapes_10_Autonomy <- as.numeric(as.character(pivot$'103_scalesraw_10'))
X103_shapes_11_Flexibility <- as.numeric(as.character(pivot$'103_scalesraw_11'))
X103_shapes_12_Recognition <- as.numeric(as.character(pivot$'103_scalesraw_12'))
X103_shapes_13_Self_efficacy <- as.numeric(as.character(pivot$'103_scalesraw_13'))
X103_shapes_14_Perseverance <- as.numeric(as.character(pivot$'103_scalesraw_14'))
X103_shapes_15_Keenness <- as.numeric(as.character(pivot$'103_scalesraw_15'))
X103_shapes_testtime <- as.numeric(as.character(pivot$'103_testtime'))

# views
X201_consistencyraw <- as.numeric(as.character(pivot$'201_consistencyraw'))
X201_pointdistribution  <- as.numeric(as.character(pivot$'201_pointdistribution'))
X201_views_01_Professional_Challenge <- as.numeric(as.character(pivot$'201_scalesraw_1'))
X201_views_02_Recognition_of_Performance <- as.numeric(as.character(pivot$'201_scalesraw_2'))
X201_views_03_Financial_Reward <- as.numeric(as.character(pivot$'201_scalesraw_3')) 
X201_views_04_Security <- as.numeric(as.character(pivot$'201_scalesraw_4'))
X201_views_05_Fun_while_working <- as.numeric(as.character(pivot$'201_scalesraw_5'))
X201_views_06_Identification <- as.numeric(as.character(pivot$'201_scalesraw_6')) 
X201_views_07_Harmony <- as.numeric(as.character(pivot$'201_scalesraw_7')) 
X201_views_08_Honesty <- as.numeric(as.character(pivot$'201_scalesraw_8')) 
X201_views_09_Cooperativeness <- as.numeric(as.character(pivot$'201_scalesraw_9')) 
X201_views_10_Integrity <- as.numeric(as.character(pivot$'201_scalesraw_10')) 
X201_views_11_Fairness <- as.numeric(as.character(pivot$'201_scalesraw_11')) 
X201_views_12_Hierarchy <- as.numeric(as.character(pivot$'201_scalesraw_12'))
X201_views_13_Structuring <- as.numeric(as.character(pivot$'201_scalesraw_13'))
X201_views_14_Rate_of_change <- as.numeric(as.character(pivot$'201_scalesraw_14'))
X201_views_15_Development_Opportunities <- as.numeric(as.character(pivot$'201_scalesraw_15'))
X201_views_16_Absence_of_Stress <- as.numeric(as.character(pivot$'201_scalesraw_16'))
X201_views_17_Influence_Possibilities <- as.numeric(as.character(pivot$'201_scalesraw_17'))
X201_views_18_Working_Environment <- as.numeric(as.character(pivot$'201_scalesraw_18'))
X201_views_testtime <- as.numeric(as.character(pivot$'201_testtime'))

# clues
X305_active <- as.numeric(as.character(pivot$'305_activity'))
X305_targeted <- as.numeric(as.character(pivot$'305_precision'))
X305_accurate <- as.numeric(as.character(pivot$'305_prioritiescorrect'))
X305_complete <- as.numeric(as.character(pivot$'305_prioritiesset'))

# numerical (consumer)
X311_accuracy <- as.numeric(as.character(pivot$'311_accuracy'))
X311_overallperformance <- as.numeric(as.character(pivot$'311_overallperformance'))
X311_speed <- as.numeric(as.character(pivot$'311_speed'))

# verbal (consumer)
X312_accuracy <- as.numeric(as.character(pivot$'312_accuracy'))
X312_overallperformance <- as.numeric(as.character(pivot$'312_overallperformance'))
X312_speed <- as.numeric(as.character(pivot$'312_speed'))

# lt-e
X315_fluencyaccuracy <- as.numeric(as.character(pivot$'315_fluencyaccuracy'))
X315_fluencyraw <- as.numeric(as.character(pivot$'315_fluencyraw'))
X315_fluencyspeed <- as.numeric(as.character(pivot$'315_fluencyspeed'))
X315_spellingaccuracy <- as.numeric(as.character(pivot$'315_spellingaccuracy'))
X315_spellingraw <- as.numeric(as.character(pivot$'315_spellingraw'))
X315_spellingspeed <- as.numeric(as.character(pivot$'315_spellingspeed'))
X315_vocabaccuracy <- as.numeric(as.character(pivot$'315_vocabaccuracy'))
X315_vocabraw <- as.numeric(as.character(pivot$'315_vocabraw'))
X315_vocabspeed <- as.numeric(as.character(pivot$'315_vocabspeed'))

# lt-d
X316_fluencyaccuracy <- as.numeric(as.character(pivot$'316_fluencyaccuracy'))
X316_fluencyraw <- as.numeric(as.character(pivot$'316_fluencyraw'))
X316_fluencyspeed <- as.numeric(as.character(pivot$'316_fluencyspeed'))
X316_spellingaccuracy <- as.numeric(as.character(pivot$'316_spellingaccuracy'))
X316_spellingraw <- as.numeric(as.character(pivot$'316_spellingraw'))
X316_spellingspeed <- as.numeric(as.character(pivot$'316_spellingspeed'))
X316_vocabaccuracy <- as.numeric(as.character(pivot$'316_vocabaccuracy'))
X316_vocabraw <- as.numeric(as.character(pivot$'316_vocabraw'))
X316_vocabspeed <- as.numeric(as.character(pivot$'316_vocabspeed'))

# e3+
X323_accuracy <- as.numeric(as.character(pivot$'323_accuracy'))
X323_overallperformance <- as.numeric(as.character(pivot$'323_overallperformance'))
X323_speed <- as.numeric(as.character(pivot$'323_speed'))

X328_accuracy <- as.numeric(as.character(pivot$'328_accuracy'))
X328_overallperformance <- as.numeric(as.character(pivot$'328_overallperformance'))
X328_speed <- as.numeric(as.character(pivot$'328_speed'))

# numerical (consumer)
X331_accuracy <- as.numeric(as.character(pivot$'331_accuracy'))
X331_overallperformance <- as.numeric(as.character(pivot$'331_overallperformance'))
X331_speed <- as.numeric(as.character(pivot$'331_speed'))

# verbal (admin)
X332_accuracy <- as.numeric(as.character(pivot$'332_accuracy'))
X332_overallperformance <- as.numeric(as.character(pivot$'332_overallperformance'))
X332_speed <- as.numeric(as.character(pivot$'332_speed'))

# lst
X344_overallperformance <- as.numeric(as.character(pivot$'344_overallperformance'))
X344_accuracy <- as.numeric(as.character(pivot$'344_accuracy'))
X344_speed <- as.numeric(as.character(pivot$'344_speed'))

# ix
#!!!!!!!!!!!
X349

# eql
X354_accuracy <- as.numeric(as.character(pivot$'354_accuracy'))
X354_overallperformance <- as.numeric(as.character(pivot$'354_overallperformance'))
X354_speed <- as.numeric(as.character(pivot$'354_speed'))

# cls
X361_overallperformance <- as.numeric(as.character(pivot$'361_overallperformance'))
X361_speed <- as.numeric(as.character(pivot$'361_speed'))
X361_accuracy <-  as.numeric(as.character(pivot$'361_accuracy'))

X701_accuracy <- as.numeric(as.character(pivot$'701_accuracy'))
X701_overallperformance <- as.numeric(as.character(pivot$'701_overallperformance'))
X701_speed <-  as.numeric(as.character(pivot$'701_speed'))

X751_accuracy <- as.numeric(as.character(pivot$'751_accuracy'))
X751_overallperformance <- as.numeric(as.character(pivot$'751_overallperformance'))
X751_speed <- as.numeric(as.character(pivot$'751_speed'))	
	
# verbal (instruct)
X752_accuracy <- as.numeric(as.character(pivot$'752_accuracy'))
X752_overallperformance <- as.numeric(as.character(pivot$'752_overallperformance'))
X752_speed <- as.numeric(as.character(pivot$'752_speed'))

pivot_all <- cbind(Part_ID, 
X102_consistencyraw,
X102_pointdistribution,
X102_shapes_01_directing,
X102_shapes_02_persuasive,
X102_shapes_03_socially_confident,
X102_shapes_04_sociable,
X102_shapes_05_agreeable,
X102_shapes_06_behavioral,
X102_shapes_07_prudent,
X102_shapes_08_focused_on_results,
X102_shapes_09_systematic,
X102_shapes_10_conscientious,
X102_shapes_11_analytical,
X102_shapes_12_conceptual,
X102_shapes_13_imaginative,
X102_shapes_14_open_to_change,
X102_shapes_15_autonomous,
X102_shapes_16_achieving,
X102_shapes_17_competitive,
X102_shapes_18_energetic,
X102_shapes_testtime,
X103_consistencyraw, 
X103_pointdistribution, 
X103_shapes_01_Professional_Challenge, 
X103_shapes_02_Identification, 
X103_shapes_03_Conscientiousness, 
X103_shapes_04_Creativity, 
X103_shapes_05_Circumspection, 
X103_shapes_06_Fun_at_Work, 
X103_shapes_07_Striving_for_Harmony, 
X103_shapes_08_Sociable_Skills, 
X103_shapes_09_Cooperation, 
X103_shapes_10_Autonomy, 
X103_shapes_11_Flexibility, 
X103_shapes_12_Recognition, 
X103_shapes_13_Self_efficacy, 
X103_shapes_14_Perseverance, 
X103_shapes_15_Keenness, 
X103_shapes_testtime, 
X201_consistencyraw, 
X201_pointdistribution, 
X201_views_01_Professional_Challenge, 
X201_views_02_Recognition_of_Performance, 
X201_views_03_Financial_Reward, 
X201_views_04_Security, 
X201_views_05_Fun_while_working, 
X201_views_06_Identification, 
X201_views_07_Harmony, 
X201_views_08_Honesty, 
X201_views_09_Cooperativeness, 
X201_views_10_Integrity, 
X201_views_11_Fairness, 
X201_views_12_Hierarchy, 
X201_views_13_Structuring, 
X201_views_14_Rate_of_change, 
X201_views_15_Development_Opportunities,
X201_views_16_Absence_of_Stress,
X201_views_17_Influence_Possibilities,
X201_views_18_Working_Environment, 
X201_views_testtime,
X305_complete,
X305_accurate,
X305_active,
X305_targeted,
X311_overallperformance, 
X311_speed,
X311_accuracy, 
X312_overallperformance, 
X312_speed,
X312_accuracy, 
X315_fluencyraw, 
X315_fluencyspeed,
X315_fluencyaccuracy, 
X315_spellingraw, 
X315_spellingspeed,
X315_spellingaccuracy, 
X315_vocabraw, 
X315_vocabspeed,
X315_vocabaccuracy, 
X316_fluencyraw, 
X316_fluencyspeed,
X316_fluencyaccuracy, 
X316_spellingraw, 
X316_spellingspeed,
X316_spellingaccuracy, 
X316_vocabraw, 
X316_vocabspeed,
X316_vocabaccuracy, 
X323_overallperformance, 
X323_speed,
X323_accuracy, 
X328_overallperformance, 
X328_speed,
X328_accuracy, 
X331_overallperformance, 
X331_speed,
X331_accuracy, 
X332_overallperformance, 
X332_speed,
X332_accuracy, 
X344_overallperformance, 
X344_speed, 
X344_accuracy, 
X354_overallperformance, 
X354_speed, 
X354_accuracy, 
X701_overallperformance, 
X701_speed,
X701_accuracy, 
X751_overallperformance, 
X751_speed,
X751_accuracy,
X752_overallperformance, 
X752_speed,
X752_accuracy

) 

colnames(pivot_all)
class(pivot_all)
pivot_all <- as.data.frame(pivot_all)
#names(pivot_all)

# save pivot table
pivot <- pivot_all
names(pivot)

save.xlsx("_data\\pivot.xlsx", pivot)

############################################
# 103 shapes (basic)

if(length(X103_consistencyraw) > 0) {

shapes_basic <- as.data.frame(cbind(Part_ID, 
				X103_consistencyraw, 
				X103_pointdistribution, 
				X103_shapes_01_Professional_Challenge, 
				X103_shapes_02_Identification, 
				X103_shapes_03_Conscientiousness, 
				X103_shapes_04_Creativity, 
				X103_shapes_05_Circumspection, 
				X103_shapes_06_Fun_at_Work, 
				X103_shapes_07_Striving_for_Harmony, 
				X103_shapes_08_Sociable_Skills, 
				X103_shapes_09_Cooperation, 
				X103_shapes_10_Autonomy, 
				X103_shapes_11_Flexibility, 
				X103_shapes_12_Recognition, 
				X103_shapes_13_Self_efficacy, 
				X103_shapes_14_Perseverance, 
				X103_shapes_15_Keenness, 
				X103_shapes_testtime))
names(shapes_basic)

# data clearing

# remove rows with low consistency
shapes_basic[shapes_basic$X103_consistencyraw > 2,]
class(shapes_basic)
names(shapes_basic)

# remove rows with no results for shapes
shapes_basic <- na.omit(shapes_basic)

# remove rows with low consistency
del_count <- 0
for (i in 1:length(shapes_basic$Part_ID)) {
	if (shapes_basic$X103_consistencyraw[i]  <= 2 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("consistency low: ", shapes_basic$X103_consistencyraw[i], ", Part_ID:", shapes_basic$Part_ID[i], " will be deleted from dataset"))
		shapes_basic$X103_consistencyraw[i] <- NA
		#remove(shapes_basic[1,])
	}
}
print(paste0(del_count, " cases with consistency <= 2 will be deleted from dataset"))
shapes_basic <- na.omit(shapes_basic)

# remove rows with testtime shorter than 5 or longer than 60 minutes
del_count <- 0
for (i in 1:length(shapes_basic$Part_ID)) {
	if (shapes_basic$X103_shapes_testtime[i]  < 5 || shapes_basic$X103_shapes_testtime[i] > 60 ) {  # adjust test time thresholds in order to delete cases here
		del_count <- del_count + 1
		print(paste0("time very short or long: ", shapes_basic$X103_shapes_testtime[i], ", Part_ID:", shapes_basic$Part_ID[i], " will be deleted from dataset"))
		shapes_basic$X103_shapes_testtime[i] <- NA
		#remove(shapes_basic[1,])
	}
}
print(paste0(del_count, " case(s) with test time < 5 or > 60 will be deleted from dataset"))
shapes_basic <- na.omit(shapes_basic)

# remove cases with low point distribution
del_count <- 0
for (i in 1:length(shapes_basic$Part_ID)) {
	if (shapes_basic$X103_pointdistribution[i]  < .8 ) {  # adjust point distribution threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("point distribution low: ", shapes_basic$X103_shapes_testtime[i], ", Part_ID:", shapes_basic$Part_ID[i], " will be deleted from dataset"))
		shapes_basic$X103_shapes_testtime[i] <- NA
		#remove(shapes_basic[1,])
	}
}
print(paste0(del_count, " case(s) with point distribution < .8 will be deleted from dataset"))
shapes_basic <- na.omit(shapes_basic)

names(shapes_basic)

# Stanine
M103_01 <- mean(shapes_basic$X103_shapes_01_Professional_Challenge, na.rm=TRUE)
M103_02 <- mean(shapes_basic$X103_shapes_02_Identification, na.rm=TRUE)
M103_03 <- mean(shapes_basic$X103_shapes_03_Conscientiousness, na.rm=TRUE)
M103_04 <- mean(shapes_basic$X103_shapes_04_Creativity, na.rm=TRUE)
M103_05 <- mean(shapes_basic$X103_shapes_05_Circumspection, na.rm=TRUE)
M103_06 <- mean(shapes_basic$X103_shapes_06_Fun_at_Work, na.rm=TRUE)
M103_07 <- mean(shapes_basic$X103_shapes_07_Striving_for_Harmony, na.rm=TRUE)
M103_08 <- mean(shapes_basic$X103_shapes_08_Sociable_Skills)
M103_09 <- mean(shapes_basic$X103_shapes_09_Cooperation, na.rm=TRUE)
M103_10 <- mean(shapes_basic$X103_shapes_10_Autonomy, na.rm=TRUE)
M103_11 <- mean(shapes_basic$X103_shapes_11_Flexibility, na.rm=TRUE)
M103_12 <- mean(shapes_basic$X103_shapes_12_Recognition, na.rm=TRUE)
M103_13 <- mean(shapes_basic$X103_shapes_13_Self_efficacy, na.rm=TRUE)
M103_14 <- mean(shapes_basic$X103_shapes_14_Perseverance, na.rm=TRUE)
M103_15 <- mean(shapes_basic$X103_shapes_15_Keenness, na.rm=TRUE)

SD103_01 <- sd(shapes_basic$X103_shapes_01_Professional_Challenge, na.rm=TRUE)
SD103_02 <- sd(shapes_basic$X103_shapes_02_Identification, na.rm=TRUE)
SD103_03 <- sd(shapes_basic$X103_shapes_03_Conscientiousness, na.rm=TRUE)
SD103_04 <- sd(shapes_basic$X103_shapes_04_Creativity, na.rm=TRUE)
SD103_05 <- sd(shapes_basic$X103_shapes_05_Circumspection, na.rm=TRUE)
SD103_06 <- sd(shapes_basic$X103_shapes_06_Fun_at_Work, na.rm=TRUE)
SD103_07 <- sd(shapes_basic$X103_shapes_07_Striving_for_Harmony, na.rm=TRUE)
SD103_08 <- sd(shapes_basic$X103_shapes_08_Sociable_Skills)
SD103_09 <- sd(shapes_basic$X103_shapes_09_Cooperation, na.rm=TRUE)
SD103_10 <- sd(shapes_basic$X103_shapes_10_Autonomy, na.rm=TRUE)
SD103_11 <- sd(shapes_basic$X103_shapes_11_Flexibility, na.rm=TRUE)
SD103_12 <- sd(shapes_basic$X103_shapes_12_Recognition, na.rm=TRUE)
SD103_13 <- sd(shapes_basic$X103_shapes_13_Self_efficacy, na.rm=TRUE)
SD103_14 <- sd(shapes_basic$X103_shapes_14_Perseverance, na.rm=TRUE)
SD103_15 <- sd(shapes_basic$X103_shapes_15_Keenness, na.rm=TRUE)

X103_shapes_01_Professional_Challenge_Stanine <- round(ifelse((shapes_basic$X103_shapes_01_Professional_Challenge - M103_01)/SD103_01*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_01_Professional_Challenge - M103_01)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_01_Professional_Challenge - M103_01)/SD103_01*2+5)))
X103_shapes_02_Identification_Stanine <- round(ifelse((shapes_basic$X103_shapes_02_Identification - M103_02)/SD103_02*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_02_Identification - M103_02)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_02_Identification - M103_02)/SD103_02*2+5)))
X103_shapes_03_Conscientiousness_Stanine <- round(ifelse((shapes_basic$X103_shapes_03_Conscientiousness - M103_03)/SD103_03*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_03_Conscientiousness - M103_03)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_03_Conscientiousness - M103_03)/SD103_03*2+5)))
X103_shapes_04_Creativity_Stanine <- round(ifelse((shapes_basic$X103_shapes_04_Creativity - M103_04)/SD103_04*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_04_Creativity - M103_04)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_04_Creativity - M103_04)/SD103_04*2+5)))
X103_shapes_05_Circumspection_Stanine <- round(ifelse((shapes_basic$X103_shapes_05_Circumspection - M103_05)/SD103_05*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_05_Circumspection - M103_05)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_05_Circumspection - M103_05)/SD103_05*2+5)))
X103_shapes_06_Fun_at_Work_Stanine <- round(ifelse((shapes_basic$X103_shapes_06_Fun_at_Work - M103_06)/SD103_06*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_06_Fun_at_Work - M103_06)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_06_Fun_at_Work - M103_06)/SD103_06*2+5)))
X103_shapes_07_Striving_for_Harmony_Stanine <- round(ifelse((shapes_basic$X103_shapes_07_Striving_for_Harmony - M103_07)/SD103_07*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_07_Striving_for_Harmony - M103_07)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_07_Striving_for_Harmony - M103_07)/SD103_07*2+5)))
X103_shapes_08_Sociable_Skills_Stanine <- round(ifelse((shapes_basic$X103_shapes_08_Sociable_Skills - M103_08)/SD103_08*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_08_Sociable_Skills - M103_08)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_08_Sociable_Skills - M103_08)/SD103_08*2+5)))
X103_shapes_09_Cooperation_Stanine <- round(ifelse((shapes_basic$X103_shapes_09_Cooperation - M103_09)/SD103_09*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_09_Cooperation - M103_09)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_09_Cooperation - M103_09)/SD103_09*2+5)))
X103_shapes_10_Autonomy_Stanine <- round(ifelse((shapes_basic$X103_shapes_10_Autonomy - M103_10)/SD103_10*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_10_Autonomy - M103_10)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_10_Autonomy - M103_10)/SD103_10*2+5)))
X103_shapes_11_Flexibility_Stanine <- round(ifelse((shapes_basic$X103_shapes_11_Flexibility - M103_11)/SD103_11*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_11_Flexibility - M103_11)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_11_Flexibility - M103_11)/SD103_11*2+5)))
X103_shapes_12_Recognition_Stanine <- round(ifelse((shapes_basic$X103_shapes_12_Recognition - M103_12)/SD103_12*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_12_Recognition - M103_12)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_12_Recognition - M103_12)/SD103_12*2+5)))
X103_shapes_13_Self_efficacy_Stanine <- round(ifelse((shapes_basic$X103_shapes_13_Self_efficacy - M103_13)/SD103_13*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_13_Self_efficacy - M103_13)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_13_Self_efficacy - M103_13)/SD103_13*2+5)))
X103_shapes_14_Perseverance_Stanine <- round(ifelse((shapes_basic$X103_shapes_14_Perseverance - M103_14)/SD103_14*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_14_Perseverance - M103_14)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_14_Perseverance - M103_14)/SD103_14*2+5)))
X103_shapes_15_Keenness_Stanine <- round(ifelse((shapes_basic$X103_shapes_15_Keenness - M103_15)/SD103_15*2+5>9 , 9, ifelse((shapes_basic$X103_shapes_15_Keenness - M103_15)/SD103_01*2+5<1, 1, (shapes_basic$X103_shapes_15_Keenness - M103_15)/SD103_15*2+5)))

#class(shapes_basic)

shapes_basic <- cbind(shapes_basic,
	X103_shapes_01_Professional_Challenge_Stanine,
	X103_shapes_02_Identification_Stanine,
	X103_shapes_03_Conscientiousness_Stanine,
	X103_shapes_04_Creativity_Stanine,
	X103_shapes_05_Circumspection_Stanine,
	X103_shapes_06_Fun_at_Work_Stanine,
	X103_shapes_07_Striving_for_Harmony_Stanine,
	X103_shapes_08_Sociable_Skills_Stanine,
	X103_shapes_09_Cooperation_Stanine,
	X103_shapes_10_Autonomy_Stanine,
	X103_shapes_11_Flexibility_Stanine,
	X103_shapes_12_Recognition_Stanine,
	X103_shapes_13_Self_efficacy_Stanine,
	X103_shapes_14_Perseverance_Stanine,
	X103_shapes_15_Keenness_Stanine
)

colnames(shapes_basic)
class(shapes_basic)
shapes_basic <- as.data.frame(shapes_basic)
names(shapes_basic)

# diagrams stanine


#freq_s01 <- as.data.frame(prop.table(table(X103_shapes_01_Professional_Challenge_Stanine)))

#plot(freq_s01$X103_shapes_01_Professional_Challenge_Stanine,freq_s01$Freq*100, col=c.blue)

barplot(prop.table(table(X103_shapes_01_Professional_Challenge_Stanine)), col=c.blue, xlab="Stanine Professional Challenge", ylab="percent/100")
barplot(prop.table(table(X103_shapes_02_Identification_Stanine)), col=c.blue, xlab="Stanine Identification", ylab="percent/100")
barplot(prop.table(table(X103_shapes_03_Conscientiousness_Stanine)), col=c.blue, xlab="Stanine Conscientiousness", ylab="percent/100")
barplot(prop.table(table(X103_shapes_04_Creativity_Stanine)), col=c.blue, xlab="Stanine Creativity", ylab="percent/100")
barplot(prop.table(table(X103_shapes_05_Circumspection_Stanine)), col=c.blue, xlab="Stanine Circumspection", ylab="percent/100")
barplot(prop.table(table(X103_shapes_06_Fun_at_Work_Stanine)), col=c.blue, xlab="Stanine Fun_at_Work", ylab="percent/100")
barplot(prop.table(table(X103_shapes_07_Striving_for_Harmony_Stanine)), col=c.blue, xlab="Stanine Striving_for_Harmony", ylab="percent/100")
##barplot(prop.table(table(X103_shapes_08_Sociable_Skills_Stanine)), col=c.blue, xlab="Stanine Sociable_Skills", ylab="percent/100")
barplot(prop.table(table(X103_shapes_09_Cooperation_Stanine)), col=c.blue, xlab="Stanine Cooperation", ylab="percent/100")
barplot(prop.table(table(X103_shapes_10_Autonomy_Stanine)), col=c.blue, xlab="Stanine Autonomy", ylab="percent/100")
barplot(prop.table(table(X103_shapes_11_Flexibility_Stanine)), col=c.blue, xlab="Stanine Flexibility", ylab="percent/100")
barplot(prop.table(table(X103_shapes_12_Recognition_Stanine)), col=c.blue, xlab="Stanine Recognition", ylab="percent/100")
barplot(prop.table(table(X103_shapes_13_Self_efficacy_Stanine)), col=c.blue, xlab="Stanine Self_efficacy", ylab="percent/100")
barplot(prop.table(table(X103_shapes_14_Perseverance_Stanine)), col=c.blue, xlab="Stanine Perseverance", ylab="percent/100")
##barplot(prop.table(table(X103_shapes_15_Keenness_Stanine)), col=c.blue, xlab="Stanine Keenness", ylab="percent/100")

# add biodata to shapes basic
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

shapes_basic_gender <- NA
shapes_basic_BirthYear <- NA
shapes_basic_country <- NA
shapes_basic_language <- NA
shapes_basic_Part_EduTime <- NA
shapes_basic_Part_ExpTime <- NA
shapes_basic_Part_ExpManage <- NA
shapes_basic_Part_ExpArea <- NA
shapes_basic_Part_University <- NA

for (i in 1:length(shapes_basic$Part_ID)) {
	shapes_basic_gender[i] <- x$Part_Gender[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_country[i] <- x$Part_country[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_language[i] <- x$Task_Lid[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==shapes_basic$Part_ID[i]][1]
	shapes_basic_Part_University[i] <- x$Part_University[x$Part_ID==shapes_basic$Part_ID[i]][1]
}
shapes_basic_gender
shapes_basic_BirthYear
shapes_basic_country
shapes_basic_language
shapes_basic_Part_EduTime
shapes_basic_Part_ExpTime
shapes_basic_Part_ExpManage
shapes_basic_Part_ExpArea
shapes_basic_Part_University

shapes_basic_bio <- as.data.frame(cbind(shapes_basic_gender,
				shapes_basic_BirthYear,
				shapes_basic_country,
				shapes_basic_language,
				shapes_basic_Part_EduTime,
				shapes_basic_Part_ExpTime,
				shapes_basic_Part_ExpManage,
				shapes_basic_Part_ExpArea,
				shapes_basic_Part_University
				))
names(shapes_basic_bio)

shapes_basic <- cbind(shapes_basic, shapes_basic_bio)
names(shapes_basic)
				
### shapes_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#103				shapes basic - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 103
Norm_ID <- ""
Norm_Name <- "shapes basic - fill in norm name here"
Scale <- as.numeric(1:15) 
Mean <- round(as.numeric(c(M103_01, M103_02, M103_03, M103_04, M103_05, M103_06, M103_07, M103_08, M103_09, M103_10, M103_11, M103_12, M103_13, M103_14, M103_15)),2)
sd <- round(c(SD103_01, SD103_02, SD103_03, SD103_04, SD103_05, SD103_06, SD103_07, SD103_08, SD103_09, SD103_10, SD103_11, SD103_12, SD103_13, SD103_14, SD103_15),2)
mSnine <- ""
Scale_Info <- c("Professional Challenge", "Identification", "Conscientiousness", "Creativity", "Circumspection", "Fun at Work", "Striving for Harmony",
"Sociable Skills", "Cooperation", "Autonomy", "Flexibility", "Recognition", "Self-efficacy", "Perseverance", "Keenness")

shapes_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
shapes_norms
names(shapes_norms)

# shapes bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
"languages", "country", "education time")

### age
shapes_basic_BirthYear <- ifelse(shapes_basic_BirthYear==0 , NA, shapes_basic_BirthYear)

age <- (current_year-shapes_basic_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
if (freq_age$Freq[i]>=0.05) {
	age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
	age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(shapes_basic$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

#class(shapes_basic_gender)
#table(shapes_basic_gender)
#table(shapes_basic_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(shapes_basic_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(shapes_basic_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(shapes_basic_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$shapes_basic_Part_University==-1]

shapes_basic_Part_University <- ifelse(shapes_basic_Part_University==-1 , NA, shapes_basic_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(shapes_basic_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$shapes_basic_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(shapes_basic_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$shapes_basic_Part_EduTime==0]

shapes_basic_Part_EduTime <- ifelse(shapes_basic_Part_EduTime==0 , NA, shapes_basic_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(shapes_basic_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$shapes_basic_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$shapes_basic_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt
					
# experience time
				freq_ExpTime <- as.data.frame(prop.table(table(shapes_basic_Part_ExpTime)))
				freq_ExpTime
				ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$shapes_basic_Part_ExpTime==0]
				
				shapes_basic_Part_ExpTime <- ifelse(shapes_basic_Part_ExpTime==0 , NA, shapes_basic_Part_ExpTime)
				freq_ExpTime <- as.data.frame(prop.table(table(shapes_basic_Part_ExpTime)))
				
				ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpTime$shapes_basic_Part_ExpTime)){
					if (freq_ExpTime$Freq[i]>=0.05) {
						ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$shapes_basic_Part_ExpTime[i]))],";  others < 5%")
					}
				}
	
				work_exp <- ExpTime_txt
							
# management experience
				freq_ExpManage <- as.data.frame(prop.table(table(shapes_basic_Part_ExpManage)))
				freq_ExpManage
				ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$shapes_basic_Part_ExpManage==0]
				
				shapes_basic_Part_ExpManage <- ifelse(shapes_basic_Part_ExpManage==0 , NA, shapes_basic_Part_ExpManage)
				freq_ExpManage <- as.data.frame(prop.table(table(shapes_basic_Part_ExpManage)))
				
				ExpManage_txt <- paste0(round(ExpManage_na,4)*100,"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpManage$shapes_basic_Part_ExpManage)){
					if (freq_ExpManage$Freq[i]>=0.05) {
						ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i],4)*100, "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$shapes_basic_Part_ExpManage[i]))],"; others < 5%")
					}
				}
				
				manage_exp <- ExpManage_txt	
				
# exp area

freq_ExpArea <- as.data.frame(prop.table(table(shapes_basic_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$shapes_basic_Part_ExpArea==0]

shapes_basic_Part_ExpArea <- ifelse(shapes_basic_Part_ExpArea==0 , NA, shapes_basic_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(shapes_basic_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na,4)*100,"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$shapes_basic_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$shapes_basic_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(shapes_basic_language)))
language_txt <- ""
for (i in 1:length(freq_language$shapes_basic_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$shapes_basic_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(shapes_basic_country)))
freq_country
country_na <- freq_country$Freq[freq_country$shapes_basic_country==0]

shapes_basic_country <- ifelse(shapes_basic_country==0 , NA, shapes_basic_country)

freq_country <- as.data.frame(prop.table(table(shapes_basic_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$shapes_basic_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$shapes_basic_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$shapes_basic_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

shapes_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\shapes_basic.xlsx", shapes_basic, shapes_basic_deleted, shapes_basic_norms, shapes_basic_bio)

}

#########################################################
#### 305 clues

if (length(X305_active) >0) {

clues <- as.data.frame(cbind(Part_ID, 
				X305_active,
				X305_targeted,
				X305_accurate,
				X305_complete
		)) 

names(clues)


# data clearing

# remove rows with no results for clues
length(clues$Part_ID)
clues <- na.omit(clues)
length(clues$Part_ID)

# remove rows with very low accuracy
del_count <- 0
clues_deleted <- ""

for (i in 1:length(clues$Part_ID)) {
	if (clues$X305_active[i]  == 0 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", clues$X305_active[i] , ", Part_ID:", clues$Part_ID[i] , " will be deleted from dataset"))
		clues_deleted<-rbind(clues_deleted,clues[i,1:5])
		clues$X305_active[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with active == 0 will be deleted from dataset"))
clues <- na.omit(clues)
length(clues$Part_ID)

#
clues_deleted_txt <- as.data.frame(cbind("thresholds for deletion","act == 0","","","")) # only once for a test 
names(clues_deleted_txt) <- c("Part_ID", "X305_active", "X305_targeted", "X305_accurate", "X305_complete")
clues_deleted <- rbind(clues_deleted_txt,clues_deleted)

# Stanine
M305_01 <- mean(clues$X305_active, na.rm=TRUE)
M305_02 <- mean(clues$X305_targeted, na.rm=TRUE)
M305_03 <- mean(clues$X305_accurate, na.rm=TRUE)
M305_04 <- mean(clues$X305_complete, na.rm=TRUE)

SD305_01 <- sd(clues$X305_active, na.rm=TRUE)
SD305_02 <- sd(clues$X305_targeted, na.rm=TRUE)
SD305_03 <- sd(clues$X305_accurate, na.rm=TRUE)
SD305_04 <- sd(clues$X305_complete, na.rm=TRUE)

X305_active_Stanine <- round(ifelse((clues$X305_active - M305_01)/SD305_01*2+5>9 , 9, ifelse((clues$X305_active - M305_01)/SD305_01*2+5<1, 1, (clues$X305_active - M305_01)/SD305_01*2+5)))
X305_targeted_Stanine <- round(ifelse((clues$X305_targeted - M305_02)/SD305_02*2+5>9 , 9, ifelse((clues$X305_targeted - M305_02)/SD305_02*2+5<1, 1, (clues$X305_targeted - M305_02)/SD305_02*2+5)))
X305_accurate_Stanine <- round(ifelse((clues$X305_accurate - M305_03)/SD305_03*2+5>9 , 9, ifelse((clues$X305_accurate - M305_03)/SD305_03*2+5<1, 1, (clues$X305_accurate - M305_03)/SD305_03*2+5)))
X305_complete_Stanine <- round(ifelse((clues$X305_complete - M305_04)/SD305_04*2+5>9 , 9, ifelse((clues$X305_complete - M305_04)/SD305_04*2+5<1, 1, (clues$X305_complete - M305_04)/SD305_04*2+5)))

clues <- cbind(clues,
		X305_active_Stanine,
		X305_targeted_Stanine, 
		X305_accurate_Stanine,
		X305_complete_Stanine 
)

colnames(clues)
class(clues)
clues <- as.data.frame(clues)
names(clues)

barplot(prop.table(table(X305_active_Stanine)), col=c.blue, xlab="clues Stanine active", ylab="percent/100")
barplot(prop.table(table(X305_targeted_Stanine)), col=c.blue, xlab="clues Stanine targeted", ylab="percent/100")
barplot(prop.table(table(X305_accurate_Stanine)), col=c.blue, xlab="clues Stanine accurate", ylab="percent/100")
barplot(prop.table(table(X305_complete_Stanine)), col=c.blue, xlab="clues Stanine complete", ylab="percent/100")


# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

clues_gender <- NA
clues_BirthYear <- NA
clues_country <- NA
clues_language <- NA
clues_Part_EduTime <- NA
clues_Part_ExpTime <- NA
clues_Part_ExpManage <- NA
clues_Part_ExpArea <- NA
clues_Part_University <- NA

for (i in 1:length(clues$Part_ID)) {
	clues_gender[i] <- x$Part_Gender[x$Part_ID==clues$Part_ID[i]][1]
	clues_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==clues$Part_ID[i]][1]
	clues_country[i] <- x$Part_country[x$Part_ID==clues$Part_ID[i]][1]
	clues_language[i] <- x$Task_Lid[x$Part_ID==clues$Part_ID[i]][1]
	clues_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==clues$Part_ID[i]][1]
	clues_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==clues$Part_ID[i]][1]
	clues_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==clues$Part_ID[i]][1]
	clues_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==clues$Part_ID[i]][1]
	clues_Part_University[i] <- x$Part_University[x$Part_ID==clues$Part_ID[i]][1]
}

clues_gender
clues_BirthYear
clues_country
clues_language
clues_Part_EduTime
clues_Part_ExpTime
clues_Part_ExpManage
clues_Part_ExpArea
clues_Part_University

clues_bio <- as.data.frame(cbind(clues_gender,
				clues_BirthYear,
				clues_country,
				clues_language,
				clues_Part_EduTime,
				clues_Part_ExpTime,
				clues_Part_ExpManage,
				clues_Part_ExpArea,
				clues_Part_University
		))
names(clues_bio)

clues <- cbind(clues, clues_bio)
names(clues)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 305
Norm_ID <- ""
Norm_Name <- "clues"
Scale <- as.numeric(1:4) 
Mean <- round(as.numeric(c(M305_01, M305_02, M305_03, M305_04)),2)
sd <- round(c(SD305_01, SD305_02, SD305_03, SD305_04),2)
mSnine <- ""
Scale_Info <- c("active", "targeted", "accurate","complete")

clues_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
clues_norms
names(clues_norms)

### age
clues_BirthYear <- ifelse(clues_BirthYear==0 , NA, clues_BirthYear)

age <- (current_year-clues_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(clues$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(clues_gender)
table(clues_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(clues_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(clues_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$clues_Part_University==-1]

clues_Part_University <- ifelse(clues_Part_University==-1 , NA, clues_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(clues_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$clues_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(clues_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$clues_Part_EduTime==0]

clues_Part_EduTime <- ifelse(clues_Part_EduTime==0 , NA, clues_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(clues_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$clues_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$clues_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(clues_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$clues_Part_ExpTime==0]

clues_Part_ExpTime <- ifelse(clues_Part_ExpTime==0 , NA, clues_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(clues_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$clues_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$clues_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(clues_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$clues_Part_ExpManage==0]

clues_Part_ExpManage <- ifelse(clues_Part_ExpManage==0 , NA, clues_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(clues_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$clues_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$clues_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(clues_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$clues_Part_ExpArea==0]

clues_Part_ExpArea <- ifelse(clues_Part_ExpArea==0 , NA, clues_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(clues_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$clues_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$clues_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(clues_language)))
language_txt <- ""
for (i in 1:length(freq_language$clues_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$clues_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(clues_country)))
freq_country
country_na <- freq_country$Freq[freq_country$clues_country==0]

clues_country <- ifelse(clues_country==0 , NA, clues_country)

freq_country <- as.data.frame(prop.table(table(clues_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$clues_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$clues_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

clues_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\clues.xlsx", clues, clues_deleted, clues_norms, clues_bio)
}

#########################################################
#### 311 numerical (consumer)

if (length(X311_overallperformance) > 0 ) {
num_cons <- as.data.frame(cbind(Part_ID, 
				X311_overallperformance,
				X311_speed,
				X311_accuracy
						)) 

names(num_cons)


# data clearing

# remove rows with no results for numerical
length(num_cons$Part_ID)
num_cons <- na.omit(num_cons)
length(num_cons$Part_ID)

# remove rows with very low accuracy
del_count <- 0
num_cons_deleted <- ""

for (i in 1:length(num_cons$Part_ID)) {
	if (num_cons$X311_accuracy[i]  <= 33 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", num_cons$X311_accuracy[i] , ", Part_ID:", num_cons$Part_ID[i] , " will be deleted from dataset"))
		num_cons_deleted<-rbind(num_cons_deleted,num_cons[i,1:4])
		num_cons$X311_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with accuracy <= 33 will be deleted from dataset"))
num_cons <- na.omit(num_cons)
length(num_cons$Part_ID)

#
num_cons_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= ","acc <= 33")) # only once for a test 
names(num_cons_deleted_txt) <- c("Part_ID", "X311_overallperformance", "X311_speed", "X311_accuracy")
num_cons_deleted <- rbind(num_cons_deleted_txt,num_cons_deleted)

# Stanine
M311_01 <- mean(num_cons$X311_overallperformance, na.rm=TRUE)
M311_02 <- mean(num_cons$X311_speed, na.rm=TRUE)
M311_03 <- mean(num_cons$X311_accuracy, na.rm=TRUE)

SD311_01 <- sd(num_cons$X311_overallperformance, na.rm=TRUE)
SD311_02 <- sd(num_cons$X311_speed, na.rm=TRUE)
SD311_03 <- sd(num_cons$X311_accuracy, na.rm=TRUE)

X311_overallperformance_Stanine <- round(ifelse((num_cons$X311_overallperformance- M311_01)/SD311_01*2+5>9 , 9, ifelse((num_cons$X311_overallperformance- M311_01)/SD311_01*2+5<1, 1, (num_cons$X311_overallperformance- M311_01)/SD311_01*2+5)))
X311_speed_Stanine <- round(ifelse((num_cons$X311_speed - M311_02)/SD311_02*2+5>9 , 9, ifelse((num_cons$X311_speed - M311_02)/SD311_01*2+5<1, 1, (num_cons$X311_speed - M311_02)/SD311_02*2+5)))
X311_accuracy_Stanine <- round(ifelse((num_cons$X311_accuracy- M311_03)/SD311_03*2+5>9 , 9, ifelse((num_cons$X311_accuracy- M311_03)/SD311_01*2+5<1, 1, (num_cons$X311_accuracy- M311_03)/SD311_03*2+5)))

num_cons <- cbind(num_cons,
		X311_overallperformance_Stanine,
		X311_speed_Stanine, 
		X311_accuracy_Stanine 		
)

colnames(num_cons)
class(num_cons)
num_cons <- as.data.frame(num_cons)
names(num_cons)

barplot(prop.table(table(X311_overallperformance_Stanine)), col=c.blue, xlab="numerical (consumer) Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X311_speed_Stanine)), col=c.blue, xlab="numerical (consumer) Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X311_accuracy_Stanine)), col=c.blue, xlab="numerical (consumer) Stanine Accuracy", ylab="percent/100")

# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

num_cons_gender <- NA
num_cons_BirthYear <- NA
num_cons_country <- NA
num_cons_language <- NA
num_cons_Part_EduTime <- NA
num_cons_Part_ExpTime <- NA
num_cons_Part_ExpManage <- NA
num_cons_Part_ExpArea <- NA
num_cons_Part_University <- NA

for (i in 1:length(num_cons$Part_ID)) {
	num_cons_gender[i] <- x$Part_Gender[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_country[i] <- x$Part_country[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_language[i] <- x$Task_Lid[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==num_cons$Part_ID[i]][1]
	num_cons_Part_University[i] <- x$Part_University[x$Part_ID==num_cons$Part_ID[i]][1]
}

num_cons_gender
num_cons_BirthYear
num_cons_country
num_cons_language
num_cons_Part_EduTime
num_cons_Part_ExpTime
num_cons_Part_ExpManage
num_cons_Part_ExpArea
num_cons_Part_University

num_cons_bio <- as.data.frame(cbind(num_cons_gender,
				num_cons_BirthYear,
				num_cons_country,
				num_cons_language,
				num_cons_Part_EduTime,
				num_cons_Part_ExpTime,
				num_cons_Part_ExpManage,
				num_cons_Part_ExpArea,
				num_cons_Part_University
		))
names(num_cons_bio)

num_cons <- cbind(num_cons, num_cons_bio)
names(num_cons)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 311
Norm_ID <- ""
Norm_Name <- "numerical (consumer)"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M311_01, M311_02, M311_03)),2)
sd <- round(c(SD311_01, SD311_02, SD311_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

num_cons_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
num_cons_norms
names(num_cons_norms)

### age
num_cons_BirthYear <- ifelse(num_cons_BirthYear==0 , NA, num_cons_BirthYear)

age <- (current_year-num_cons_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(num_cons$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(num_cons_gender)
table(num_cons_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(num_cons_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(num_cons_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$num_cons_Part_University==-1]

num_cons_Part_University <- ifelse(num_cons_Part_University==-1 , NA, num_cons_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(num_cons_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$num_cons_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(num_cons_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$num_cons_Part_EduTime==0]

num_cons_Part_EduTime <- ifelse(num_cons_Part_EduTime==0 , NA, num_cons_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(num_cons_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$num_cons_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$num_cons_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(num_cons_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$num_cons_Part_ExpTime==0]

num_cons_Part_ExpTime <- ifelse(num_cons_Part_ExpTime==0 , NA, num_cons_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(num_cons_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$num_cons_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$num_cons_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(num_cons_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$num_cons_Part_ExpManage==0]

num_cons_Part_ExpManage <- ifelse(num_cons_Part_ExpManage==0 , NA, num_cons_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(num_cons_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$num_cons_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$num_cons_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(num_cons_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$num_cons_Part_ExpArea==0]

num_cons_Part_ExpArea <- ifelse(num_cons_Part_ExpArea==0 , NA, num_cons_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(num_cons_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$num_cons_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$num_cons_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(num_cons_language)))
language_txt <- ""
for (i in 1:length(freq_language$num_cons_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$num_cons_language[i]))],"; others < 5%")
	}
}

language <- language_txt


# country

freq_country <- as.data.frame(prop.table(table(num_cons_country)))
freq_country
country_na <- freq_country$Freq[freq_country$num_cons_country==0]

num_cons_country <- ifelse(num_cons_country==0 , NA, num_cons_country)

freq_country <- as.data.frame(prop.table(table(num_cons_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$num_cons_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$num_cons_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

num_cons_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\num_cons.xlsx", num_cons, num_cons_deleted, num_cons_norms, num_cons_bio)

#########################################################
#### 312 verbal (consumer)

verbal_cons <- as.data.frame(cbind(Part_ID, 
				X312_overallperformance,
				X312_speed,
				X312_accuracy
		)) 

names(verbal_cons)


# data clearing

# remove rows with no results for numerical
length(verbal_cons$Part_ID)
verbal_cons <- na.omit(verbal_cons)
length(verbal_cons$Part_ID)

# remove rows with very low accuracy
del_count <- 0
verbal_cons_deleted <- ""

for (i in 1:length(verbal_cons$Part_ID)) {
	if (verbal_cons$X312_accuracy[i]  <= 33 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", verbal_cons$X312_accuracy[i] , ", Part_ID:", verbal_cons$Part_ID[i] , " will be deleted from dataset"))
		verbal_cons_deleted<-rbind(verbal_cons_deleted,verbal_cons[i,1:4])
		verbal_cons$X312_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with accuracy <= 33 will be deleted from dataset"))
verbal_cons <- na.omit(verbal_cons)
length(verbal_cons$Part_ID)

#
verbal_cons_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= ","acc <= 33")) # only once for a test 
names(verbal_cons_deleted_txt) <- c("Part_ID", "X312_overallperformance", "X312_speed", "X312_accuracy")
verbal_cons_deleted <- rbind(verbal_cons_deleted_txt,verbal_cons_deleted)

# Stanine
M312_01 <- mean(verbal_cons$X312_overallperformance, na.rm=TRUE)
M312_02 <- mean(verbal_cons$X312_speed, na.rm=TRUE)
M312_03 <- mean(verbal_cons$X312_accuracy, na.rm=TRUE)

SD312_01 <- sd(verbal_cons$X312_overallperformance, na.rm=TRUE)
SD312_02 <- sd(verbal_cons$X312_speed, na.rm=TRUE)
SD312_03 <- sd(verbal_cons$X312_accuracy, na.rm=TRUE)

X312_overallperformance_Stanine <- round(ifelse((verbal_cons$X312_overallperformance- M312_01)/SD312_01*2+5>9 , 9, ifelse((verbal_cons$X312_overallperformance- M312_01)/SD312_01*2+5<1, 1, (verbal_cons$X312_overallperformance- M312_01)/SD312_01*2+5)))
X312_speed_Stanine <- round(ifelse((verbal_cons$X312_speed - M312_02)/SD312_02*2+5>9 , 9, ifelse((verbal_cons$X312_speed - M312_02)/SD312_01*2+5<1, 1, (verbal_cons$X312_speed - M312_02)/SD312_02*2+5)))
X312_accuracy_Stanine <- round(ifelse((verbal_cons$X312_accuracy- M312_03)/SD312_03*2+5>9 , 9, ifelse((verbal_cons$X312_accuracy- M312_03)/SD312_01*2+5<1, 1, (verbal_cons$X312_accuracy- M312_03)/SD312_03*2+5)))

verbal_cons <- cbind(verbal_cons,
		X312_overallperformance_Stanine,
		X312_speed_Stanine, 
		X312_accuracy_Stanine 		
)

colnames(verbal_cons)
class(verbal_cons)
verbal_cons <- as.data.frame(verbal_cons)
names(verbal_cons)

barplot(prop.table(table(X312_overallperformance_Stanine)), col=c.blue, xlab="verbal (consumer) Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X312_speed_Stanine)), col=c.blue, xlab="verbal (consumer) Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X312_accuracy_Stanine)), col=c.blue, xlab="verbal (consumer) Stanine Accuracy", ylab="percent/100")

#############
# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

verbal_cons_gender <- NA
verbal_cons_BirthYear <- NA
verbal_cons_country <- NA
verbal_cons_language <- NA
verbal_cons_Part_EduTime <- NA
verbal_cons_Part_ExpTime <- NA
verbal_cons_Part_ExpManage <- NA
verbal_cons_Part_ExpArea <- NA
verbal_cons_Part_University <- NA

for (i in 1:length(verbal_cons$Part_ID)) {
	verbal_cons_gender[i] <- x$Part_Gender[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_country[i] <- x$Part_country[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_language[i] <- x$Task_Lid[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==verbal_cons$Part_ID[i]][1]
	verbal_cons_Part_University[i] <- x$Part_University[x$Part_ID==verbal_cons$Part_ID[i]][1]
}
verbal_cons_gender
verbal_cons_BirthYear
verbal_cons_country
verbal_cons_language
verbal_cons_Part_EduTime
verbal_cons_Part_ExpTime
verbal_cons_Part_ExpManage
verbal_cons_Part_ExpArea
verbal_cons_Part_University

verbal_cons_bio <- as.data.frame(cbind(verbal_cons_gender,
				verbal_cons_BirthYear,
				verbal_cons_country,
				verbal_cons_language,
				verbal_cons_Part_EduTime,
				verbal_cons_Part_ExpTime,
				verbal_cons_Part_ExpManage,
				verbal_cons_Part_ExpArea,
				verbal_cons_Part_University
		))
names(verbal_cons_bio)

verbal_cons <- cbind(verbal_cons, verbal_cons_bio)
names(verbal_cons)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 312
Norm_ID <- ""
Norm_Name <- "verbal (consumer)"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M312_01, M312_02, M312_03)),2)
sd <- round(c(SD312_01, SD312_02, SD312_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

verbal_cons_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
verbal_cons_norms
names(verbal_cons_norms)

### age
verbal_cons_BirthYear <- ifelse(verbal_cons_BirthYear==0 , NA, verbal_cons_BirthYear)

age <- (current_year-verbal_cons_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(verbal_cons$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(verbal_cons_gender)
table(verbal_cons_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(verbal_cons_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(verbal_cons_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$verbal_cons_Part_University==-1]

verbal_cons_Part_University <- ifelse(verbal_cons_Part_University==-1 , NA, verbal_cons_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(verbal_cons_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$verbal_cons_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(verbal_cons_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$verbal_cons_Part_EduTime==0]

verbal_cons_Part_EduTime <- ifelse(verbal_cons_Part_EduTime==0 , NA, verbal_cons_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(verbal_cons_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$verbal_cons_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$verbal_cons_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(verbal_cons_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$verbal_cons_Part_ExpTime==0]

verbal_cons_Part_ExpTime <- ifelse(verbal_cons_Part_ExpTime==0 , NA, verbal_cons_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(verbal_cons_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$verbal_cons_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$verbal_cons_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(verbal_cons_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$verbal_cons_Part_ExpManage==0]

verbal_cons_Part_ExpManage <- ifelse(verbal_cons_Part_ExpManage==0 , NA, verbal_cons_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(verbal_cons_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$verbal_cons_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$verbal_cons_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(verbal_cons_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$verbal_cons_Part_ExpArea==0]

verbal_cons_Part_ExpArea <- ifelse(verbal_cons_Part_ExpArea==0 , NA, verbal_cons_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(verbal_cons_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$verbal_cons_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$verbal_cons_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(verbal_cons_language)))
language_txt <- ""
for (i in 1:length(freq_language$verbal_cons_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$verbal_cons_language[i]))],"; others < 5%")
	}
}

language <- language_txt
# country

freq_country <- as.data.frame(prop.table(table(verbal_cons_country)))
freq_country
country_na <- freq_country$Freq[freq_country$verbal_cons_country==0]

verbal_cons_country <- ifelse(verbal_cons_country==0 , NA, verbal_cons_country)

freq_country <- as.data.frame(prop.table(table(verbal_cons_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$verbal_cons_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$verbal_cons_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

verbal_cons_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\verbal_cons.xlsx", verbal_cons, verbal_cons_deleted, verbal_cons_norms, verbal_cons_bio)
}

##############################################
# 316 lt-d

if (length(X316_fluencyraw) > 0) {

lt_d <- as.data.frame(cbind(Part_ID, 
				X316_fluencyraw,
				X316_fluencyspeed,
				X316_fluencyaccuracy, 				
				X316_spellingraw, 
				X316_spellingspeed,
				X316_spellingaccuracy,				
				X316_vocabraw, 
				X316_vocabspeed,
				X316_vocabaccuracy
		)) 

names(lt_d)

# Stanine
M316_01 <- mean(X316_fluencyaccuracy, na.rm=TRUE)
M316_02 <- mean(X316_fluencyraw, na.rm=TRUE)
M316_03 <- mean(X316_fluencyspeed, na.rm=TRUE)
M316_04 <- mean(X316_spellingaccuracy, na.rm=TRUE)
M316_05 <- mean(X316_spellingraw, na.rm=TRUE)
M316_06 <- mean(X316_spellingspeed, na.rm=TRUE)
M316_07 <- mean(X316_vocabaccuracy, na.rm=TRUE)
M316_08 <- mean(X316_vocabraw, na.rm=TRUE)
M316_09 <- mean(X316_vocabspeed, na.rm=TRUE)

SD316_01 <- sd(X316_fluencyaccuracy, na.rm=TRUE)
SD316_02 <- sd(X316_fluencyraw, na.rm=TRUE)
SD316_03 <- sd(X316_fluencyspeed, na.rm=TRUE)
SD316_04 <- sd(X316_spellingaccuracy, na.rm=TRUE)
SD316_05 <- sd(X316_spellingraw, na.rm=TRUE)
SD316_06 <- sd(X316_spellingspeed, na.rm=TRUE)
SD316_07 <- sd(X316_vocabaccuracy, na.rm=TRUE)
SD316_08 <- sd(X316_vocabraw, na.rm=TRUE)
SD316_09 <- sd(X316_vocabspeed, na.rm=TRUE)

X316_fluencyaccuracy_Stanine <- round(ifelse((X316_fluencyaccuracy - M316_01)/SD316_01*2+5>9 , 9, ifelse((X316_fluencyaccuracy - M316_01)/SD316_01*2+5<1, 1, (X316_fluencyaccuracy - M316_01)/SD316_01*2+5)))
X316_fluencyraw_Stanine <- round(ifelse((X316_fluencyraw - M316_02)/SD316_02*2+5>9 , 9, ifelse((X316_fluencyraw - M316_02)/SD316_01*2+5<1, 1, (X316_fluencyraw - M316_02)/SD316_02*2+5)))
X316_fluencyspeed_Stanine <- round(ifelse((X316_fluencyspeed - M316_03)/SD316_03*2+5>9 , 9, ifelse((X316_fluencyspeed - M316_03)/SD316_01*2+5<1, 1, (X316_fluencyspeed - M316_03)/SD316_03*2+5)))
X316_spellingaccuracy_Stanine <- round(ifelse((X316_spellingaccuracy - M316_04)/SD316_04*2+5>9 , 9, ifelse((X316_spellingaccuracy - M316_04)/SD316_01*2+5<1, 1, (X316_spellingaccuracy - M316_04)/SD316_04*2+5)))
X316_spellingraw_Stanine <- round(ifelse((X316_spellingraw - M316_05)/SD316_05*2+5>9 , 9, ifelse((X316_spellingraw - M316_05)/SD316_01*2+5<1, 1, (X316_spellingraw - M316_05)/SD316_05*2+5)))
X316_spellingspeed_Stanine <- round(ifelse((X316_spellingspeed - M316_06)/SD316_06*2+5>9 , 9, ifelse((X316_spellingspeed - M316_06)/SD316_01*2+5<1, 1, (X316_spellingspeed - M316_06)/SD316_06*2+5)))
X316_vocabaccuracy_Stanine <- round(ifelse((X316_vocabaccuracy - M316_07)/SD316_07*2+5>9 , 9, ifelse((X316_vocabaccuracy - M316_07)/SD316_01*2+5<1, 1, (X316_vocabaccuracy - M316_07)/SD316_07*2+5)))
X316_vocabraw_Stanine <- round(ifelse((X316_vocabraw - M316_08)/SD316_08*2+5>9 , 9, ifelse((X316_vocabraw - M316_08)/SD316_01*2+5<1, 1, (X316_vocabraw - M316_08)/SD316_08*2+5)))
X316_vocabspeed_Stanine <- round(ifelse((X316_vocabspeed - M316_09)/SD316_09*2+5>9 , 9, ifelse((X316_vocabspeed - M316_09)/SD316_01*2+5<1, 1, (X316_vocabspeed - M316_09)/SD316_09*2+5)))

lt_d <- cbind(lt_d,
		X316_fluencyraw_Stanine,
		X316_fluencyspeed_Stanine, 
		X316_fluencyaccuracy_Stanine, 
		X316_spellingraw_Stanine, 
		X316_spellingspeed_Stanine, 
		X316_spellingaccuracy_Stanine, 
		X316_vocabraw_Stanine, 
		X316_vocabspeed_Stanine, 
		X316_vocabaccuracy_Stanine
)

colnames(lt_d)
class(lt_d)
lt_d <- as.data.frame(lt_d)
names(lt_d)

barplot(prop.table(table(X316_fluencyraw_Stanine)), col=c.blue, xlab="Stanine Fluencyraw", ylab="percent/100")
barplot(prop.table(table(X316_fluencyspeed_Stanine)), col=c.blue, xlab="Stanine Fluency Speech", ylab="percent/100")
barplot(prop.table(table(X316_fluencyaccuracy_Stanine)), col=c.blue, xlab="Stanine Fluency Accuracy", ylab="percent/100")

barplot(prop.table(table(X316_spellingraw_Stanine)), col=c.blue, xlab="Stanine Spellingraw", ylab="percent/100")
barplot(prop.table(table(X316_spellingspeed_Stanine)), col=c.blue, xlab="Stanine Spelling Speed", ylab="percent/100")
barplot(prop.table(table(X316_spellingaccuracy_Stanine)), col=c.blue, xlab="Stanine Spelling Accuracy", ylab="percent/100")

barplot(prop.table(table(X316_vocabraw_Stanine)), col=c.blue, xlab="Stanine Vocabulary Raw", ylab="percent/100")
barplot(prop.table(table(X316_vocabspeed_Stanine)), col=c.blue, xlab="Stanine Vocabulary Speed", ylab="percent/100")
barplot(prop.table(table(X316_vocabaccuracy_Stanine)), col=c.blue, xlab="Stanine Vocabulary Accuracy", ylab="percent/100")

# add biodata to lt_d
#gender
# BirthYear
#country
# education time
# experience time
# management experience time
# Experience area
# university degree

lt_d_gender <- NA
lt_d_BirthYear <- NA
lt_d_country <- NA
lt_d_language <- NA
lt_d_EduTime <- NA
lt_d_Part_ExpTime <- NA
lt_d_Part_ExpManage <- NA
lt_d_Part_ExpArea <- NA
lt_d_Part_University <- NA

for (i in 1:length(lt_d$Part_ID)) {
	lt_d_gender[i] <- x$Part_Gender[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_country[i] <- x$Part_country[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_language[i] <- x$Task_Lid[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_EduTime[i] <- x$Part_EduTime[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==lt_d$Part_ID[i]][1]
	lt_d_Part_University[i] <- x$Part_University[x$Part_ID==lt_d$Part_ID[i]][1]
}
lt_d_gender
lt_d_BirthYear
lt_d_country
lt_d_language
lt_d_EduTime
lt_d_Part_ExpTime
lt_d_Part_ExpManage
lt_d_Part_ExpArea
lt_d_Part_University

lt_d_bio <- as.data.frame(cbind(lt_d_gender,
				lt_d_BirthYear,
				lt_d_country,
				lt_d_language,
				lt_d_EduTime,
				lt_d_Part_ExpTime,
				lt_d_Part_ExpManage,
				lt_d_Part_ExpArea,
				lt_d_Part_University
		))
names(lt_d_bio)

lt_d <- cbind(lt_d, lt_d_bio)
names(lt_d)

### lt_d_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#316				shapes basic - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 316
Norm_ID <- ""
Norm_Name <- "lt-d - fill in norm name here"
Scale <- as.numeric(1:9) 
Mean <- round(as.numeric(c(M316_01, M316_02, M316_03, M316_04, M316_05, M316_06, M316_07, M316_08, M316_09)),2)
sd <- round(c(SD316_01, SD316_02, SD316_03, SD316_04, SD316_05, SD316_06, SD316_07, SD316_08, SD316_09),2)
mSnine <- ""
Scale_Info <- c("fluency accuracy", "fluency raw", "fluency speed", "spelling accuracy", "spelling raw", "spelling speed", "vocabulary accuracy",
		"vocabulary raw", "vocabulary speed")

lt_d_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
lt_d_norms
names(lt_d_norms)

### age
lt_d_BirthYear <- ifelse(shapes_basic_BirthYear==0 , NA, shapes_basic_BirthYear)

age <- (current_year-lt_d_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(lt_d$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(lt_d_gender)
table(lt_d_gender)
#table(lt_d_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(lt_d_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(lt_d_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(lt_d_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$lt_d_Part_University==-1]

lt_d_Part_University <- ifelse(lt_d_Part_University==-1 , NA, lt_d_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(lt_d_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$lt_d_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(lt_d_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$lt_d_Part_EduTime==0]

lt_d_Part_EduTime <- ifelse(lt_d_Part_EduTime==0 , NA, lt_d_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(lt_d_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$lt_d_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$lt_d_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(lt_d_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$lt_d_Part_ExpTime==0]

lt_d_Part_ExpTime <- ifelse(lt_d_Part_ExpTime==0 , NA, lt_d_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(lt_d_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$lt_d_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$lt_d_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(lt_d_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$lt_d_Part_ExpManage==0]

lt_d_Part_ExpManage <- ifelse(lt_d_Part_ExpManage==0 , NA, lt_d_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(shapes_lt_d_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$lt_d_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$lt_d_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(lt_d_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$lt_d_Part_ExpArea==0]

lt_d_Part_ExpArea <- ifelse(lt_d_Part_ExpArea==0 , NA, lt_d_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(lt_d_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$lt_d_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$lt_d_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(lt_d_language)))
language_txt <- ""
for (i in 1:length(freq_language$lt_d_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$lt_d_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(lt_d_country)))
freq_country
country_na <- freq_country$Freq[freq_country$lt_d_country==0]

lt_d_country <- ifelse(lt_d_country==0 , NA, lt_d_country)

freq_country <- as.data.frame(prop.table(table(lt_d_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$lt_d_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$lt_d_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$lt_d_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt


# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)


lt_d_bio <- as.data.frame(cbind(bio_titles,bio_info))


# save data, each test to a separate sheet
save.xlsx("_data\\lt_d.xlsx", lt_d, lt_d_deleted, lt_d_norms, lt_d_bio)
}


#########################################################
#### e3+

if (length(X323_overallperformance) > 0 ) {

e3plus <- as.data.frame(cbind(Part_ID, 
				X323_overallperformance,
				X323_speed,
				X323_accuracy)) 

names(e3plus)
length(e3plus$Part_ID)

# data clearing

# remove rows with no results for e3+
e3plus <- na.omit(e3plus)
length(e3plus$Part_ID)

# remove rows with very low accuracy
del_count <- 0
e3plus_deleted <- ""

for (i in 1:length(e3plus$Part_ID)) {
	if (e3plus$X323_accuracy[i]  <= 25 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", e3plus$X323_accuracy[i] , ", Part_ID:", e3plus$Part_ID[i] , " will be deleted from dataset"))
		e3plus_deleted<-rbind(e3plus_deleted,e3plus[i,1:4])
		e3plus$X323_accuracy[i]<- NA # remove value
		}
}
print(paste0(del_count, " cases with accuracy <= 25 will be deleted from dataset"))
e3plus <- na.omit(e3plus)
length(e3plus$Part_ID)

# remove rows with very low speed
del_count <- 0
for (i in 1:length(e3plus$Part_ID)) {
	if (e3plus$X323_speed[i]  <= 15 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("speed low: ", e3plus$X323_speed[i] , ", Part_ID:", e3plus$Part_ID[i] , " will be deleted from dataset"))
		e3plus_deleted<-rbind(e3plus_deleted,e3plus[i,])
		e3plus$X323_speed[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with speed <= 15 will be deleted from dataset"))
e3plus <- na.omit(e3plus)
length(e3plus$Part_ID)

#
e3plus_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= 15","acc <= 25")) # only once for a test 
names(e3plus_deleted_txt) <- c("Part_ID", "X323_overallperformance", "X323_speed", "X323_accuracy")
e3plus_deleted <- rbind(e3plus_deleted_txt,e3plus_deleted)

# Stanine
M323_01 <- mean(e3plus$X323_overallperformance, na.rm=TRUE)
M323_02 <- mean(e3plus$X323_speed, na.rm=TRUE)
M323_03 <- mean(e3plus$X323_accuracy, na.rm=TRUE)

SD323_01 <- sd(e3plus$X323_overallperformance, na.rm=TRUE)
SD323_02 <- sd(e3plus$X323_speed, na.rm=TRUE)
SD323_03 <- sd(e3plus$X323_accuracy, na.rm=TRUE)

X323_overallperformance_Stanine <- round(ifelse((e3plus$X323_overallperformance- M323_01)/SD323_01*2+5>9 , 9, ifelse((e3plus$X323_overallperformance- M323_01)/SD323_01*2+5<1, 1, (e3plus$X323_overallperformance- M323_01)/SD323_01*2+5)))
X323_speed_Stanine <- round(ifelse((e3plus$X323_speed - M323_02)/SD323_02*2+5>9 , 9, ifelse((e3plus$X323_speed - M323_02)/SD323_01*2+5<1, 1, (e3plus$X323_speed - M323_02)/SD323_02*2+5)))
X323_accuracy_Stanine <- round(ifelse((e3plus$X323_accuracy- M323_03)/SD323_03*2+5>9 , 9, ifelse((e3plus$X323_accuracy- M323_03)/SD323_01*2+5<1, 1, (e3plus$X323_accuracy- M323_03)/SD323_03*2+5)))

e3plus <- cbind(e3plus,
		X323_overallperformance_Stanine,
		X323_speed_Stanine, 
		X323_accuracy_Stanine 		
)

colnames(e3plus)
class(e3plus)
e3plus <- as.data.frame(e3plus)
names(e3plus)

barplot(prop.table(table(e3plus$X323_overallperformance_Stanine)), col=c.blue, xlab="e3+ Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(e3plus$X323_speed_Stanine)), col=c.blue, xlab="e3+ Stanine Speed", ylab="percent/100")
barplot(prop.table(table(e3plus$X323_accuracy_Stanine)), col=c.blue, xlab="e3+ Stanine Accuracy", ylab="percent/100")

# add biodata to scales_e3plus

# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

e3plus_gender <- NA
e3plus_BirthYear <- NA
e3plus_country <- NA
e3plus_language <- NA
e3plus_EduTime <- NA
e3plus_Part_ExpTime <- NA
e3plus_Part_ExpManage <- NA
e3plus_Part_ExpArea <- NA
e3plus_Part_University <- NA

for (i in 1:length(e3plus$Part_ID)) {
	e3plus_gender[i] <- x$Part_Gender[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_country[i] <- x$Part_country[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_language[i] <- x$Task_Lid[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_EduTime[i] <- x$Part_EduTime[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==e3plus$Part_ID[i]][1]
	e3plus_Part_University[i] <- x$Part_University[x$Part_ID==e3plus$Part_ID[i]][1]
}
e3plus_gender
e3plus_BirthYear
e3plus_country
e3plus_language
e3plus_EduTime
e3plus_Part_ExpTime
e3plus_Part_ExpManage
e3plus_Part_ExpArea
e3plus_Part_University

e3plus_bio <- as.data.frame(cbind(e3plus_gender,
				e3plus_BirthYear,
				e3plus_country,
				e3plus_language,
				e3plus_EduTime,
				e3plus_Part_ExpTime,
				e3plus_Part_ExpManage,
				e3plus_Part_ExpArea,
				e3plus_Part_University
		))
names(e3plus_bio)

e3plus <- cbind(e3plus, e3plus_bio)
names(e3plus)

### scales_e3plus_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#323				scales_e3plus - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 323
Norm_ID <- ""
Norm_Name <- "e3+ - fill in norm name here"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M323_01, M323_02, M323_03)),2)
sd <- round(c(SD323_01, SD323_02, SD323_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

e3plus_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
e3plus_norms
names(e3plus_norms)

# e3plus bio

### age
e3plus_BirthYear <- ifelse(e3plus_BirthYear==0 , NA, e3plus_BirthYear)

age <- (current_year-e3plus_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(e3plus$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(e3plus_gender)
table(e3plus_gender)

freq_gender <- as.data.frame(prop.table(table(e3plus_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(e3plus_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$e3plus_Part_University==-1]

e3plus_Part_University <- ifelse(e3plus_Part_University==-1 , NA, e3plus_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(e3plus_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$e3plus_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(e3plus_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$e3plus_EduTime==0]

e3plus_EduTime <- ifelse(e3plus_EduTime==0 , NA, e3plus_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(e3plus_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$e3plus_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$e3plus_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(e3plus_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$e3plus_Part_ExpTime==0]

e3plus_Part_ExpTime <- ifelse(e3plus_Part_ExpTime==0 , NA, e3plus_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(e3plus_Part_ExpTime)))

ExpTime_txt <- ""
ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$e3plus_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$e3plus_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(e3plus_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$e3plus_Part_ExpManage==0]

e3plus_Part_ExpManage <- ifelse(e3plus_Part_ExpManage==0 , NA, e3plus_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(e3plus_Part_ExpManage)))

ExpManage_txt <- ""
ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$e3plus_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$e3plus_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(e3plus_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$e3plus_Part_ExpArea==0]

e3plus_Part_ExpArea <- ifelse(e3plus_Part_ExpArea==0 , NA, e3plus_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(e3plus_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- ""
ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$e3plus_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$e3plus_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(e3plus_language)))
language_txt <- ""
for (i in 1:length(freq_language$e3plus_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$e3plus_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(e3plus_country)))
freq_country
country_na <- freq_country$Freq[freq_country$e3plus_country==0]

e3plus_country <- ifelse(e3plus_country==0 , NA, e3plus_country)

freq_country <- as.data.frame(prop.table(table(e3plus_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$e3plus_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$e3plus_country[i]))],"; others < 5%")
		}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

e3plus_bio <- as.data.frame(cbind(bio_titles,bio_info))

# save data
save.xlsx("_data\\e3plus.xlsx", e3plus, e3plus_deleted, e3plus_norms, e3plus_bio)

}

#########################################################
#### scales eql

if (length(X354_overallperformance) >0 ) {

eql <- as.data.frame(cbind(Part_ID, 
				X354_overallperformance,
				X354_speed,
				X354_accuracy, 				
		
		)) 

names(eql)

# Stanine
M354_01 <- mean(eql$X354_overallperformance, na.rm=TRUE)
M354_02 <- mean(eql$X354_speed, na.rm=TRUE)
M354_03 <- mean(eql$X354_accuracy, na.rm=TRUE)

SD354_01 <- sd(eql$X354_overallperformance, na.rm=TRUE)
SD354_02 <- sd(eql$X354_speed, na.rm=TRUE)
SD354_03 <- sd(eql$X354_accuracy, na.rm=TRUE)

X354_overallperformance_Stanine <- round(ifelse((eql$X354_overallperformance- M354_01)/SD354_01*2+5>9 , 9, ifelse((eql$X354_overallperformance- M354_01)/SD354_01*2+5<1, 1, (eql$X354_overallperformance- M354_01)/SD354_01*2+5)))
X354_speed_Stanine <- round(ifelse((eql$X354_speed - M354_02)/SD354_02*2+5>9 , 9, ifelse((eql$X354_speed - M354_02)/SD354_01*2+5<1, 1, (eql$X354_speed - M354_02)/SD354_02*2+5)))
X354_accuracy_Stanine <- round(ifelse((eql$X354_accuracy- M354_03)/SD354_03*2+5>9 , 9, ifelse((eql$X354_accuracy- M354_03)/SD354_01*2+5<1, 1, (eql$X354_accuracy- M354_03)/SD354_03*2+5)))

scales_eql <- cbind(eql,
		X354_overallperformance_Stanine,
		X354_speed_Stanine, 
		X354_accuracy_Stanine 		
)

colnames(eql)
class(eql)
eql <- as.data.frame(eql)
names(eql)

barplot(prop.table(table(X354_overallperformance_Stanine)), col=c.blue, xlab="eql Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X354_speed_Stanine)), col=c.blue, xlab="eql Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X354_accuracy_Stanine)), col=c.blue, xlab="eql Stanine Accuracy", ylab="percent/100")

# add biodata to scales_eql

# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

eql_gender <- NA
eql_BirthYear <- NA
eql_country <- NA
eql_language <- NA
eql_EduTime <- NA
eql_Part_ExpTime <- NA
eql_Part_ExpManage <- NA
eql_Part_ExpArea <- NA
eql_Part_University <- NA

for (i in 1:length(eql$Part_ID)) {
	eql_gender[i] <- x$Part_Gender[x$Part_ID==eql$Part_ID[i]][1]
	eql_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==eql$Part_ID[i]][1]
	eql_country[i] <- x$Part_country[x$Part_ID==eql$Part_ID[i]][1]
	eql_language[i] <- x$Task_Lid[x$Part_ID==eql$Part_ID[i]][1]
	eql_EduTime[i] <- x$Part_EduTime[x$Part_ID==eql$Part_ID[i]][1]
	eql_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==eql$Part_ID[i]][1]
	eql_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==eql$Part_ID[i]][1]
	eql_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==eql$Part_ID[i]][1]
	eql_Part_University[i] <- x$Part_University[x$Part_ID==eql$Part_ID[i]][1]
}

eql_gender
eql_BirthYear
eql_country
eql_language
eql_EduTime
eql_Part_ExpTime
eql_Part_ExpManage
eql_Part_ExpArea
eql_Part_University

scales_eql_bio <- as.data.frame(cbind(eql_gender,
				eql_BirthYear,
				eql_country,
				eql_language,
				eql_EduTime,
				eql_Part_ExpTime,
				eql_Part_ExpManage,
				eql_Part_ExpArea,
				eql_Part_University
		))
names(eql_bio)

scales_eql <- cbind(eql, eql_bio)
names(eql)

### scales_eql_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#354				scales_eql - 	1		16,39	3,54			Professional Challenge
Inst_ID <- 354
Norm_ID <- ""
Norm_Name <- "scales_eql - fill in norm name here"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M354_01, M354_02, M354_03)),2)
sd <- round(c(SD354_01, SD354_02, SD354_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

eql_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
eql_norms
names(eql_norms)


### age
eql_BirthYear <- ifelse(eql_BirthYear==0 , NA, eql_BirthYear)

age <- (current_year-eql_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(eql$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(eql_gender)
table(eql_gender)
#table(scales_eql_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(eql_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(scales_eql_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(eql_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$eql_Part_University==-1]

eql_Part_University <- ifelse(eql_Part_University==-1 , NA, eql_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(eql_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$eql_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(eql_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$eql_Part_EduTime==0]

eql_Part_EduTime <- ifelse(eql_Part_EduTime==0 , NA, eql_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(eql_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$eql_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$eql_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(eql_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$eql_Part_ExpTime==0]

eql_Part_ExpTime <- ifelse(eql_Part_ExpTime==0 , NA, eql_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(eql_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$eql_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$eql_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(eql_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$eql_Part_ExpManage==0]

eql_Part_ExpManage <- ifelse(eql_Part_ExpManage==0 , NA, eql_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(eql_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$eql_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$eql_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(eql_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$eql_Part_ExpArea==0]

eql_Part_ExpArea <- ifelse(eql_Part_ExpArea==0 , NA, eql_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(eql_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$eql_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$eql_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(eql_language)))
language_txt <- ""
for (i in 1:length(freq_language$eql_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$eql_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(eql_country)))
freq_country
country_na <- freq_country$Freq[freq_country$eql_country==0]

eql_country <- ifelse(eql_country==0 , NA, eql_country)

freq_country <- as.data.frame(prop.table(table(eql_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$eql_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$scales_eql_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$eql_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

eql_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\eql.xlsx", eql, eql_deleted, eql_norms, eql_bio)
}

#########################################################
#### scales numerical finance compact

if (length(X701_overallperformance) > 0) {

num <- as.data.frame(cbind(Part_ID, 
				X701_overallperformance,
				X701_speed,
				X701_accuracy 				
		)) 

names(num)

# Stanine
M701_01 <- mean(num$X701_overallperformance, na.rm=TRUE)
M701_02 <- mean(num$X701_speed, na.rm=TRUE)
M701_03 <- mean(num$X701_accuracy, na.rm=TRUE)

SD701_01 <- sd(num$X701_overallperformance, na.rm=TRUE)
SD701_02 <- sd(num$X701_speed, na.rm=TRUE)
SD701_03 <- sd(num$X701_accuracy, na.rm=TRUE)

X701_overallperformance_Stanine <- round(ifelse((num$X701_overallperformance- M701_01)/SD701_01*2+5>9 , 9, ifelse((num$X701_overallperformance- M701_01)/SD701_01*2+5<1, 1, (num$X701_overallperformance- M701_01)/SD701_01*2+5)))
X701_speed_Stanine <- round(ifelse((num$X701_speed - M701_02)/SD701_02*2+5>9 , 9, ifelse((num$X701_speed - M701_02)/SD701_01*2+5<1, 1, (num$X701_speed - M701_02)/SD701_02*2+5)))
X701_accuracy_Stanine <- round(ifelse((num$X701_accuracy- M701_03)/SD701_03*2+5>9 , 9, ifelse((num$X701_accuracy- M701_03)/SD701_01*2+5<1, 1, (num$X701_accuracy- M701_03)/SD701_03*2+5)))

num <- cbind(num,
		X701_overallperformance_Stanine,
		X701_speed_Stanine, 
		X701_accuracy_Stanine 		
)

colnames(num)
class(num)
num <- as.data.frame(num)
names(num)

barplot(prop.table(table(X701_overallperformance_Stanine)), col=c.blue, xlab="numerical finance compact Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X701_speed_Stanine)), col=c.blue, xlab="numerical finance compact Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X701_accuracy_Stanine)), col=c.blue, xlab="numerical finance compact Stanine Accuracy", ylab="percent/100")

# add biodata to numerical

# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

num_gender <- NA
num_BirthYear <- NA
num_country <- NA
num_language <- NA
num_EduTime <- NA
num_Part_ExpTime <- NA
num_Part_ExpManage <- NA
num_Part_ExpArea <- NA
num_Part_University <- NA

for (i in 1:length(num$Part_ID)) {
	num_gender[i] <- x$Part_Gender[x$Part_ID==num$Part_ID[i]][1]
	num_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==num$Part_ID[i]][1]
	num_country[i] <- x$Part_country[x$Part_ID==num$Part_ID[i]][1]
	num_language[i] <- x$Task_Lid[x$Part_ID==num$Part_ID[i]][1]
	num_EduTime[i] <- x$Part_EduTime[x$Part_ID==num$Part_ID[i]][1]
	num_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==num$Part_ID[i]][1]
	num_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==num$Part_ID[i]][1]
	num_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==num$Part_ID[i]][1]
	num_Part_University[i] <- x$Part_University[x$Part_ID==num$Part_ID[i]][1]
}
num_gender
num_BirthYear
num_country
num_language
num_EduTime
num_Part_ExpTime
num_Part_ExpManage
num_Part_ExpArea
num_Part_University

num_bio <- as.data.frame(cbind(num_gender,
				num_BirthYear,
				num_country,
				num_language,
				num_EduTime,
				num_Part_ExpTime,
				num_Part_ExpManage,
				num_Part_ExpArea,
				num_Part_University
		))
names(num_bio)

num <- cbind(num, num_bio)
names(num)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 701
Norm_ID <- ""
Norm_Name <- "scales_num(fin_cp) - fill in norm name here"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M701_01, M701_02, M701_03)),2)
sd <- round(c(SD701_01, SD701_02, SD701_03),2)
mSnine <- ""
Scale_Info <- c("Overallperformance", "Speed", "Accuracy")

num_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
num_norms
names(num_norms)

### age
num_BirthYear <- ifelse(num_BirthYear==0 , NA, num_BirthYear)

age <- (current_year-num_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(num$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(num_gender)
table(num_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(num_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(scales_num(fin_cp)_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(num_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$num_Part_University==-1]

num_Part_University <- ifelse(num_Part_University==-1 , NA, num_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(num_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$num_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(num_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$num_Part_EduTime==0]

num_Part_EduTime <- ifelse(num_Part_EduTime==0 , NA, num_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(num_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$num_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$num_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(num_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$num_Part_ExpTime==0]

num_Part_ExpTime <- ifelse(num_Part_ExpTime==0 , NA, num_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(num_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$num_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$num_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(num_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$num_Part_ExpManage==0]

num_Part_ExpManage <- ifelse(num_Part_ExpManage==0 , NA, num_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(num_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$num_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$num_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(num_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$num_Part_ExpArea==0]

num_Part_ExpArea <- ifelse(num_Part_ExpArea==0 , NA, num_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(num_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$num_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$num_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(num_language)))
language_txt <- ""
for (i in 1:length(freq_language$num_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$num_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(num_country)))
freq_country
country_na <- freq_country$Freq[freq_country$num_country==0]

num_country <- ifelse(num_country==0 , NA, num_country)

freq_country <- as.data.frame(prop.table(table(num_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$num_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$scales_num(fin_cp)_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$num_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

num_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\num_fin_cp.xlsx", eql, eql_norms, eql_bio)
}

#########################################################
#### 751 scales_verbal (finance compact)

if (length(X751_overallperformance) > 0) {

verbal <- as.data.frame(cbind(Part_ID, 
				X751_overallperformance,
				X751_speed,
				X751_accuracy, 				
		
		)) 

names(verbal)

# Stanine
M751_01 <- mean(verbal$X751_overallperformance, na.rm=TRUE)
M751_02 <- mean(verbal$X751_speed, na.rm=TRUE)
M751_03 <- mean(verbal$X751_accuracy, na.rm=TRUE)

SD751_01 <- sd(verbal$X751_overallperformance, na.rm=TRUE)
SD751_02 <- sd(verbal$X751_speed, na.rm=TRUE)
SD751_03 <- sd(verbal$X751_accuracy, na.rm=TRUE)

X751_overallperformance_Stanine <- round(ifelse((verbal$X751_overallperformance- M751_01)/SD751_01*2+5>9 , 9, ifelse((verbal$X751_overallperformance- M751_01)/SD751_01*2+5<1, 1, (verbal$X751_overallperformance- M751_01)/SD751_01*2+5)))
X751_speed_Stanine <- round(ifelse((verbal$X751_speed - M751_02)/SD751_02*2+5>9 , 9, ifelse((verbal$X751_speed - M751_02)/SD751_01*2+5<1, 1, (verbal$X751_speed - M751_02)/SD751_02*2+5)))
X751_accuracy_Stanine <- round(ifelse((verbal$X751_accuracy- M751_03)/SD751_03*2+5>9 , 9, ifelse((verbal$X751_accuracy- M751_03)/SD751_01*2+5<1, 1, (verbal$X751_accuracy- M751_03)/SD751_03*2+5)))

verbal <- cbind(verbal,
		X751_overallperformance_Stanine,
		X751_speed_Stanine, 
		X751_accuracy_Stanine 		
)

colnames(verbal)
class(verbal)
verbal <- as.data.frame(verbal)
names(verbal)

barplot(prop.table(table(X751_overallperformance_Stanine)), col=c.blue, xlab="verbal finance compact Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X751_speed_Stanine)), col=c.blue, xlab="verbal finance compact Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X751_accuracy_Stanine)), col=c.blue, xlab="verbal finance compact Stanine Accuracy", ylab="percent/100")

# add biodata to scales_verbal(fin_cp)

# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

verbal_gender <- NA
verbal_BirthYear <- NA
verbal_country <- NA
verbal_language
verbal_EduTime <- NA
verbal_Part_ExpTime <- NA
verbal_Part_ExpManage <- NA
verbal_Part_ExpArea <- NA
verbal_Part_University <- NA

for (i in 1:length(verbal$Part_ID)) {
	verbal_gender[i] <- x$Part_Gender[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_country[i] <- x$Part_country[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_language[i] <- x$Task_Lid[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_EduTime[i] <- x$Part_EduTime[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==verbal$Part_ID[i]][1]
	verbal_Part_University[i] <- x$Part_University[x$Part_ID==verbal$Part_ID[i]][1]
}
verbal_gender
verbal_BirthYear
verbal_country
verbal_language
verbal_EduTime
verbal_Part_ExpTime
verbal_Part_ExpManage
verbal_Part_ExpArea
verbal_Part_University

verbal_bio <- as.data.frame(cbind(verbal_gender,
				verbal_BirthYear,
				verbal_country,
				verbal_language,
				verbal_EduTime,
				verbal_Part_ExpTime,
				verbal_Part_ExpManage,
				verbal_Part_ExpArea,
				verbal_Part_University
		))
names(verbal_bio)

verbal <- cbind(verbal, verbal_bio)
names(verbal)

### scales_verbal(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#751				scales_verbal(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 751
Norm_ID <- ""
Norm_Name <- "scales_verbal(fin_cp) - fill in norm name here"
Scale <- as.verbaleric(1:3) 
Mean <- round(as.verbaleric(c(M751_01, M751_02, M751_03)),2)
sd <- round(c(SD751_01, SD751_02, SD751_03),2)
mSnine <- ""
Scale_Info <- c("Overallperformance", "Speed", "Accuracy")

verbal_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
verbal_norms
names(verbal_norms)

# scales_verbal(fin_cp) bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
		"languages", "country", "education time")

### age
verbal_BirthYear <- ifelse(verbal_BirthYear==0 , NA, verbal_BirthYear)

age <- (current_year-verbal_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(verbal$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(verbal_gender)
table(verbal_gender)
#table(scales_verbal(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(verbal_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(scales_verbal(fin_cp)_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(verbal_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$verbal_Part_University==-1]

verbal_Part_University <- ifelse(verbal_Part_University==-1 , NA, verbal_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(verbal_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$verbal_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(verbal_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$verbal_Part_EduTime==0]

verbal_Part_EduTime <- ifelse(verbal_Part_EduTime==0 , NA, verbal_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(verbal_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$verbal_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$verbal_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(verbal_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$verbal_Part_ExpTime==0]

verbal_Part_ExpTime <- ifelse(verbal_Part_ExpTime==0 , NA, verbal_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(verbal_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$verbal_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$verbal_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(verbal_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$verbal_Part_ExpManage==0]

verbal_Part_ExpManage <- ifelse(verbal_Part_ExpManage==0 , NA, verbal_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(verbal_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$verbal_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$verbal_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(verbal_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$verbal_Part_ExpArea==0]

verbal_Part_ExpArea <- ifelse(verbal_Part_ExpArea==0 , NA, verbal_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(verbal_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$verbal_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$verbal_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(verbal_language)))
language_txt <- ""
for (i in 1:length(freq_language$verbal_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$verbal_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(verbal_country)))
freq_country
country_na <- freq_country$Freq[freq_country$verbal_country==0]

verbal_country <- ifelse(verbal_country==0 , NA, verbal_country)

freq_country <- as.data.frame(prop.table(table(verbal_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$verbal_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$scales_verbal(fin_cp)_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$verbal_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

verbal_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\verbal_fin_cp.xlsx", verbal_fin_cp, verbal_fin_cp_deleted, verbal_fin_cp_norms, verbal_fin_cp_bio)
}

#########################################################
#### lst

if (length(X344_overallperformance) > 0) {
	
lst <- as.data.frame(cbind(Part_ID, 
				X344_overallperformance,
				X344_speed,
				X344_accuracy)) 

names(lst)


# data clearing

# remove rows with no results for lst
length(lst$Part_ID)
lst <- na.omit(lst)
length(lst$Part_ID)

# remove rows with very low accuracy
del_count <- 0
lst_deleted <- ""

for (i in 1:length(lst$Part_ID)) {
	if (lst$X344_accuracy[i]  <= 25 ) {  # adjust accuracy threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", lst$X344_accuracy[i] , ", Part_ID:", lst$Part_ID[i] , " will be deleted from dataset"))
		lst_deleted<-rbind(lst_deleted,lst[i,1:4])
		lst$X344_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " case(s) with accuracy <= 25 will be deleted from dataset"))
lst <- na.omit(lst)
length(lst$Part_ID)

# remove rows with very low or high speed
del_count <- 0
for (i in 1:length(lst$Part_ID)) {
	if (lst$X344_speed[i]  <= 2 || lst$X344_speed[i]>= 60 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("speed low or high: ", lst$X344_speed[i] , ", Part_ID:", lst$Part_ID[i] , " will be deleted from dataset"))
		lst_deleted<-rbind(lst_deleted,lst[i,1:4])
		lst$X344_speed[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with speed <= 2 or >= 60 will be deleted from dataset"))
lst <- na.omit(lst)
length(lst$Part_ID)

#
lst_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= 2 or >= 60","acc <= 25")) # only once for a test 
names(lst_deleted_txt) <- c("Part_ID", "X344_overallperformance", "X344_speed", "X344_accuracy")
lst_deleted <- rbind(lst_deleted_txt,lst_deleted)


# Stanine
M344_01 <- mean(lst$X344_overallperformance, na.rm=TRUE)
M344_02 <- mean(lst$X344_speed, na.rm=TRUE)
M344_03 <- mean(lst$X344_accuracy, na.rm=TRUE)

SD344_01 <- sd(lst$X344_overallperformance, na.rm=TRUE)
SD344_02 <- sd(lst$X344_speed, na.rm=TRUE)
SD344_03 <- sd(lst$X344_accuracy, na.rm=TRUE)

X344_overallperformance_Stanine <- round(ifelse((lst$X344_overallperformance- M344_01)/SD344_01*2+5>9 , 9, ifelse((lst$X344_overallperformance- M344_01)/SD344_01*2+5<1, 1, (lst$X344_overallperformance- M344_01)/SD344_01*2+5)))
X344_speed_Stanine <- round(ifelse((lst$X344_speed - M344_02)/SD344_02*2+5>9 , 9, ifelse((lst$X344_speed - M344_02)/SD344_01*2+5<1, 1, (lst$X344_speed - M344_02)/SD344_02*2+5)))
X344_accuracy_Stanine <- round(ifelse((lst$X344_accuracy- M344_03)/SD344_03*2+5>9 , 9, ifelse((lst$X344_accuracy- M344_03)/SD344_01*2+5<1, 1, (lst$X344_accuracy- M344_03)/SD344_03*2+5)))

lst <- cbind(lst,
		X344_overallperformance_Stanine,
		X344_speed_Stanine, 
		X344_accuracy_Stanine 		
)

colnames(lst)
class(lst)
lst <- as.data.frame(lst)
names(lst)

barplot(prop.table(table(lst$X344_overallperformance_Stanine)), col=c.blue, xlab="lst Stanine Performance", ylab="percent/100")
barplot(prop.table(table(lst$X344_speed_Stanine)), col=c.blue, xlab="lst Stanine Speed", ylab="percent/100")
barplot(prop.table(table(lst$X344_accuracy_Stanine)), col=c.blue, xlab="lst Stanine Accuracy", ylab="percent/100")

# add biodata to scales_lst
#gender
# BirthYear
# country
# language
# education time
# experience time
# management experience time
# Experience area
# university degree

lst_gender <- NA
lst_BirthYear <- NA
lst_country <- NA
lst_language <- NA
lst_EduTime <- NA
lst_Part_ExpTime <- NA
lst_Part_ExpManage <- NA
lst_Part_ExpArea <- NA
lst_Part_University <- NA

for (i in 1:length(lst$Part_ID)) {
	lst_gender[i] <- x$Part_Gender[x$Part_ID==lst$Part_ID[i]][1]
	lst_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==lst$Part_ID[i]][1]
	lst_country[i] <- x$Part_country[x$Part_ID==lst$Part_ID[i]][1]
	lst_language[i] <- x$Task_Lid[x$Part_ID==lst$Part_ID[i]][1]
	lst_EduTime[i] <- x$Part_EduTime[x$Part_ID==lst$Part_ID[i]][1]
	lst_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==lst$Part_ID[i]][1]
	lst_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==lst$Part_ID[i]][1]
	lst_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==lst$Part_ID[i]][1]
	lst_Part_University[i] <- x$Part_University[x$Part_ID==lst$Part_ID[i]][1]
}

lst_gender
lst_BirthYear
lst_country
lst_language
lst_EduTime
lst_Part_ExpTime
lst_Part_ExpManage
lst_Part_ExpArea
lst_Part_University

lst_bio <- as.data.frame(cbind(lst_gender,
				lst_BirthYear,
				lst_country,
				lst_language,
				lst_EduTime,
				lst_Part_ExpTime,
				lst_Part_ExpManage,
				lst_Part_ExpArea,
				lst_Part_University
		))
names(lst_bio)

lst <- cbind(lst, lst_bio)
names(lst)

### scales_lst_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#344				scales_lst - AXA	1		16,39	3,54			Performance
Inst_ID <- 344
Norm_ID <- ""
Norm_Name <- "lst - fill in norm here"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M344_01, M344_02, M344_03)),2)
sd <- round(c(SD344_01, SD344_02, SD344_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

lst_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
lst_norms
names(lst_norms)

### age
lst_BirthYear <- ifelse(lst_BirthYear==0 , NA, lst_BirthYear)

age <- (current_year-lst_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(lst$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(lst_gender)
table(lst_gender)
#table(scales_lst_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(lst_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(scales_lst_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(lst_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$lst_Part_University==-1]

lst_Part_University <- ifelse(lst_Part_University==-1 , NA, lst_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(lst_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$lst_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(lst_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$lst_EduTime==0]

lst_EduTime <- ifelse(lst_EduTime==0 , NA, lst_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(lst_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$lst_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$lst_EduTime[i]))],";  others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(lst_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$lst_Part_ExpTime==0]

lst_Part_ExpTime <- ifelse(lst_Part_ExpTime==0 , NA, lst_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(lst_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$lst_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$lst_Part_ExpTime[i]))],";  others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(lst_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$lst_Part_ExpManage==0]

lst_Part_ExpManage <- ifelse(lst_Part_ExpManage==0 , NA, lst_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(lst_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$lst_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$lst_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(lst_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$lst_Part_ExpArea==0]

lst_Part_ExpArea <- ifelse(lst_Part_ExpArea==0 , NA, lst_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(lst_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$lst_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$lst_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(lst_language)))
language_txt <- ""
for (i in 1:length(freq_language$lst_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$lst_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(lst_country)))
freq_country
country_na <- freq_country$Freq[freq_country$lst_country==0]

lst_country <- ifelse(lst_country==0 , NA, lst_country)

freq_country <- as.data.frame(prop.table(table(lst_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$lst_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$lst_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

lst_bio <- as.data.frame(cbind(bio_titles,bio_info))

# save data, each test to a separate sheet
save.xlsx("_data\\lst.xlsx", lst, lst_deleted, lst_norms, lst_bio)

}
#########################################################
#### scales_cls(public)

if (length(X361_overallperformance) > 0) {

cls <- as.data.frame(cbind(Part_ID, 
				X361_overallperformance,
				X361_speed,
				X361_accuracy, 				
		
		)) 

names(cls)

# Stanine
M361_01 <- mean(cls$X361_overallperformance, na.rm=TRUE)
M361_02 <- mean(cls$X361_speed, na.rm=TRUE)
M361_03 <- mean(cls$X361_accuracy, na.rm=TRUE)

SD361_01 <- sd(cls$X361_overallperformance, na.rm=TRUE)
SD361_02 <- sd(cls$X361_speed, na.rm=TRUE)
SD361_03 <- sd(cls$X361_accuracy, na.rm=TRUE)

X361_overallperformance_Stanine <- round(ifelse((cls$X361_overallperformance- M361_01)/SD361_01*2+5>9 , 9, ifelse((cls$X361_overallperformance- M361_01)/SD361_01*2+5<1, 1, (cls$X361_overallperformance- M361_01)/SD361_01*2+5)))
X361_speed_Stanine <- round(ifelse((cls$X361_speed - M361_02)/SD361_02*2+5>9 , 9, ifelse((cls$X361_speed - M361_02)/SD361_01*2+5<1, 1, (cls$X361_speed - M361_02)/SD361_02*2+5)))
X361_accuracy_Stanine <- round(ifelse((cls$X361_accuracy- M361_03)/SD361_03*2+5>9 , 9, ifelse((cls$X361_accuracy- M361_03)/SD361_01*2+5<1, 1, (cls$X361_accuracy- M361_03)/SD361_03*2+5)))

cls <- cbind(cls,
		X361_overallperformance_Stanine,
		X361_speed_Stanine, 
		X361_accuracy_Stanine 		
)

colnames(cls)
class(cls)
cls <- as.data.frame(cls)
names(cls)

barplot(prop.table(table(X361_overallperformance_Stanine)), col=c.blue, xlab="cls Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X361_speed_Stanine)), col=c.blue, xlab="cls Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X361_accuracy_Stanine)), col=c.blue, xlab="cls Stanine Accuracy", ylab="percent/100")

# add biodata to cls
#gender
# BirthYear
#country
# education time
# experience time
# management experience time
# Experience area
# university degree

cls_gender <- NA
cls_BirthYear <- NA
cls_country <- NA
cls_language <- NA
cls_EduTime <- NA
cls_Part_ExpTime <- NA
cls_Part_ExpManage <- NA
cls_Part_ExpArea <- NA
cls_Part_University <- NA

for (i in 1:length(cls$Part_ID)) {
	cls_gender[i] <- x$Part_Gender[x$Part_ID==cls$Part_ID[i]][1]
	cls_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==cls$Part_ID[i]][1]
	cls_country[i] <- x$Part_country[x$Part_ID==cls$Part_ID[i]][1]
	cls_language[i] <- x$Task_Lid[x$Part_ID==cls$Part_ID[i]][1]
	cls_EduTime[i] <- x$Part_EduTime[x$Part_ID==cls$Part_ID[i]][1]
	cls_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==cls$Part_ID[i]][1]
	cls_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==cls$Part_ID[i]][1]
	cls_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==cls$Part_ID[i]][1]
	cls_Part_University[i] <- x$Part_University[x$Part_ID==cls$Part_ID[i]][1]
}
cls_gender
cls_BirthYear
cls_country
cls_language
cls_EduTime
cls_Part_ExpTime
cls_Part_ExpManage
cls_Part_ExpArea
cls_Part_University

cls_bio <- as.data.frame(cbind(cls_gender,
				cls_BirthYear,
				cls_country,
				cls_language,
				cls_EduTime,
				cls_Part_ExpTime,
				cls_Part_ExpManage,
				cls_Part_ExpArea,
				cls_Part_University
		))
names(cls_bio)

cls <- cbind(cls, cls_bio)
names(cls)

### scales_cls(public)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#361				scales_cls(public) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 361
Norm_ID <- ""
Norm_Name <- "cls (public) - fill in norm name here"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M361_01, M361_02, M361_03)),2)
sd <- round(c(SD361_01, SD361_02, SD361_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

cls_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
cls_norms
names(cls_norms)

# cls (public) bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
		"languages", "country", "education time")

### age
cls_BirthYear <- ifelse(cls_BirthYear==0 , NA, cls_BirthYear)

age <- (current_year-cls_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(cls$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(cls_gender)
table(cls_gender)

freq_gender <- as.data.frame(prop.table(table(cls_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(cls_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$cls_Part_University==-1]

cls_Part_University <- ifelse(cls_Part_University==-1 , NA, cls_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(cls_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$cls_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(cls_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$cls_Part_EduTime==0]

cls_Part_EduTime <- ifelse(cls_Part_EduTime==0 , NA, cls_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(cls_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$scales_cls_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$cls_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(cls_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$cls_Part_ExpTime==0]

cls_Part_ExpTime <- ifelse(cls_Part_ExpTime==0 , NA, cls_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(cls_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$cls_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$cls_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(cls_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$cls_Part_ExpManage==0]

cls_Part_ExpManage <- ifelse(cls_Part_ExpManage==0 , NA, cls_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(cls_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$scales_cls_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$cls_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(cls_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$cls_Part_ExpArea==0]

cls_Part_ExpArea <- ifelse(cls_Part_ExpArea==0 , NA, cls_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(cls_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$cls_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$cls_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

# language

freq_language <- as.data.frame(prop.table(table(cls_language)))

for (i in 1:length(freq_language$cls_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, " ", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$cls_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(cls_country)))
freq_country
country_na <- freq_country$Freq[freq_country$cls_country==0]

cls_country <- ifelse(cls_country==0 , NA, cls_country)

freq_country <- as.data.frame(prop.table(table(cls_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$cls_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$scales_cls(public)_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$cls_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

cls_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\cls.xlsx", cls, cls_deleted, cls_norms, cls_bio)

#########################################################
#### scales_ix

scales_ix <- as.data.frame(cbind(Part_ID, 
				X349_overallperformance,
				X349_speed,
				X349_accuracy, 				
		
		)) 

names(scales_ix)

# Stanine
M349_01 <- mean(X349_overallperformance, na.rm=TRUE)
M349_02 <- mean(X349_speed, na.rm=TRUE)
M349_03 <- mean(X349_accuracy, na.rm=TRUE)

SD349_01 <- sd(X349_overallperformance, na.rm=TRUE)
SD349_02 <- sd(X349_speed, na.rm=TRUE)
SD349_03 <- sd(X349_accuracy, na.rm=TRUE)

X349_overallperformance_Stanine <- round(ifelse((X349_overallperformance- M349_01)/SD349_01*2+5>9 , 9, ifelse((X349_overallperformance- M349_01)/SD349_01*2+5<1, 1, (X349_overallperformance- M349_01)/SD349_01*2+5)))
X349_speed_Stanine <- round(ifelse((X349_speed - M349_02)/SD349_02*2+5>9 , 9, ifelse((X349_speed - M349_02)/SD349_01*2+5<1, 1, (X349_speed - M349_02)/SD349_02*2+5)))
X349_accuracy_Stanine <- round(ifelse((X349_accuracy- M349_03)/SD349_03*2+5>9 , 9, ifelse((X349_accuracy- M349_03)/SD349_01*2+5<1, 1, (X349_accuracy- M349_03)/SD349_03*2+5)))

scales_ix <- cbind(ix,
		X349_overallperformance_Stanine,
		X349_speed_Stanine, 
		X349_accuracy_Stanine 		
)

colnames(scales_ix)
class(scales_ix)
scales_ix <- as.data.frame(scales_ix)
names(scales_ix)

barplot(prop.table(table(X349_overallperformance_Stanine)), col=c.blue, xlab="Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X349_speed_Stanine)), col=c.blue, xlab="Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X349_accuracy_Stanine)), col=c.blue, xlab="Stanine Accuracy", ylab="percent/100")

# add biodata to scales_ix

# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

scales_ix_gender <- NA
scales_ix_BirthYear <- NA
scales_ix_country <- NA
scales_ix_language <- NA
scales_ix_EduTime <- NA
scales_ix_Part_ExpTime <- NA
scales_ix_Part_ExpManage <- NA
scales_ix_Part_ExpArea <- NA
scales_ix_Part_University <- NA

for (i in 1:length(ix$Part_ID)) {
	scales_ix_gender[i] <- x$Part_Gender[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_country[i] <- x$Part_country[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_language[i] <- x$Task_Lid[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_EduTime[i] <- x$Part_EduTime[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==scales_ix$Part_ID[i]][1]
	scales_ix_Part_University[i] <- x$Part_University[x$Part_ID==scales_ix$Part_ID[i]][1]
}
scales_ix_d_gender
scales_ix_BirthYear
scales_ix_country
scales_ix_language
scales_ix_EduTime
scales_ix_Part_ExpTime
ix_Part_ExpManage
scales_ix_Part_ExpArea
scales_ix_Part_University

scales_ix_bio <- as.data.frame(cbind(scales_ix_gender,
				scales_ix_BirthYear,
				scales_ix_country,
				scales_ix_language,
				scales_ix_EduTime,
				scales_ix_Part_ExpTime,
				scales_ix_Part_ExpManage,
				scales_ix_Part_ExpArea,
				scales_ix_Part_University
		))
names(scales_ix_bio)

scales_ix <- cbind(scales_ix, scales_ix_bio)
names(scales_ix)

### scales_ix_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#349				scales_ix - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 349
Norm_ID <- ""
Norm_Name <- "scales_ix - AXA"
Scale <- as.numeric(1:3) 
Mean <- round(as.verbaleric(c(M349_01, M349_02, M349_03)),2)
sd <- round(c(SD349_01, SD349_02, SD349_03),2)
mSnine <- ""
Scale_Info <- c("Overallperformance", "Speed", "Accuracy")

scales_ix_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
scales_ix_norms
names(scales_ix_norms)

# scales_ix bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
		"languages", "country", "education time")

### age
scales_ix_BirthYear <- ifelse(scales_ix_BirthYear==0 , NA, scales_ix_BirthYear)

age <- (current_year-scales_ix_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(scales_ix$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(scales_ix_gender)
table(scales_ix_gender)
#table(scales_ix_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(scales_ix_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(scales_ix_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(scales_ix_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$scales_ix_Part_University==-1]

scales_ix_Part_University <- ifelse(scales_ix_Part_University==-1 , NA, scales_ix_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(scales_ix_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$scales_ix_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(scales_ix_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$scales_ix_Part_EduTime==0]

scales_ix_Part_EduTime <- ifelse(ix_Part_EduTime==0 , NA, scales_ix_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(ix_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$scales_ix_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$scales_ix_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(scales_ix_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$scales_ix_Part_ExpTime==0]

scales_ix_Part_ExpTime <- ifelse(scales_ix_Part_ExpTime==0 , NA, scales_ix_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(scales_ix_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$ix_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$scales_ix_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(scales_ix_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$scales_ix_Part_ExpManage==0]

scales_ix_Part_ExpManage <- ifelse(scales_ix_Part_ExpManage==0 , NA, scales_ix_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(scales_ix_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$scales_ix_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$scales_ix_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(scales_ix_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$scales_ix_Part_ExpArea==0]

scales_ix_Part_ExpArea <- ifelse(scales_ix_Part_ExpArea==0 , NA, scales_ix_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(scales_ix_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$scales_ix_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$scales_ix_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(ix_language)))

for (i in 1:length(freq_language$ix_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, " ", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$ix_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(ix_country)))
freq_country
country_na <- freq_country$Freq[freq_country$ix_country==0]

scales_ix_country <- ifelse(ix_country==0 , NA, ix_country)

freq_country <- as.data.frame(prop.table(table(scales_ix_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$scales_ix_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$scales_ix_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$scales_ix_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

ix_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################
# save pivot table
pivot <- pivot_all

# save data, each test to a separate sheet
save.xlsx("_data\\ix.xlsx", ix, ix_deleted, ix_norms, ix_bio)
}

##############################################
# 315 lt-e

if (length(X315_fluencyraw) > 0) {

lt_e <- as.data.frame(cbind(Part_ID, 
				X315_fluencyraw,
				X315_fluencyspeed,
				X315_fluencyaccuracy, 				
				X315_spellingraw, 
				X315_spellingspeed,
				X315_spellingaccuracy,				
				X315_vocabraw, 
				X315_vocabspeed,
				X315_vocabaccuracy
		)) 

names(lt_e)

# Stanine
M315_01 <- mean(X315_fluencyaccuracy, na.rm=TRUE)
M315_02 <- mean(X315_fluencyraw, na.rm=TRUE)
M315_03 <- mean(X315_fluencyspeed, na.rm=TRUE)
M315_04 <- mean(X315_spellingaccuracy, na.rm=TRUE)
M315_05 <- mean(X315_spellingraw, na.rm=TRUE)
M315_06 <- mean(X315_spellingspeed, na.rm=TRUE)
M315_07 <- mean(X315_vocabaccuracy, na.rm=TRUE)
M315_08 <- mean(X315_vocabraw, na.rm=TRUE)
M315_09 <- mean(X315_vocabspeed, na.rm=TRUE)

SD315_01 <- sd(X315_fluencyaccuracy, na.rm=TRUE)
SD315_02 <- sd(X315_fluencyraw, na.rm=TRUE)
SD315_03 <- sd(X315_fluencyspeed, na.rm=TRUE)
SD315_04 <- sd(X315_spellingaccuracy, na.rm=TRUE)
SD315_05 <- sd(X315_spellingraw, na.rm=TRUE)
SD315_06 <- sd(X315_spellingspeed, na.rm=TRUE)
SD315_07 <- sd(X315_vocabaccuracy, na.rm=TRUE)
SD315_08 <- sd(X315_vocabraw, na.rm=TRUE)
SD315_09 <- sd(X315_vocabspeed, na.rm=TRUE)

X315_fluencyaccuracy_Stanine <- round(ifelse((X315_fluencyaccuracy - M315_01)/SD315_01*2+5>9 , 9, ifelse((X315_fluencyaccuracy - M315_01)/SD315_01*2+5<1, 1, (X315_fluencyaccuracy - M315_01)/SD315_01*2+5)))
X315_fluencyraw_Stanine <- round(ifelse((X315_fluencyraw - M315_02)/SD315_02*2+5>9 , 9, ifelse((X315_fluencyraw - M315_02)/SD315_01*2+5<1, 1, (X315_fluencyraw - M315_02)/SD315_02*2+5)))
X315_fluencyspeed_Stanine <- round(ifelse((X315_fluencyspeed - M315_03)/SD315_03*2+5>9 , 9, ifelse((X315_fluencyspeed - M315_03)/SD315_01*2+5<1, 1, (X315_fluencyspeed - M315_03)/SD315_03*2+5)))
X315_spellingaccuracy_Stanine <- round(ifelse((X315_spellingaccuracy - M315_04)/SD315_04*2+5>9 , 9, ifelse((X315_spellingaccuracy - M315_04)/SD315_01*2+5<1, 1, (X315_spellingaccuracy - M315_04)/SD315_04*2+5)))
X315_spellingraw_Stanine <- round(ifelse((X315_spellingraw - M315_05)/SD315_05*2+5>9 , 9, ifelse((X315_spellingraw - M315_05)/SD315_01*2+5<1, 1, (X315_spellingraw - M315_05)/SD315_05*2+5)))
X315_spellingspeed_Stanine <- round(ifelse((X315_spellingspeed - M315_06)/SD315_06*2+5>9 , 9, ifelse((X315_spellingspeed - M315_06)/SD315_01*2+5<1, 1, (X315_spellingspeed - M315_06)/SD315_06*2+5)))
X315_vocabaccuracy_Stanine <- round(ifelse((X315_vocabaccuracy - M315_07)/SD315_07*2+5>9 , 9, ifelse((X315_vocabaccuracy - M315_07)/SD315_01*2+5<1, 1, (X315_vocabaccuracy - M315_07)/SD315_07*2+5)))
X315_vocabraw_Stanine <- round(ifelse((X315_vocabraw - M315_08)/SD315_08*2+5>9 , 9, ifelse((X315_vocabraw - M315_08)/SD315_01*2+5<1, 1, (X315_vocabraw - M315_08)/SD315_08*2+5)))
X315_vocabspeed_Stanine <- round(ifelse((X315_vocabspeed - M315_09)/SD315_09*2+5>9 , 9, ifelse((X315_vocabspeed - M315_09)/SD315_01*2+5<1, 1, (X315_vocabspeed - M315_09)/SD315_09*2+5)))

lt_e <- cbind(lt_e,
		X315_fluencyraw_Stanine,
		X315_fluencyspeed_Stanine, 
		X315_fluencyaccuracy_Stanine, 
		X315_spellingraw_Stanine, 
		X315_spellingspeed_Stanine, 
		X315_spellingaccuracy_Stanine, 
		X315_vocabraw_Stanine, 
		X315_vocabspeed_Stanine, 
		X315_vocabaccuracy_Stanine
)

colnames(lt_e)
class(lt_e)
lt_e <- as.data.frame(lt_e)
names(lt_e)

barplot(prop.table(table(X315_fluencyraw_Stanine)), col=c.blue, xlab="Stanine Fluencyraw", ylab="percent/100")
barplot(prop.table(table(X315_fluencyspeed_Stanine)), col=c.blue, xlab="Stanine Fluency Speech", ylab="percent/100")
barplot(prop.table(table(X315_fluencyaccuracy_Stanine)), col=c.blue, xlab="Stanine Fluency Accuracy", ylab="percent/100")

barplot(prop.table(table(X315_spellingraw_Stanine)), col=c.blue, xlab="Stanine Spellingraw", ylab="percent/100")
barplot(prop.table(table(X315_spellingspeed_Stanine)), col=c.blue, xlab="Stanine Spelling Speed", ylab="percent/100")
barplot(prop.table(table(X315_spellingaccuracy_Stanine)), col=c.blue, xlab="Stanine Spelling Accuracy", ylab="percent/100")

barplot(prop.table(table(X315_vocabraw_Stanine)), col=c.blue, xlab="Stanine Vocabulary Raw", ylab="percent/100")
barplot(prop.table(table(X315_vocabspeed_Stanine)), col=c.blue, xlab="Stanine Vocabulary Speed", ylab="percent/100")
barplot(prop.table(table(X315_vocabaccuracy_Stanine)), col=c.blue, xlab="Stanine Vocabulary Accuracy", ylab="percent/100")

# add biodata to lt_e
#gender
# BirthYear
#country
# education time
# experience time
# management experience time
# Experience area
# university degree

lt_e_gender <- NA
lt_e_BirthYear <- NA
lt_e_country <- NA
lt_e_language <- NA
lt_e_EduTime <- NA
lt_e_Part_ExpTime <- NA
lt_e_Part_ExpManage <- NA
lt_e_Part_ExpArea <- NA
lt_e_Part_University <- NA

for (i in 1:length(lt_e$Part_ID)) {
	lt_e_gender[i] <- x$Part_Gender[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_country[i] <- x$Part_country[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_language[i] <- x$Task_Lid[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_EduTime[i] <- x$Part_EduTime[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==lt_e$Part_ID[i]][1]
	lt_e_Part_University[i] <- x$Part_University[x$Part_ID==lt_e$Part_ID[i]][1]
}
lt_e_gender
lt_e_BirthYear
lt_e_country
lt_e_language
lt_e_EduTime
lt_e_Part_ExpTime
lt_e_Part_ExpManage
lt_e_Part_ExpArea
lt_e_Part_University

lt_e_bio <- as.data.frame(cbind(lt_e_gender,
				lt_e_BirthYear,
				lt_e_country,
				lt_e_language,
				lt_e_EduTime,
				lt_e_Part_ExpTime,
				lt_e_Part_ExpManage,
				lt_e_Part_ExpArea,
				lt_e_Part_University
		))
names(lt_e_bio)

lt_e <- cbind(lt_e, lt_e_bio)
names(lt_e)

### lt_e_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#315				shapes basic - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 315
Norm_ID <- ""
Norm_Name <- "lt-e - fill in norm name here"
Scale <- as.numeric(1:9) 
Mean <- round(as.numeric(c(M315_01, M315_02, M315_03, M315_04, M315_05, M315_06, M315_07, M315_08, M315_09)),2)
sd <- round(c(SD315_01, SD315_02, SD315_03, SD315_04, SD315_05, SD315_06, SD315_07, SD315_08, SD315_09),2)
mSnine <- ""
Scale_Info <- c("fluency accuracy", "fluency raw", "fluency speed", "spelling accuracy", "spelling raw", "spelling speed", "vocabulary accuracy",
		"vocabulary raw", "vocabulary speed")

lt_e_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
lt_e_norms
names(lt_e_norms)

### age
lt_e_BirthYear <- ifelse(shapes_basic_BirthYear==0 , NA, shapes_basic_BirthYear)

age <- (current_year-lt_e_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(lt_e$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(lt_e_gender)
table(lt_e_gender)
#table(lt_e_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(lt_e_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(lt_e_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(lt_e_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$lt_e_Part_University==-1]

lt_e_Part_University <- ifelse(lt_e_Part_University==-1 , NA, lt_e_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(lt_e_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$lt_e_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(lt_e_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$lt_e_Part_EduTime==0]

lt_e_Part_EduTime <- ifelse(lt_e_Part_EduTime==0 , NA, lt_e_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(lt_e_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$lt_e_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$lt_e_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(lt_e_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$lt_e_Part_ExpTime==0]

lt_e_Part_ExpTime <- ifelse(lt_e_Part_ExpTime==0 , NA, lt_e_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(lt_e_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$lt_e_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$lt_e_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(lt_e_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$lt_e_Part_ExpManage==0]

lt_e_Part_ExpManage <- ifelse(lt_e_Part_ExpManage==0 , NA, lt_e_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(shapes_lt_e_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$lt_e_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$lt_e_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(lt_e_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$lt_e_Part_ExpArea==0]

lt_e_Part_ExpArea <- ifelse(lt_e_Part_ExpArea==0 , NA, lt_e_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(lt_e_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$lt_e_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$lt_e_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(lt_e_language)))
language_txt <- ""
for (i in 1:length(freq_language$lt_e_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$lt_e_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(lt_e_country)))
freq_country
country_na <- freq_country$Freq[freq_country$lt_e_country==0]

lt_e_country <- ifelse(lt_e_country==0 , NA, lt_e_country)

freq_country <- as.data.frame(prop.table(table(lt_e_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$lt_e_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$lt_e_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$lt_e_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt


# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)


lt_e_bio <- as.data.frame(cbind(bio_titles,bio_info))
}

##############################################

# save data, each test to a separate sheet
###save.xlsx("_data\\cls.xlsx", cls, cls_deleted, cls_norms, cls_bio)###

############################################
# 102 shapes (management)

if(length(X102_consistencyraw) > 0) {

shapes_management <- as.data.frame(cbind(Part_ID, 
				X102_consistencyraw,
				X102_pointdistribution,
				X102_shapes_01_directing,
				X102_shapes_02_persuasive,
				X102_shapes_03_socially_confident,
				X102_shapes_04_sociable,
				X102_shapes_05_agreeable,
				X102_shapes_06_behavioral,
				X102_shapes_07_prudent,
				X102_shapes_08_focused_on_results,
				X102_shapes_09_systematic,
				X102_shapes_10_conscientious,
				X102_shapes_11_analytical,
				X102_shapes_12_conceptual,
				X102_shapes_13_imaginative,
				X102_shapes_14_open_to_change,
				X102_shapes_15_autonomous,
				X102_shapes_16_achieving,
				X102_shapes_17_competitive,
				X102_shapes_18_energetic,
				X102_shapes_testtime))

names(shapes_management)

# data clearing

# remove rows with low consistency
shapes_management[shapes_management$X102_consistencyraw > 2,]
class(shapes_management)
names(shapes_management)

# remove rows with no results for shapes
shapes_management <- na.omit(shapes_management)

# remove rows with low consistency
del_count <- 0
for (i in 1:length(shapes_management$Part_ID)) {
	if (shapes_management$X102_consistencyraw[i]  <= 2 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("consistency low: ", shapes_management$X102_consistencyraw[i], ", Part_ID:", shapes_management$Part_ID[i], " will be deleted from dataset"))
		shapes_management$X102_consistencyraw[i] <- NA
		#remove(shapes_management[1,])
	}
}
print(paste0(del_count, " cases with consistency <= 2 will be deleted from dataset"))
shapes_management <- na.omit(shapes_management)

# remove rows with testtime shorter than 5 or longer than 60 minutes
del_count <- 0
for (i in 1:length(shapes_management$Part_ID)) {
	if (shapes_management$X102_shapes_testtime[i]  < 5 || shapes_management$X102_shapes_testtime[i] > 60 ) {  # adjust test time thresholds in order to delete cases here
		del_count <- del_count + 1
		print(paste0("time very short or long: ", shapes_management$X102_shapes_testtime[i], ", Part_ID:", shapes_management$Part_ID[i], " will be deleted from dataset"))
		shapes_management$X102_shapes_testtime[i] <- NA
		#remove(shapes_management[1,])
	}
}
print(paste0(del_count, " case(s) with test time < 5 or > 60 will be deleted from dataset"))
shapes_management <- na.omit(shapes_management)

# remove cases with low point distribution
del_count <- 0
for (i in 1:length(shapes_management$Part_ID)) {
	if (shapes_management$X102_pointdistribution[i]  < .8 ) {  # adjust point distribution threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("point distribution low: ", shapes_management$X102_shapes_testtime[i], ", Part_ID:", shapes_management$Part_ID[i], " will be deleted from dataset"))
		shapes_management$X102_shapes_testtime[i] <- NA
		#remove(shapes_management[1,])
	}
}
print(paste0(del_count, " case(s) with point distribution < .8 will be deleted from dataset"))
shapes_management <- na.omit(shapes_management)

names(shapes_management)

# Stanine
M102_01 <- mean(shapes_management$X102_shapes_01_directing, na.rm=TRUE)
M102_02 <- mean(shapes_management$X102_shapes_02_persuasive, na.rm=TRUE)
M102_03 <- mean(shapes_management$X102_shapes_03_socially_confident, na.rm=TRUE)
M102_04 <- mean(shapes_management$X102_shapes_04_sociable, na.rm=TRUE)
M102_05 <- mean(shapes_management$X102_shapes_05_agreeable, na.rm=TRUE)
M102_06 <- mean(shapes_management$X102_shapes_06_behavioral, na.rm=TRUE)
M102_07 <- mean(shapes_management$X102_shapes_07_prudent, na.rm=TRUE)
M102_08 <- mean(shapes_management$X102_shapes_08_focused_on_results)
M102_09 <- mean(shapes_management$X102_shapes_09_systematic, na.rm=TRUE)
M102_10 <- mean(shapes_management$X102_shapes_10_conscientious, na.rm=TRUE)
M102_11 <- mean(shapes_management$X102_shapes_11_analytical, na.rm=TRUE)
M102_12 <- mean(shapes_management$X102_shapes_12_conceptual, na.rm=TRUE)
M102_13 <- mean(shapes_management$X102_shapes_13_imaginative, na.rm=TRUE)
M102_14 <- mean(shapes_management$X102_shapes_14_open_to_change, na.rm=TRUE)
M102_15 <- mean(shapes_management$X102_shapes_15_autonomous, na.rm=TRUE)
M102_16 <- mean(shapes_management$X102_shapes_16_achieving, na.rm=TRUE)
M102_17 <- mean(shapes_management$X102_shapes_17_competitive, na.rm=TRUE)
M102_18 <- mean(shapes_management$X102_shapes_18_energetic, na.rm=TRUE)

SD102_01 <- sd(shapes_management$X102_shapes_01_directing, na.rm=TRUE)
SD102_02 <- sd(shapes_management$X102_shapes_02_persuasive, na.rm=TRUE)
SD102_03 <- sd(shapes_management$X102_shapes_03_socially_confident, na.rm=TRUE)
SD102_04 <- sd(shapes_management$X102_shapes_04_sociable, na.rm=TRUE)
SD102_05 <- sd(shapes_management$X102_shapes_05_agreeable, na.rm=TRUE)
SD102_06 <- sd(shapes_management$X102_shapes_06_behavioral, na.rm=TRUE)
SD102_07 <- sd(shapes_management$X102_shapes_07_prudent, na.rm=TRUE)
SD102_08 <- sd(shapes_management$X102_shapes_08_focused_on_results)
SD102_09 <- sd(shapes_management$X102_shapes_09_systematic, na.rm=TRUE)
SD102_10 <- sd(shapes_management$X102_shapes_10_conscientious, na.rm=TRUE)
SD102_11 <- sd(shapes_management$X102_shapes_11_analytical, na.rm=TRUE)
SD102_12 <- sd(shapes_management$X102_shapes_12_conceptual, na.rm=TRUE)
SD102_13 <- sd(shapes_management$X102_shapes_13_imaginative, na.rm=TRUE)
SD102_14 <- sd(shapes_management$X102_shapes_14_open_to_change, na.rm=TRUE)
SD102_15 <- sd(shapes_management$X102_shapes_15_autonomous, na.rm=TRUE)
SD102_16 <- sd(shapes_management$X102_shapes_16_achieving, na.rm=TRUE)
SD102_17 <- sd(shapes_management$X102_shapes_17_competitive, na.rm=TRUE)
SD102_18 <- sd(shapes_management$X102_shapes_18_energetic, na.rm=TRUE)

X102_shapes_01_directing_Stanine <- round(ifelse((shapes_management$X102_shapes_01_directing - M102_01)/SD102_01*2+5>9 , 9, ifelse((shapes_management$X102_shapes_01_directing - M102_01)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_01_directing - M102_01)/SD102_01*2+5)))
X102_shapes_02_persuasive_Stanine <- round(ifelse((shapes_management$X102_shapes_02_persuasive - M102_02)/SD102_02*2+5>9 , 9, ifelse((shapes_management$X102_shapes_02_persuasive - M102_02)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_02_persuasive - M102_02)/SD102_02*2+5)))
X102_shapes_03_socially_confident_Stanine <- round(ifelse((shapes_management$X102_shapes_03_socially_confident - M102_03)/SD102_03*2+5>9 , 9, ifelse((shapes_management$X102_shapes_03_socially_confident - M102_03)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_03_socially_confident - M102_03)/SD102_03*2+5)))
X102_shapes_04_sociable_Stanine <- round(ifelse((shapes_management$X102_shapes_04_sociable - M102_04)/SD102_04*2+5>9 , 9, ifelse((shapes_management$X102_shapes_04_sociable - M102_04)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_04_sociable - M102_04)/SD102_04*2+5)))
X102_shapes_05_agreeable_Stanine <- round(ifelse((shapes_management$X102_shapes_05_agreeable - M102_05)/SD102_05*2+5>9 , 9, ifelse((shapes_management$X102_shapes_05_agreeable - M102_05)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_05_agreeable - M102_05)/SD102_05*2+5)))
X102_shapes_06_behavioral_Stanine <- round(ifelse((shapes_management$X102_shapes_06_behavioral - M102_06)/SD102_06*2+5>9 , 9, ifelse((shapes_management$X102_shapes_06_behavioral - M102_06)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_06_behavioral - M102_06)/SD102_06*2+5)))
X102_shapes_07_prudent_Stanine <- round(ifelse((shapes_management$X102_shapes_07_prudent - M102_07)/SD102_07*2+5>9 , 9, ifelse((shapes_management$X102_shapes_07_prudent - M102_07)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_07_prudent - M102_07)/SD102_07*2+5)))
X102_shapes_08_focused_on_results_Stanine <- round(ifelse((shapes_management$X102_shapes_08_focused_on_results - M102_08)/SD102_08*2+5>9 , 9, ifelse((shapes_management$X102_shapes_08_focused_on_results - M102_08)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_08_focused_on_results - M102_08)/SD102_08*2+5)))
X102_shapes_09_systematic_Stanine <- round(ifelse((shapes_management$X102_shapes_09_systematic - M102_09)/SD102_09*2+5>9 , 9, ifelse((shapes_management$X102_shapes_09_systematic - M102_09)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_09_systematic - M102_09)/SD102_09*2+5)))
X102_shapes_10_conscientious_Stanine <- round(ifelse((shapes_management$X102_shapes_10_conscientious - M102_10)/SD102_10*2+5>9 , 9, ifelse((shapes_management$X102_shapes_10_conscientious - M102_10)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_10_conscientious - M102_10)/SD102_10*2+5)))
X102_shapes_11_analytical_Stanine <- round(ifelse((shapes_management$X102_shapes_11_analytical - M102_11)/SD102_11*2+5>9 , 9, ifelse((shapes_management$X102_shapes_11_analytical - M102_11)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_11_analytical - M102_11)/SD102_11*2+5)))
X102_shapes_12_conceptual_Stanine <- round(ifelse((shapes_management$X102_shapes_12_conceptual - M102_12)/SD102_12*2+5>9 , 9, ifelse((shapes_management$X102_shapes_12_conceptual - M102_12)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_12_conceptual - M102_12)/SD102_12*2+5)))
X102_shapes_13_imaginative_Stanine <- round(ifelse((shapes_management$X102_shapes_13_imaginative - M102_13)/SD102_13*2+5>9 , 9, ifelse((shapes_management$X102_shapes_13_imaginative - M102_13)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_13_imaginative - M102_13)/SD102_13*2+5)))
X102_shapes_14_open_to_change_Stanine <- round(ifelse((shapes_management$X102_shapes_14_open_to_change - M102_14)/SD102_14*2+5>9 , 9, ifelse((shapes_management$X102_shapes_14_open_to_change - M102_14)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_14_open_to_change - M102_14)/SD102_14*2+5)))
X102_shapes_15_autonomous_Stanine <- round(ifelse((shapes_management$X102_shapes_15_autonomous - M102_15)/SD102_15*2+5>9 , 9, ifelse((shapes_management$X102_shapes_15_autonomous - M102_15)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_15_autonomous - M102_15)/SD102_15*2+5)))
X102_shapes_16_achieving_Stanine <- round(ifelse((shapes_management$X102_shapes_16_achieving - M102_16)/SD102_16*2+5>9 , 9, ifelse((shapes_management$X102_shapes_16_achieving - M102_16)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_16_achieving - M102_16)/SD102_16*2+5)))
X102_shapes_17_competitive_Stanine <- round(ifelse((shapes_management$X102_shapes_17_competitive - M102_17)/SD102_17*2+5>9 , 9, ifelse((shapes_management$X102_shapes_17_competitive - M102_17)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_17_competitive - M102_17)/SD102_17*2+5)))
X102_shapes_18_energetic_Stanine <- round(ifelse((shapes_management$X102_shapes_18_energetic - M102_18)/SD102_18*2+5>9 , 9, ifelse((shapes_management$X102_shapes_18_energetic - M102_18)/SD102_01*2+5<1, 1, (shapes_management$X102_shapes_18_energetic - M102_18)/SD102_18*2+5)))

#class(shapes_management)

shapes_management <- cbind(shapes_management,
	X102_shapes_01_directing_Stanine,
	X102_shapes_02_persuasive_Stanine,
	X102_shapes_03_socially_confident_Stanine,
	X102_shapes_04_sociable_Stanine,
	X102_shapes_05_agreeable_Stanine,
	X102_shapes_06_behavioral_Stanine,
	X102_shapes_07_prudent_Stanine,
	X102_shapes_08_focused_on_results_Stanine,
	X102_shapes_09_systematic_Stanine,
	X102_shapes_10_conscientious_Stanine,
	X102_shapes_11_analytical_Stanine,
	X102_shapes_12_conceptual_Stanine,
	X102_shapes_13_imaginative_Stanine,
	X102_shapes_14_open_to_change_Stanine,
	X102_shapes_15_autonomous_Stanine,
	X102_shapes_16_achieving_Stanine,
	X102_shapes_17_competitive_Stanine,
	X102_shapes_18_energetic_Stanine

)

colnames(shapes_management)
class(shapes_management)
shapes_management <- as.data.frame(shapes_management)
names(shapes_management)

# diagrams stanine


#freq_s01 <- as.data.frame(prop.table(table(X102_shapes_01_directing_Stanine)))

#plot(freq_s01$X102_shapes_01_directing_Stanine,freq_s01$Freq*100, col=c.blue)

barplot(prop.table(table(X102_shapes_01_directing_Stanine)), col=c.blue, xlab="Stanine Professional Challenge", ylab="percent/100")
barplot(prop.table(table(X102_shapes_02_persuasive_Stanine)), col=c.blue, xlab="Stanine persuasive", ylab="percent/100")
barplot(prop.table(table(X102_shapes_03_socially_confident_Stanine)), col=c.blue, xlab="Stanine socially_confident", ylab="percent/100")
barplot(prop.table(table(X102_shapes_04_sociable_Stanine)), col=c.blue, xlab="Stanine sociable", ylab="percent/100")
barplot(prop.table(table(X102_shapes_05_agreeable_Stanine)), col=c.blue, xlab="Stanine agreeable", ylab="percent/100")
barplot(prop.table(table(X102_shapes_06_behavioral_Stanine)), col=c.blue, xlab="Stanine behavioral", ylab="percent/100")
barplot(prop.table(table(X102_shapes_07_prudent_Stanine)), col=c.blue, xlab="Stanine prudent", ylab="percent/100")
##barplot(prop.table(table(X102_shapes_08_focused_on_results_Stanine)), col=c.blue, xlab="Stanine focused_on_results", ylab="percent/100")
barplot(prop.table(table(X102_shapes_09_systematic_Stanine)), col=c.blue, xlab="Stanine systematic", ylab="percent/100")
barplot(prop.table(table(X102_shapes_10_conscientious_Stanine)), col=c.blue, xlab="Stanine conscientious", ylab="percent/100")
barplot(prop.table(table(X102_shapes_11_analytical_Stanine)), col=c.blue, xlab="Stanine analytical", ylab="percent/100")
barplot(prop.table(table(X102_shapes_12_conceptual_Stanine)), col=c.blue, xlab="Stanine conceptual", ylab="percent/100")
barplot(prop.table(table(X102_shapes_13_imaginative_Stanine)), col=c.blue, xlab="Stanine imaginative", ylab="percent/100")
barplot(prop.table(table(X102_shapes_14_open_to_change_Stanine)), col=c.blue, xlab="Stanine open_to_change", ylab="percent/100")
##barplot(prop.table(table(X102_shapes_15_autonomous_Stanine)), col=c.blue, xlab="Stanine autonomous", ylab="percent/100")
barplot(prop.table(table(X102_shapes_16_achieving_Stanine)), col=c.blue, xlab="Stanine achieving", ylab="percent/100")
barplot(prop.table(table(X102_shapes_17_competitive_Stanine)), col=c.blue, xlab="Stanine competitive", ylab="percent/100")
barplot(prop.table(table(X102_shapes_18_energetic_Stanine)), col=c.blue, xlab="Stanine energetic", ylab="percent/100")



# add biodata to shapes management
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

shapes_management_gender <- NA
shapes_management_BirthYear <- NA
shapes_management_country <- NA
shapes_management_language <- NA
shapes_management_Part_EduTime <- NA
shapes_management_Part_ExpTime <- NA
shapes_management_Part_ExpManage <- NA
shapes_management_Part_ExpArea <- NA
shapes_management_Part_University <- NA

for (i in 1:length(shapes_management$Part_ID)) {
	shapes_management_gender[i] <- x$Part_Gender[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_country[i] <- x$Part_country[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_language[i] <- x$Task_Lid[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==shapes_management$Part_ID[i]][1]
	shapes_management_Part_University[i] <- x$Part_University[x$Part_ID==shapes_management$Part_ID[i]][1]
}
shapes_management_gender
shapes_management_BirthYear
shapes_management_country
shapes_management_language
shapes_management_Part_EduTime
shapes_management_Part_ExpTime
shapes_management_Part_ExpManage
shapes_management_Part_ExpArea
shapes_management_Part_University

shapes_management_bio <- as.data.frame(cbind(shapes_management_gender,
				shapes_management_BirthYear,
				shapes_management_country,
				shapes_management_language,
				shapes_management_Part_EduTime,
				shapes_management_Part_ExpTime,
				shapes_management_Part_ExpManage,
				shapes_management_Part_ExpArea,
				shapes_management_Part_University
				))
names(shapes_management_bio)

shapes_management <- cbind(shapes_management, shapes_management_bio)
names(shapes_management)
				
### shapes_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#102				shapes management 		16,39	3,54			Professional Challenge
Inst_ID <- 102
Norm_ID <- ""
Norm_Name <- "shapes management - fill in norm name here"
Scale <- as.numeric(1:18) 
Mean <- round(as.numeric(c(M102_01, M102_02, M102_03, M102_04, M102_05, M102_06, M102_07, M102_08, M102_09, M102_10, M102_11, M102_12, M102_13, M102_14, M102_15, M102_16, M102_17, M102_18)),2)
sd <- round(c(SD102_01, SD102_02, SD102_03, SD102_04, SD102_05, SD102_06, SD102_07, SD102_08, SD102_09, SD102_10, SD102_11, SD102_12, SD102_13, SD102_14, SD102_15, SD102_16, SD102_17, SD102_18),2)
mSnine <- ""
Scale_Info <- c("directing", "persuasive", "socially_confident", "sociable", "agreeable", "behavioral", "prudent",
"focused on results", "systematic", "conscientious", "analytical", "conceptual", "imaginative", "open to change", "autonomous", "achieving", "competitive", "energetic")

shapes_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
shapes_norms
names(shapes_norms)

# shapes bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
"languages", "country", "education time")

### age
shapes_management_BirthYear <- ifelse(shapes_management_BirthYear==0 , NA, shapes_management_BirthYear)

age <- (current_year-shapes_management_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
if (freq_age$Freq[i]>=0.05) {
	age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
	age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(shapes_management$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

#class(shapes_management_gender)
#table(shapes_management_gender)
#table(shapes_management_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(shapes_management_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(shapes_management_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(shapes_management_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$shapes_management_Part_University==-1]

shapes_management_Part_University <- ifelse(shapes_management_Part_University==-1 , NA, shapes_management_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(shapes_management_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$shapes_management_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(shapes_management_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$shapes_management_Part_EduTime==0]

shapes_management_Part_EduTime <- ifelse(shapes_management_Part_EduTime==0 , NA, shapes_management_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(shapes_management_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$shapes_management_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$shapes_management_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt
					
# experience time
				freq_ExpTime <- as.data.frame(prop.table(table(shapes_management_Part_ExpTime)))
				freq_ExpTime
				ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$shapes_management_Part_ExpTime==0]
				
				shapes_management_Part_ExpTime <- ifelse(shapes_management_Part_ExpTime==0 , NA, shapes_management_Part_ExpTime)
				freq_ExpTime <- as.data.frame(prop.table(table(shapes_management_Part_ExpTime)))
				
				ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpTime$shapes_management_Part_ExpTime)){
					if (freq_ExpTime$Freq[i]>=0.05) {
						ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$shapes_management_Part_ExpTime[i]))],";  others < 5%")
					}
				}
	
				work_exp <- ExpTime_txt
							
# management experience
				freq_ExpManage <- as.data.frame(prop.table(table(shapes_management_Part_ExpManage)))
				freq_ExpManage
				ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$shapes_management_Part_ExpManage==0]
				
				shapes_management_Part_ExpManage <- ifelse(shapes_management_Part_ExpManage==0 , NA, shapes_management_Part_ExpManage)
				freq_ExpManage <- as.data.frame(prop.table(table(shapes_management_Part_ExpManage)))
				
				ExpManage_txt <- paste0(round(ExpManage_na,4)*100,"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpManage$shapes_management_Part_ExpManage)){
					if (freq_ExpManage$Freq[i]>=0.05) {
						ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i],4)*100, "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$shapes_management_Part_ExpManage[i]))],"; others < 5%")
					}
				}
				
				manage_exp <- ExpManage_txt	
				
# exp area

freq_ExpArea <- as.data.frame(prop.table(table(shapes_management_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$shapes_management_Part_ExpArea==0]

shapes_management_Part_ExpArea <- ifelse(shapes_management_Part_ExpArea==0 , NA, shapes_management_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(shapes_management_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na,4)*100,"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$shapes_management_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$shapes_management_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(shapes_management_language)))
language_txt <- ""
for (i in 1:length(freq_language$shapes_management_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$shapes_management_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(shapes_management_country)))
freq_country
country_na <- freq_country$Freq[freq_country$shapes_management_country==0]

shapes_management_country <- ifelse(shapes_management_country==0 , NA, shapes_management_country)

freq_country <- as.data.frame(prop.table(table(shapes_management_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$shapes_management_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$shapes_management_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$shapes_management_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

shapes_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\shapes_management.xlsx", shapes_management, shapes_management_deleted, shapes_management_norms, shapes_management_bio)

}

#########################################################
#### 332 verbal (admin)

if(length(X332_accuracy) > 0) {

verbal_admin <- as.data.frame(cbind(Part_ID, 
				X332_overallperformance,
				X332_speed,
				X332_accuracy
		)) 

names(verbal_admin)

# data clearing

# remove rows with no results 
length(verbal_admin$Part_ID)
verbal_admin <- na.omit(verbal_admin)
length(verbal_admin$Part_ID)

# remove rows with very low accuracy
del_count <- 0
verbal_admin_deleted <- ""

for (i in 1:length(verbal_admin$Part_ID)) {
	if (verbal_admin$X332_accuracy[i]  <= 33 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", verbal_admin$X332_accuracy[i] , ", Part_ID:", verbal_admin$Part_ID[i] , " will be deleted from dataset"))
		verbal_admin_deleted<-rbind(verbal_admin_deleted,verbal_admin[i,1:4])
		verbal_admin$X332_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with accuracy <= 33 will be deleted from dataset"))
verbal_admin <- na.omit(verbal_admin)
length(verbal_admin$Part_ID)

# remove rows with testtime shorter than 5 or longer than 60 minutes
del_count <- 0
for (i in 1:length(verbal_admin$Part_ID)) {
	if (verbal_admin$X332_speed[i]  < 5 ) {  # adjust test time thresholds in order to delete cases here
		del_count <- del_count + 1
		print(paste0("time very short or long: ", verbal_admin$X332_speed[i], ", Part_ID:", verbal_admin$Part_ID[i], " will be deleted from dataset"))
		verbal_admin$X332_speed[i] <- NA
		#remove(shapes_basic[1,])
	}
}
print(paste0(del_count, " case(s) with speed < 5 "))
verbal_admin <- na.omit(verbal_admin)

#
verbal_admin_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= ","acc <= 33")) # only once for a test 
names(verbal_admin_deleted_txt) <- c("Part_ID", "X332_overallperformance", "X332_speed", "X332_accuracy")
verbal_admin_deleted <- rbind(verbal_admin_deleted_txt,verbal_admin_deleted)

# Stanine
M332_01 <- mean(verbal_admin$X332_overallperformance, na.rm=TRUE)
M332_02 <- mean(verbal_admin$X332_speed, na.rm=TRUE)
M332_03 <- mean(verbal_admin$X332_accuracy, na.rm=TRUE)

SD332_01 <- sd(verbal_admin$X332_overallperformance, na.rm=TRUE)
SD332_02 <- sd(verbal_admin$X332_speed, na.rm=TRUE)
SD332_03 <- sd(verbal_admin$X332_accuracy, na.rm=TRUE)

X332_overallperformance_Stanine <- round(ifelse((verbal_admin$X332_overallperformance- M332_01)/SD332_01*2+5>9 , 9, ifelse((verbal_admin$X332_overallperformance- M332_01)/SD332_01*2+5<1, 1, (verbal_admin$X332_overallperformance- M332_01)/SD332_01*2+5)))
X332_speed_Stanine <- round(ifelse((verbal_admin$X332_speed - M332_02)/SD332_02*2+5>9 , 9, ifelse((verbal_admin$X332_speed - M332_02)/SD332_01*2+5<1, 1, (verbal_admin$X332_speed - M332_02)/SD332_02*2+5)))
X332_accuracy_Stanine <- round(ifelse((verbal_admin$X332_accuracy- M332_03)/SD332_03*2+5>9 , 9, ifelse((verbal_admin$X332_accuracy- M332_03)/SD332_01*2+5<1, 1, (verbal_admin$X332_accuracy- M332_03)/SD332_03*2+5)))

verbal_admin <- cbind(verbal_admin,
		X332_overallperformance_Stanine,
		X332_speed_Stanine, 
		X332_accuracy_Stanine 		
)

colnames(verbal_admin)
class(verbal_admin)
verbal_admin <- as.data.frame(verbal_admin)
names(verbal_admin)

png(file = "_images/verb_admin_overall.jpg")  #name of chart file
barplot(prop.table(table(X332_overallperformance_Stanine)), col=c.blue, xlab="verbal (admin) Stanine Overallperformance", ylab="percent/100")
dev.off()

png(file = "_images/verb_admin_speed.jpg")  #name of chart file
barplot(prop.table(table(X332_speed_Stanine)), col=c.blue, xlab="verbal (admin) Stanine Speed", ylab="percent/100")
dev.off()

png(file = "_images/verb_admin_acc.jpg")  #name of chart file
barplot(prop.table(table(X332_accuracy_Stanine)), col=c.blue, xlab="verbal (admin) Stanine Accuracy", ylab="percent/100")
dev.off()

#############
# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

verbal_admin_gender <- NA
verbal_admin_BirthYear <- NA
verbal_admin_country <- NA
verbal_admin_language <- NA
verbal_admin_Part_EduTime <- NA
verbal_admin_Part_ExpTime <- NA
verbal_admin_Part_ExpManage <- NA
verbal_admin_Part_ExpArea <- NA
verbal_admin_Part_University <- NA

for (i in 1:length(verbal_admin$Part_ID)) {
	verbal_admin_gender[i] <- x$Part_Gender[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_country[i] <- x$Part_country[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_language[i] <- x$Task_Lid[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==verbal_admin$Part_ID[i]][1]
	verbal_admin_Part_University[i] <- x$Part_University[x$Part_ID==verbal_admin$Part_ID[i]][1]
}
verbal_admin_gender
verbal_admin_BirthYear
verbal_admin_country
verbal_admin_language
verbal_admin_Part_EduTime
verbal_admin_Part_ExpTime
verbal_admin_Part_ExpManage
verbal_admin_Part_ExpArea
verbal_admin_Part_University

verbal_admin_bio <- as.data.frame(cbind(verbal_admin_gender,
				verbal_admin_BirthYear,
				verbal_admin_country,
				verbal_admin_language,
				verbal_admin_Part_EduTime,
				verbal_admin_Part_ExpTime,
				verbal_admin_Part_ExpManage,
				verbal_admin_Part_ExpArea,
				verbal_admin_Part_University
		))
names(verbal_admin_bio)

verbal_admin <- cbind(verbal_admin, verbal_admin_bio)
names(verbal_admin)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 332
Norm_ID <- ""
Norm_Name <- "verbal (admin)"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M332_01, M332_02, M332_03)),2)
sd <- round(c(SD332_01, SD332_02, SD332_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

verbal_admin_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
verbal_admin_norms
names(verbal_admin_norms)

### age
verbal_admin_BirthYear <- ifelse(verbal_admin_BirthYear==0 , NA, verbal_admin_BirthYear)

age <- (current_year-verbal_admin_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(verbal_admin$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(verbal_admin_gender)
table(verbal_admin_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(verbal_admin_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(verbal_admin_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$verbal_admin_Part_University==-1]

verbal_admin_Part_University <- ifelse(verbal_admin_Part_University==-1 , NA, verbal_admin_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(verbal_admin_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$verbal_admin_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(verbal_admin_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$verbal_admin_Part_EduTime==0]

verbal_admin_Part_EduTime <- ifelse(verbal_admin_Part_EduTime==0 , NA, verbal_admin_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(verbal_admin_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$verbal_admin_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$verbal_admin_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(verbal_admin_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$verbal_admin_Part_ExpTime==0]

verbal_admin_Part_ExpTime <- ifelse(verbal_admin_Part_ExpTime==0 , NA, verbal_admin_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(verbal_admin_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$verbal_admin_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$verbal_admin_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(verbal_admin_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$verbal_admin_Part_ExpManage==0]

verbal_admin_Part_ExpManage <- ifelse(verbal_admin_Part_ExpManage==0 , NA, verbal_admin_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(verbal_admin_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$verbal_admin_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$verbal_admin_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(verbal_admin_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$verbal_admin_Part_ExpArea==0]

verbal_admin_Part_ExpArea <- ifelse(verbal_admin_Part_ExpArea==0 , NA, verbal_admin_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(verbal_admin_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$verbal_admin_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$verbal_admin_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(verbal_admin_language)))
language_txt <- ""
for (i in 1:length(freq_language$verbal_admin_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$verbal_admin_language[i]))],"; others < 5%")
	}
}

language <- language_txt
# country

freq_country <- as.data.frame(prop.table(table(verbal_admin_country)))
freq_country
country_na <- freq_country$Freq[freq_country$verbal_admin_country==0]

verbal_admin_country <- ifelse(verbal_admin_country==0 , NA, verbal_admin_country)

freq_country <- as.data.frame(prop.table(table(verbal_admin_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$verbal_admin_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$verbal_admin_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

verbal_admin_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\verbal_admin.xlsx", verbal_admin, verbal_admin_deleted, verbal_admin_norms, verbal_admin_bio)
}

#########################################################
#### 752 verbal (instruct)

if(length(X752_accuracy) > 0) {

verbal_instruct <- as.data.frame(cbind(Part_ID, 
				X752_overallperformance,
				X752_speed,
				X752_accuracy
		)) 

names(verbal_instruct)


# data clearing

# remove rows with no results for numerical
length(verbal_instruct$Part_ID)
verbal_instruct <- na.omit(verbal_instruct)
length(verbal_instruct$Part_ID)

# remove rows with very low accuracy
del_count <- 0
verbal_instruct_deleted <- ""

for (i in 1:length(verbal_instruct$Part_ID)) {
	if (verbal_instruct$X752_accuracy[i]  <= 33 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", verbal_instruct$X752_accuracy[i] , ", Part_ID:", verbal_instruct$Part_ID[i] , " will be deleted from dataset"))
		verbal_instruct_deleted<-rbind(verbal_instruct_deleted,verbal_instruct[i,1:4])
		verbal_instruct$X752_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with accuracy <= 33 will be deleted from dataset"))
verbal_instruct <- na.omit(verbal_instruct)
length(verbal_instruct$Part_ID)

#
verbal_instruct_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= ","acc <= 33")) # only once for a test 
names(verbal_instruct_deleted_txt) <- c("Part_ID", "X752_overallperformance", "X752_speed", "X752_accuracy")
verbal_instruct_deleted <- rbind(verbal_instruct_deleted_txt,verbal_instruct_deleted)

# Stanine
M752_01 <- mean(verbal_instruct$X752_overallperformance, na.rm=TRUE)
M752_02 <- mean(verbal_instruct$X752_speed, na.rm=TRUE)
M752_03 <- mean(verbal_instruct$X752_accuracy, na.rm=TRUE)

SD752_01 <- sd(verbal_instruct$X752_overallperformance, na.rm=TRUE)
SD752_02 <- sd(verbal_instruct$X752_speed, na.rm=TRUE)
SD752_03 <- sd(verbal_instruct$X752_accuracy, na.rm=TRUE)

X752_overallperformance_Stanine <- round(ifelse((verbal_instruct$X752_overallperformance- M752_01)/SD752_01*2+5>9 , 9, ifelse((verbal_instruct$X752_overallperformance- M752_01)/SD752_01*2+5<1, 1, (verbal_instruct$X752_overallperformance- M752_01)/SD752_01*2+5)))
X752_speed_Stanine <- round(ifelse((verbal_instruct$X752_speed - M752_02)/SD752_02*2+5>9 , 9, ifelse((verbal_instruct$X752_speed - M752_02)/SD752_01*2+5<1, 1, (verbal_instruct$X752_speed - M752_02)/SD752_02*2+5)))
X752_accuracy_Stanine <- round(ifelse((verbal_instruct$X752_accuracy- M752_03)/SD752_03*2+5>9 , 9, ifelse((verbal_instruct$X752_accuracy- M752_03)/SD752_01*2+5<1, 1, (verbal_instruct$X752_accuracy- M752_03)/SD752_03*2+5)))

verbal_instruct <- cbind(verbal_instruct,
		X752_overallperformance_Stanine,
		X752_speed_Stanine, 
		X752_accuracy_Stanine 		
)

colnames(verbal_instruct)
class(verbal_instruct)
verbal_instruct <- as.data.frame(verbal_instruct)
names(verbal_instruct)

barplot(prop.table(table(X752_overallperformance_Stanine)), col=c.blue, xlab="verbal (instruct) Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X752_speed_Stanine)), col=c.blue, xlab="verbal (instruct) Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X752_accuracy_Stanine)), col=c.blue, xlab="verbal (instruct) Stanine Accuracy", ylab="percent/100")

#############
# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

verbal_instruct_gender <- NA
verbal_instruct_BirthYear <- NA
verbal_instruct_country <- NA
verbal_instruct_language <- NA
verbal_instruct_Part_EduTime <- NA
verbal_instruct_Part_ExpTime <- NA
verbal_instruct_Part_ExpManage <- NA
verbal_instruct_Part_ExpArea <- NA
verbal_instruct_Part_University <- NA

for (i in 1:length(verbal_instruct$Part_ID)) {
	verbal_instruct_gender[i] <- x$Part_Gender[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_country[i] <- x$Part_country[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_language[i] <- x$Task_Lid[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==verbal_instruct$Part_ID[i]][1]
	verbal_instruct_Part_University[i] <- x$Part_University[x$Part_ID==verbal_instruct$Part_ID[i]][1]
}
verbal_instruct_gender
verbal_instruct_BirthYear
verbal_instruct_country
verbal_instruct_language
verbal_instruct_Part_EduTime
verbal_instruct_Part_ExpTime
verbal_instruct_Part_ExpManage
verbal_instruct_Part_ExpArea
verbal_instruct_Part_University

verbal_instruct_bio <- as.data.frame(cbind(verbal_instruct_gender,
				verbal_instruct_BirthYear,
				verbal_instruct_country,
				verbal_instruct_language,
				verbal_instruct_Part_EduTime,
				verbal_instruct_Part_ExpTime,
				verbal_instruct_Part_ExpManage,
				verbal_instruct_Part_ExpArea,
				verbal_instruct_Part_University
		))
names(verbal_instruct_bio)

verbal_instruct <- cbind(verbal_instruct, verbal_instruct_bio)
names(verbal_instruct)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 752
Norm_ID <- ""
Norm_Name <- "verbal (instruct)"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M752_01, M752_02, M752_03)),2)
sd <- round(c(SD752_01, SD752_02, SD752_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

verbal_instruct_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
verbal_instruct_norms
names(verbal_instruct_norms)

### age
verbal_instruct_BirthYear <- ifelse(verbal_instruct_BirthYear==0 , NA, verbal_instruct_BirthYear)

age <- (current_year-verbal_instruct_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(verbal_instruct$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(verbal_instruct_gender)
table(verbal_instruct_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(verbal_instruct_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(verbal_instruct_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$verbal_instruct_Part_University==-1]

verbal_instruct_Part_University <- ifelse(verbal_instruct_Part_University==-1 , NA, verbal_instruct_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(verbal_instruct_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$verbal_instruct_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(verbal_instruct_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$verbal_instruct_Part_EduTime==0]

verbal_instruct_Part_EduTime <- ifelse(verbal_instruct_Part_EduTime==0 , NA, verbal_instruct_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(verbal_instruct_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$verbal_instruct_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$verbal_instruct_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$verbal_instruct_Part_ExpTime==0]

verbal_instruct_Part_ExpTime <- ifelse(verbal_instruct_Part_ExpTime==0 , NA, verbal_instruct_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$verbal_instruct_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$verbal_instruct_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$verbal_instruct_Part_ExpManage==0]

verbal_instruct_Part_ExpManage <- ifelse(verbal_instruct_Part_ExpManage==0 , NA, verbal_instruct_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$verbal_instruct_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$verbal_instruct_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$verbal_instruct_Part_ExpArea==0]

verbal_instruct_Part_ExpArea <- ifelse(verbal_instruct_Part_ExpArea==0 , NA, verbal_instruct_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(verbal_instruct_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$verbal_instruct_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$verbal_instruct_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(verbal_instruct_language)))
language_txt <- ""
for (i in 1:length(freq_language$verbal_instruct_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$verbal_instruct_language[i]))],"; others < 5%")
	}
}

language <- language_txt
# country

freq_country <- as.data.frame(prop.table(table(verbal_instruct_country)))
freq_country
country_na <- freq_country$Freq[freq_country$verbal_instruct_country==0]

verbal_instruct_country <- ifelse(verbal_instruct_country==0 , NA, verbal_instruct_country)

freq_country <- as.data.frame(prop.table(table(verbal_instruct_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$verbal_instruct_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$verbal_instruct_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

verbal_instruct_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\verbal_instruct.xlsx", verbal_instruct, verbal_instruct_deleted, verbal_instruct_norms, verbal_instruct_bio)
}

#########################################################
#### 331 numerical (admin)

if (length(X331_overallperformance) > 0 ) {
num_admin <- as.data.frame(cbind(Part_ID, 
				X331_overallperformance,
				X331_speed,
				X331_accuracy
						)) 

names(num_admin)


# data clearing

# remove rows with no results for numerical
length(num_admin$Part_ID)
num_admin <- na.omit(num_admin)
length(num_admin$Part_ID)

# remove rows with very low accuracy
del_count <- 0
num_admin_deleted <- ""

for (i in 1:length(num_admin$Part_ID)) {
	if (num_admin$X331_accuracy[i]  <= 33 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("accuracy low: ", num_admin$X331_accuracy[i] , ", Part_ID:", num_admin$Part_ID[i] , " will be deleted from dataset"))
		num_admin_deleted<-rbind(num_admin_deleted,num_admin[i,1:4])
		num_admin$X331_accuracy[i]<- NA # remove value
	}
}
print(paste0(del_count, " cases with accuracy <= 33 will be deleted from dataset"))
num_admin <- na.omit(num_admin)
length(num_admin$Part_ID)


# remove rows with testtime shorter than 5 or longer than 60 minutes
del_count <- 0
for (i in 1:length(num_admin$Part_ID)) {
	if (num_admin$X331_speed[i]  < 5 ) {  # adjust test time thresholds in order to delete cases here
		del_count <- del_count + 1
		print(paste0("time very short or long: ", num_admin$X331_speed[i], ", Part_ID:", num_admin$Part_ID[i], " will be deleted from dataset"))
		num_admin$X331_speed[i] <- NA
		#remove(shapes_basic[1,])
	}
}
print(paste0(del_count, " case(s) with speed < 5 "))
num_admin <- na.omit(num_admin)

#
num_admin_deleted_txt <- as.data.frame(cbind("thresholds for deletion","","sp <= ","acc <= 33")) # only once for a test 
names(num_admin_deleted_txt) <- c("Part_ID", "X331_overallperformance", "X331_speed", "X331_accuracy")
num_admin_deleted <- rbind(num_admin_deleted_txt,num_admin_deleted)

# Stanine
M331_01 <- mean(num_admin$X331_overallperformance, na.rm=TRUE)
M331_02 <- mean(num_admin$X331_speed, na.rm=TRUE)
M331_03 <- mean(num_admin$X331_accuracy, na.rm=TRUE)

SD331_01 <- sd(num_admin$X331_overallperformance, na.rm=TRUE)
SD331_02 <- sd(num_admin$X331_speed, na.rm=TRUE)
SD331_03 <- sd(num_admin$X331_accuracy, na.rm=TRUE)

X331_overallperformance_Stanine <- round(ifelse((num_admin$X331_overallperformance- M331_01)/SD331_01*2+5>9 , 9, ifelse((num_admin$X331_overallperformance- M331_01)/SD331_01*2+5<1, 1, (num_admin$X331_overallperformance- M331_01)/SD331_01*2+5)))
X331_speed_Stanine <- round(ifelse((num_admin$X331_speed - M331_02)/SD331_02*2+5>9 , 9, ifelse((num_admin$X331_speed - M331_02)/SD331_01*2+5<1, 1, (num_admin$X331_speed - M331_02)/SD331_02*2+5)))
X331_accuracy_Stanine <- round(ifelse((num_admin$X331_accuracy- M331_03)/SD331_03*2+5>9 , 9, ifelse((num_admin$X331_accuracy- M331_03)/SD331_01*2+5<1, 1, (num_admin$X331_accuracy- M331_03)/SD331_03*2+5)))

num_admin <- cbind(num_admin,
		X331_overallperformance_Stanine,
		X331_speed_Stanine, 
		X331_accuracy_Stanine 		
)

colnames(num_admin)
class(num_admin)
num_admin <- as.data.frame(num_admin)
names(num_admin)

png(file = "_images/num_admin_overall.jpg")  #name of chart file
barplot(prop.table(table(X331_overallperformance_Stanine)), col=c.blue, xlab="num (admin) Stanine Overallperformance", ylab="percent/100")
dev.off()

png(file = "_images/num_admin_speed.jpg")  #name of chart file
barplot(prop.table(table(X331_speed_Stanine)), col=c.blue, xlab="num (admin) Stanine Speed", ylab="percent/100")
dev.off()

png(file = "_images/num_admin_acc.jpg")  #name of chart file
barplot(prop.table(table(X331_accuracy_Stanine)), col=c.blue, xlab="num (admin) Stanine Accuracy", ylab="percent/100")
dev.off()

barplot(prop.table(table(X331_overallperformance_Stanine)), col=c.blue, xlab="numerical (admin) Stanine Overallperformance", ylab="percent/100")
barplot(prop.table(table(X331_speed_Stanine)), col=c.blue, xlab="numerical (admin) Stanine Speed", ylab="percent/100")
barplot(prop.table(table(X331_accuracy_Stanine)), col=c.blue, xlab="numerical (admin) Stanine Accuracy", ylab="percent/100")

# add biodata
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

num_admin_gender <- NA
num_admin_BirthYear <- NA
num_admin_country <- NA
num_admin_language <- NA
num_admin_Part_EduTime <- NA
num_admin_Part_ExpTime <- NA
num_admin_Part_ExpManage <- NA
num_admin_Part_ExpArea <- NA
num_admin_Part_University <- NA

for (i in 1:length(num_admin$Part_ID)) {
	num_admin_gender[i] <- x$Part_Gender[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_country[i] <- x$Part_country[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_language[i] <- x$Task_Lid[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==num_admin$Part_ID[i]][1]
	num_admin_Part_University[i] <- x$Part_University[x$Part_ID==num_admin$Part_ID[i]][1]
}

num_admin_gender
num_admin_BirthYear
num_admin_country
num_admin_language
num_admin_Part_EduTime
num_admin_Part_ExpTime
num_admin_Part_ExpManage
num_admin_Part_ExpArea
num_admin_Part_University

num_admin_bio <- as.data.frame(cbind(num_admin_gender,
				num_admin_BirthYear,
				num_admin_country,
				num_admin_language,
				num_admin_Part_EduTime,
				num_admin_Part_ExpTime,
				num_admin_Part_ExpManage,
				num_admin_Part_ExpArea,
				num_admin_Part_University
		))
names(num_admin_bio)

num_admin <- cbind(num_admin, num_admin_bio)
names(num_admin)

### scales_num(fin_cp)_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#701				scales_num(fin_cp) - AXA	1		16,39	3,54			Professional Challenge
Inst_ID <- 331
Norm_ID <- ""
Norm_Name <- "numerical (admin)"
Scale <- as.numeric(1:3) 
Mean <- round(as.numeric(c(M331_01, M331_02, M331_03)),2)
sd <- round(c(SD331_01, SD331_02, SD331_03),2)
mSnine <- ""
Scale_Info <- c("Performance", "Speed", "Accuracy")

num_admin_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
num_admin_norms
names(num_admin_norms)

### age
num_admin_BirthYear <- ifelse(num_admin_BirthYear==0 , NA, num_admin_BirthYear)

age <- (current_year-num_admin_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
	if (freq_age$Freq[i]>=0.05) {
		age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
		age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
	}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(num_admin$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

class(num_admin_gender)
table(num_admin_gender)
#table(scales_num(fin_cp)_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(num_admin_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

freq_uni_degree <- as.data.frame(prop.table(table(num_admin_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$num_admin_Part_University==-1]

num_admin_Part_University <- ifelse(num_admin_Part_University==-1 , NA, num_admin_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(num_admin_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$num_admin_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(num_admin_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$num_admin_Part_EduTime==0]

num_admin_Part_EduTime <- ifelse(num_admin_Part_EduTime==0 , NA, num_admin_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(num_admin_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$num_admin_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$num_admin_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt

# experience time
freq_ExpTime <- as.data.frame(prop.table(table(num_admin_Part_ExpTime)))
freq_ExpTime
ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$num_admin_Part_ExpTime==0]

num_admin_Part_ExpTime <- ifelse(num_admin_Part_ExpTime==0 , NA, num_admin_Part_ExpTime)
freq_ExpTime <- as.data.frame(prop.table(table(num_admin_Part_ExpTime)))

ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpTime$num_admin_Part_ExpTime)){
	if (freq_ExpTime$Freq[i]>=0.05) {
		ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$num_admin_Part_ExpTime[i]))],"; others < 5%")
	}
}

work_exp <- ExpTime_txt

# management experience
freq_ExpManage <- as.data.frame(prop.table(table(num_admin_Part_ExpManage)))
freq_ExpManage
ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$num_admin_Part_ExpManage==0]

num_admin_Part_ExpManage <- ifelse(num_admin_Part_ExpManage==0 , NA, num_admin_Part_ExpManage)
freq_ExpManage <- as.data.frame(prop.table(table(num_admin_Part_ExpManage)))

ExpManage_txt <- paste0(round(ExpManage_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpManage$num_admin_Part_ExpManage)){
	if (freq_ExpManage$Freq[i]>=0.05) {
		ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i]*100,2), "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$num_admin_Part_ExpManage[i]))],"; others < 5%")
	}
}

manage_exp <- ExpManage_txt	

####

# exp area

freq_ExpArea <- as.data.frame(prop.table(table(num_admin_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$num_admin_Part_ExpArea==0]

num_admin_Part_ExpArea <- ifelse(num_admin_Part_ExpArea==0 , NA, num_admin_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(num_admin_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$num_admin_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$num_admin_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(num_admin_language)))
language_txt <- ""
for (i in 1:length(freq_language$num_admin_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$num_admin_language[i]))],"; others < 5%")
	}
}

language <- language_txt


# country

freq_country <- as.data.frame(prop.table(table(num_admin_country)))
freq_country
country_na <- freq_country$Freq[freq_country$num_admin_country==0]

num_admin_country <- ifelse(num_admin_country==0 , NA, num_admin_country)

freq_country <- as.data.frame(prop.table(table(num_admin_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$num_admin_country)){
	if (freq_country$Freq[i]>=0.05) {
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$num_admin_country[i]))],"; others < 5%")
	}
}

country <- country_txt

# bio info
bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

num_admin_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\num_admin.xlsx", num_admin, num_admin_deleted, num_admin_norms, num_admin_bio)
}

############################################
# 201 views

if(length(X201_consistencyraw) > 0) {

views <- as.data.frame(cbind(Part_ID, 
				X201_consistencyraw, 
				X201_pointdistribution, 
				X201_views_01_Professional_Challenge, 
				X201_views_02_Recognition_of_Performance, 
				X201_views_03_Financial_Reward, 
				X201_views_04_Security, 
				X201_views_05_Fun_while_working, 
				X201_views_06_Identification, 
				X201_views_07_Harmony, 
				X201_views_08_Honesty, 
				X201_views_09_Cooperativeness, 
				X201_views_10_Integrity, 
				X201_views_11_Fairness, 
				X201_views_12_Hierarchy, 
				X201_views_13_Structuring, 
				X201_views_14_Rate_of_change, 
				X201_views_15_Development_Opportunities,
				X201_views_16_Absence_of_Stress,
				X201_views_17_Influence_Possibilities,
				X201_views_18_Working_Environment, 
				X201_views_testtime))
names(views)

# data clearing

# remove rows with low consistency
views[views$X201_consistencyraw > 2,]
class(views)
names(views)

# remove rows with no results for views
views <- na.omit(views)

# remove rows with low consistency
del_count <- 0
for (i in 1:length(views$Part_ID)) {
	if (views$X201_consistencyraw[i]  <= 2 ) {  # adjust consistency threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("consistency low: ", views$X201_consistencyraw[i], ", Part_ID:", views$Part_ID[i], " will be deleted from dataset"))
		views$X201_consistencyraw[i] <- NA
		#remove(views[1,])
	}
}
print(paste0(del_count, " cases with consistency <= 2 will be deleted from dataset"))
views <- na.omit(views)

# remove rows with testtime shorter than 5 or longer than 60 minutes
del_count <- 0
for (i in 1:length(views$Part_ID)) {
	if (views$X201_views_testtime[i]  < 5 || views$X201_views_testtime[i] > 60 ) {  # adjust test time thresholds in order to delete cases here
		del_count <- del_count + 1
		print(paste0("time very short or long: ", views$X201_views_testtime[i], ", Part_ID:", views$Part_ID[i], " will be deleted from dataset"))
		views$X201_views_testtime[i] <- NA
		#remove(views[1,])
	}
}
print(paste0(del_count, " case(s) with test time < 5 or > 60 will be deleted from dataset"))
views <- na.omit(views)

# remove cases with low point distribution
del_count <- 0
for (i in 1:length(views$Part_ID)) {
	if (views$X201_pointdistribution[i]  < .8 ) {  # adjust point distribution threshold in order to delete cases here
		del_count <- del_count + 1
		print(paste0("point distribution low: ", views$X201_views_testtime[i], ", Part_ID:", views$Part_ID[i], " will be deleted from dataset"))
		views$X201_views_testtime[i] <- NA
		#remove(views[1,])
	}
}
print(paste0(del_count, " case(s) with point distribution < .8 will be deleted from dataset"))
views <- na.omit(views)

names(views)

# Stanine
M201_01 <- mean(views$X201_views_01_Professional_Challenge, na.rm=TRUE)
M201_02 <- mean(views$X201_views_02_Recognition_of_Performance, na.rm=TRUE)
M201_03 <- mean(views$X201_views_03_Financial_Reward, na.rm=TRUE)
M201_04 <- mean(views$X201_views_04_Security, na.rm=TRUE)
M201_05 <- mean(views$X201_views_05_Fun_while_working, na.rm=TRUE)
M201_06 <- mean(views$X201_views_06_Identification, na.rm=TRUE)
M201_07 <- mean(views$X201_views_07_Harmony, na.rm=TRUE)
M201_08 <- mean(views$X201_views_08_Honesty)
M201_09 <- mean(views$X201_views_09_Cooperativeness, na.rm=TRUE)
M201_10 <- mean(views$X201_views_10_Integrity, na.rm=TRUE)
M201_11 <- mean(views$X201_views_11_Fairness, na.rm=TRUE)
M201_12 <- mean(views$X201_views_12_Hierarchy, na.rm=TRUE)
M201_13 <- mean(views$X201_views_13_Structuring, na.rm=TRUE)
M201_14 <- mean(views$X201_views_14_Rate_of_change, na.rm=TRUE)
M201_15 <- mean(views$X201_views_15_Development_Opportunities, na.rm=TRUE)
M201_16 <- mean(views$X201_views_16_Absence_of_Stress, na.rm=TRUE)
M201_17 <- mean(views$X201_views_17_Influence_Possibilities, na.rm=TRUE)
M201_18 <- mean(views$X201_views_18_Working_Environment, na.rm=TRUE)

SD201_01 <- sd(views$X201_views_01_Professional_Challenge, na.rm=TRUE)
SD201_02 <- sd(views$X201_views_02_Recognition_of_Performance, na.rm=TRUE)
SD201_03 <- sd(views$X201_views_03_Financial_Reward, na.rm=TRUE)
SD201_04 <- sd(views$X201_views_04_Security, na.rm=TRUE)
SD201_05 <- sd(views$X201_views_05_Fun_while_working, na.rm=TRUE)
SD201_06 <- sd(views$X201_views_06_Identification, na.rm=TRUE)
SD201_07 <- sd(views$X201_views_07_Harmony, na.rm=TRUE)
SD201_08 <- sd(views$X201_views_08_Honesty)
SD201_09 <- sd(views$X201_views_09_Cooperativeness, na.rm=TRUE)
SD201_10 <- sd(views$X201_views_10_Integrity, na.rm=TRUE)
SD201_11 <- sd(views$X201_views_11_Fairness, na.rm=TRUE)
SD201_12 <- sd(views$X201_views_12_Hierarchy, na.rm=TRUE)
SD201_13 <- sd(views$X201_views_13_Structuring, na.rm=TRUE)
SD201_14 <- sd(views$X201_views_14_Rate_of_change, na.rm=TRUE)
SD201_15 <- sd(views$X201_views_15_Development_Opportunities, na.rm=TRUE)
SD201_16 <- sd(views$X201_views_16_Absence_of_Stress, na.rm=TRUE)
SD201_17 <- sd(views$X201_views_17_Influence_Possibilities, na.rm=TRUE)
SD201_18 <- sd(views$X201_views_18_Working_Environment, na.rm=TRUE)

X201_views_01_Professional_Challenge_Stanine <- round(ifelse((views$X201_views_01_Professional_Challenge - M201_01)/SD201_01*2+5>9 , 9, ifelse((views$X201_views_01_Professional_Challenge - M201_01)/SD201_01*2+5<1, 1, (views$X201_views_01_Professional_Challenge - M201_01)/SD201_01*2+5)))
X201_views_02_Recognition_of_Performance_Stanine <- round(ifelse((views$X201_views_02_Recognition_of_Performance - M201_02)/SD201_02*2+5>9 , 9, ifelse((views$X201_views_02_Recognition_of_Performance - M201_02)/SD201_01*2+5<1, 1, (views$X201_views_02_Recognition_of_Performance - M201_02)/SD201_02*2+5)))
X201_views_03_Financial_Reward_Stanine <- round(ifelse((views$X201_views_03_Financial_Reward - M201_03)/SD201_03*2+5>9 , 9, ifelse((views$X201_views_03_Financial_Reward - M201_03)/SD201_01*2+5<1, 1, (views$X201_views_03_Financial_Reward - M201_03)/SD201_03*2+5)))
X201_views_04_Security_Stanine <- round(ifelse((views$X201_views_04_Security - M201_04)/SD201_04*2+5>9 , 9, ifelse((views$X201_views_04_Security - M201_04)/SD201_01*2+5<1, 1, (views$X201_views_04_Security - M201_04)/SD201_04*2+5)))
X201_views_05_Fun_while_working_Stanine <- round(ifelse((views$X201_views_05_Fun_while_working - M201_05)/SD201_05*2+5>9 , 9, ifelse((views$X201_views_05_Fun_while_working - M201_05)/SD201_01*2+5<1, 1, (views$X201_views_05_Fun_while_working - M201_05)/SD201_05*2+5)))
X201_views_06_Identification_Stanine <- round(ifelse((views$X201_views_06_Identification - M201_06)/SD201_06*2+5>9 , 9, ifelse((views$X201_views_06_Identification - M201_06)/SD201_01*2+5<1, 1, (views$X201_views_06_Identification - M201_06)/SD201_06*2+5)))
X201_views_07_Harmony_Stanine <- round(ifelse((views$X201_views_07_Harmony - M201_07)/SD201_07*2+5>9 , 9, ifelse((views$X201_views_07_Harmony - M201_07)/SD201_01*2+5<1, 1, (views$X201_views_07_Harmony - M201_07)/SD201_07*2+5)))
X201_views_08_Honesty_Stanine <- round(ifelse((views$X201_views_08_Honesty - M201_08)/SD201_08*2+5>9 , 9, ifelse((views$X201_views_08_Honesty - M201_08)/SD201_01*2+5<1, 1, (views$X201_views_08_Honesty - M201_08)/SD201_08*2+5)))
X201_views_09_Cooperativeness_Stanine <- round(ifelse((views$X201_views_09_Cooperativeness - M201_09)/SD201_09*2+5>9 , 9, ifelse((views$X201_views_09_Cooperativeness - M201_09)/SD201_01*2+5<1, 1, (views$X201_views_09_Cooperativeness - M201_09)/SD201_09*2+5)))
X201_views_10_Integrity_Stanine <- round(ifelse((views$X201_views_10_Integrity - M201_10)/SD201_10*2+5>9 , 9, ifelse((views$X201_views_10_Integrity - M201_10)/SD201_01*2+5<1, 1, (views$X201_views_10_Integrity - M201_10)/SD201_10*2+5)))
X201_views_11_Fairness_Stanine <- round(ifelse((views$X201_views_11_Fairness - M201_11)/SD201_11*2+5>9 , 9, ifelse((views$X201_views_11_Fairness - M201_11)/SD201_01*2+5<1, 1, (views$X201_views_11_Fairness - M201_11)/SD201_11*2+5)))
X201_views_12_Hierarchy_Stanine <- round(ifelse((views$X201_views_12_Hierarchy - M201_12)/SD201_12*2+5>9 , 9, ifelse((views$X201_views_12_Hierarchy - M201_12)/SD201_01*2+5<1, 1, (views$X201_views_12_Hierarchy - M201_12)/SD201_12*2+5)))
X201_views_13_Structuring_Stanine <- round(ifelse((views$X201_views_13_Structuring - M201_13)/SD201_13*2+5>9 , 9, ifelse((views$X201_views_13_Structuring - M201_13)/SD201_01*2+5<1, 1, (views$X201_views_13_Structuring - M201_13)/SD201_13*2+5)))
X201_views_14_Rate_of_change_Stanine <- round(ifelse((views$X201_views_14_Rate_of_change - M201_14)/SD201_14*2+5>9 , 9, ifelse((views$X201_views_14_Rate_of_change - M201_14)/SD201_01*2+5<1, 1, (views$X201_views_14_Rate_of_change - M201_14)/SD201_14*2+5)))
X201_views_15_Development_Opportunities_Stanine <- round(ifelse((views$X201_views_15_Development_Opportunities - M201_15)/SD201_15*2+5>9 , 9, ifelse((views$X201_views_15_Development_Opportunities - M201_15)/SD201_01*2+5<1, 1, (views$X201_views_15_Development_Opportunities - M201_15)/SD201_15*2+5)))
X201_views_16_Absence_of_Stress_Stanine <- round(ifelse((views$X201_views_16_Absence_of_Stress - M201_13)/SD201_13*2+5>9 , 9, ifelse((views$X201_views_16_Absence_of_Stress - M201_13)/SD201_01*2+5<1, 1, (views$X201_views_16_Absence_of_Stress - M201_13)/SD201_13*2+5)))
X201_views_17_Influence_Possibilities_Stanine <- round(ifelse((views$X201_views_17_Influence_Possibilities - M201_13)/SD201_13*2+5>9 , 9, ifelse((views$X201_views_17_Influence_Possibilities - M201_13)/SD201_01*2+5<1, 1, (views$X201_views_17_Influence_Possibilities - M201_13)/SD201_13*2+5)))
X201_views_18_Working_Environment_Stanine <- round(ifelse((views$X201_views_18_Working_Environment - M201_13)/SD201_13*2+5>9 , 9, ifelse((views$X201_views_18_Working_Environment - M201_13)/SD201_01*2+5<1, 1, (views$X201_views_18_Working_Environment - M201_13)/SD201_13*2+5)))

#class(views)

views <- cbind(views,
	X201_views_01_Professional_Challenge_Stanine,
	X201_views_02_Recognition_of_Performance_Stanine,
	X201_views_03_Financial_Reward_Stanine,
	X201_views_04_Security_Stanine,
	X201_views_05_Fun_while_working_Stanine,
	X201_views_06_Identification_Stanine,
	X201_views_07_Harmony_Stanine,
	X201_views_08_Honesty_Stanine,
	X201_views_09_Cooperativeness_Stanine,
	X201_views_10_Integrity_Stanine,
	X201_views_11_Fairness_Stanine,
	X201_views_12_Hierarchy_Stanine,
	X201_views_13_Structuring_Stanine,
	X201_views_14_Rate_of_change_Stanine,
	X201_views_15_Development_Opportunities_Stanine,
	X201_views_16_Absence_of_Stress_Stanine,
	X201_views_17_Influence_Possibilities_Stanine,
	X201_views_18_Working_Environment_Stanine,
)

colnames(views)
class(views)
views <- as.data.frame(views)
names(views)

# diagrams stanine


#freq_s01 <- as.data.frame(prop.table(table(X201_views_01_Professional_Challenge_Stanine)))

#plot(freq_s01$X201_views_01_Professional_Challenge_Stanine,freq_s01$Freq*100, col=c.blue)

barplot(prop.table(table(X201_views_01_Professional_Challenge_Stanine)), col=c.blue, xlab="Stanine Professional Challenge", ylab="percent/100")
barplot(prop.table(table(X201_views_02_Recognition_of_Performance_Stanine)), col=c.blue, xlab="Stanine Recognition_of_Performance", ylab="percent/100")
barplot(prop.table(table(X201_views_03_Financial_Reward_Stanine)), col=c.blue, xlab="Stanine Financial_Reward", ylab="percent/100")
barplot(prop.table(table(X201_views_04_Security_Stanine)), col=c.blue, xlab="Stanine Security", ylab="percent/100")
barplot(prop.table(table(X201_views_05_Fun_while_working_Stanine)), col=c.blue, xlab="Stanine Fun_while_working", ylab="percent/100")
barplot(prop.table(table(X201_views_06_Identification_Stanine)), col=c.blue, xlab="Stanine Identification", ylab="percent/100")
barplot(prop.table(table(X201_views_07_Harmony_Stanine)), col=c.blue, xlab="Stanine Harmony", ylab="percent/100")
##barplot(prop.table(table(X201_views_08_Honesty_Stanine)), col=c.blue, xlab="Stanine Honesty", ylab="percent/100")
barplot(prop.table(table(X201_views_09_Cooperativeness_Stanine)), col=c.blue, xlab="Stanine Cooperativeness", ylab="percent/100")
barplot(prop.table(table(X201_views_10_Integrity_Stanine)), col=c.blue, xlab="Stanine Integrity", ylab="percent/100")
barplot(prop.table(table(X201_views_11_Fairness_Stanine)), col=c.blue, xlab="Stanine Fairness", ylab="percent/100")
barplot(prop.table(table(X201_views_12_Hierarchy_Stanine)), col=c.blue, xlab="Stanine Hierarchy", ylab="percent/100")
barplot(prop.table(table(X201_views_13_Structuring_Stanine)), col=c.blue, xlab="Stanine Structuring", ylab="percent/100")
barplot(prop.table(table(X201_views_14_Rate_of_change_Stanine)), col=c.blue, xlab="Stanine Rate_of_change", ylab="percent/100")
##barplot(prop.table(table(X201_views_15_Development_Opportunities_Stanine)), col=c.blue, xlab="Stanine Development_Opportunities", ylab="percent/100")
barplot(prop.table(table(X201_views_16_Absence_of_Stress_Stanine)), col=c.blue, xlab="Stanine Absence_of_Stress", ylab="percent/100")
barplot(prop.table(table(X201_views_17_Influence_Possibilities_Stanine)), col=c.blue, xlab="Stanine Influence_Possibilities", ylab="percent/100")
barplot(prop.table(table(X201_views_18_Working_Environment_Stanine)), col=c.blue, xlab="Stanine Working_Environment", ylab="percent/100")


# add biodata to views basic
# gender
# BirthYear
# country
# education time
# experience time
# management experience time
# Experience area
# university degree

views_gender <- NA
views_BirthYear <- NA
views_country <- NA
views_language <- NA
views_Part_EduTime <- NA
views_Part_ExpTime <- NA
views_Part_ExpManage <- NA
views_Part_ExpArea <- NA
views_Part_University <- NA

for (i in 1:length(views$Part_ID)) {
	views_gender[i] <- x$Part_Gender[x$Part_ID==views$Part_ID[i]][1]
	views_BirthYear[i] <- x$Part_BirthYear[x$Part_ID==views$Part_ID[i]][1]
	views_country[i] <- x$Part_country[x$Part_ID==views$Part_ID[i]][1]
	views_language[i] <- x$Task_Lid[x$Part_ID==views$Part_ID[i]][1]
	views_Part_EduTime[i] <- x$Part_EduTime[x$Part_ID==views$Part_ID[i]][1]
	views_Part_ExpTime[i] <- x$Part_ExpTime[x$Part_ID==views$Part_ID[i]][1]
	views_Part_ExpManage[i] <- x$Part_ExpManage[x$Part_ID==views$Part_ID[i]][1]
	views_Part_ExpArea[i] <- x$Part_ExpArea[x$Part_ID==views$Part_ID[i]][1]
	views_Part_University[i] <- x$Part_University[x$Part_ID==views$Part_ID[i]][1]
}
views_gender
views_BirthYear
views_country
views_language
views_Part_EduTime
views_Part_ExpTime
views_Part_ExpManage
views_Part_ExpArea
views_Part_University

views_bio <- as.data.frame(cbind(views_gender,
				views_BirthYear,
				views_country,
				views_language,
				views_Part_EduTime,
				views_Part_ExpTime,
				views_Part_ExpManage,
				views_Part_ExpArea,
				views_Part_University
				))
names(views_bio)

views <- cbind(views, views_bio)
names(views)
				
### views_norms
#Inst_ID	Norm_ID	Norm_Name			Scale	Mean	sd		mSnine	Scale_Info
#201				views 				16,39	3,54			Professional Challenge
Inst_ID <- 201
Norm_ID <- ""
Norm_Name <- "views - fill in norm name here"
Scale <- as.numeric(1:18) 
Mean <- round(as.numeric(c(M201_01, M201_02, M201_03, M201_04, M201_05, M201_06, M201_07, M201_08, M201_09, M201_10, M201_11, M201_12, M201_13, M201_14, M201_15, M201_16, M201_17, M201_18)),2)
sd <- round(c(SD201_01, SD201_02, SD201_03, SD201_04, SD201_05, SD201_06, SD201_07, SD201_08, SD201_09, SD201_10, SD201_11, SD201_12, SD201_13, SD201_14, SD201_15, SD201_16, SD201_17, SD201_18),2)
mSnine <- ""
Scale_Info <- c("Professional Challenge", "Recognition of Performance", "Financial Reward", "Security", "Fun while working", "Identification", "Harmony",
"Honesty", "Cooperativeness", "Integrity", "Fairness", "Hierarchy", "Structuring", "Rate of change", "Development Opportunities", "Absence of Stress", "Influence Possibilities", "Working Environment")

views_norms <- as.data.frame(cbind(Inst_ID,Norm_ID,Norm_Name,Scale,Mean,sd,mSnine,Scale_Info))	
views_norms
names(views_norms)

# views bio
bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
"languages", "country", "education time")

### age
views_BirthYear <- ifelse(views_BirthYear==0 , NA, views_BirthYear)

age <- (current_year-views_BirthYear)

#class(age)
table(age)
freq_age <- as.data.frame(prop.table(table(age)))
freq_age

as.numeric(age_5_perc_groups <- NA)
class(age_5_perc_groups)
age_5_perc_sum <- 0

for (i in 1:length(freq_age$age)){
if (freq_age$Freq[i]>=0.05) {
	age_5_perc_groups[i] <- as.numeric(as.character(freq_age$age[i]))
	age_5_perc_sum <- age_5_perc_sum + freq_age$Freq[i]
}
}
min_5_age <- min(age_5_perc_groups, na.rm=TRUE)
max_5_age <- max(age_5_perc_groups, na.rm=TRUE)
age_5_perc_sum

#freq_age$age[17]
#age_5_perc_groups[5]

M_age <- mean(age, na.rm=TRUE)
SD_age <- sd(age, na.rm=TRUE)
range_age <- paste0(min(age, na.rm=TRUE),"-",max(age, na.rm=TRUE))
modus <- tail(sort(table(age)),1)
#modus2 <- w=table(age); w[max(w)==w] 

N=length(views$Part_ID)
age_text=paste0("M=",round(M_age,2),", SD=",round(SD_age,2),", range=",range_age,", ",round(age_5_perc_sum*100,2),"%:",min_5_age,"-",max_5_age)

# gender

#class(views_gender)
#table(views_gender)
#table(views_gender, exclude=NULL)

freq_gender <- as.data.frame(prop.table(table(views_gender)))
freq_gender

gender <- paste0(round(freq_gender$Freq[2]*100,2),"% female")

# uni degree

#freq_uni_degree_na <- as.data.frame(prop.table(table(views_Part_University, useNA="ifany")))
freq_uni_degree <- as.data.frame(prop.table(table(views_Part_University)))
freq_uni_degree
uni_degree_na <- freq_uni_degree$Freq[freq_uni_degree$views_Part_University==-1]

views_Part_University <- ifelse(views_Part_University==-1 , NA, views_Part_University)
freq_uni_degree <- as.data.frame(prop.table(table(views_Part_University)))
uni_degree_yes <- freq_uni_degree$Freq[freq_uni_degree$views_Part_University==1]

uni_degree <- paste0(round(uni_degree_na*100,2), "% n.s.; distribution of the rest: ", round(uni_degree_yes*100,2), "% with degree")

# education time
freq_EduTime <- as.data.frame(prop.table(table(views_Part_EduTime)))
freq_EduTime
EduTime_na <- freq_EduTime$Freq[freq_EduTime$views_Part_EduTime==0]

views_Part_EduTime <- ifelse(views_Part_EduTime==0 , NA, views_Part_EduTime)
freq_EduTime <- as.data.frame(prop.table(table(views_Part_EduTime)))

EduTime_txt <- paste0(round(EduTime_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_EduTime$views_Part_EduTime)){
	if (freq_EduTime$Freq[i]>=0.05) {
		EduTime_txt <- paste0(EduTime_txt, " ", round(freq_EduTime$Freq[i]*100,2), "% ", EduTime_id[as.numeric(as.character(freq_EduTime$views_Part_EduTime[i]))],"; others < 5%")
	}
}

higher_edu <- EduTime_txt
					
# experience time
				freq_ExpTime <- as.data.frame(prop.table(table(views_Part_ExpTime)))
				freq_ExpTime
				ExpTime_na <- freq_ExpTime$Freq[freq_ExpTime$views_Part_ExpTime==0]
				
				views_Part_ExpTime <- ifelse(views_Part_ExpTime==0 , NA, views_Part_ExpTime)
				freq_ExpTime <- as.data.frame(prop.table(table(views_Part_ExpTime)))
				
				ExpTime_txt <- paste0(round(ExpTime_na*100,2),"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpTime$views_Part_ExpTime)){
					if (freq_ExpTime$Freq[i]>=0.05) {
						ExpTime_txt <- paste0(ExpTime_txt, " ", round(freq_ExpTime$Freq[i]*100,2), "% ", ExpTime_id[as.numeric(as.character(freq_ExpTime$views_Part_ExpTime[i]))],";  others < 5%")
					}
				}
	
				work_exp <- ExpTime_txt
							
# management experience
				freq_ExpManage <- as.data.frame(prop.table(table(views_Part_ExpManage)))
				freq_ExpManage
				ExpManage_na <- freq_ExpManage$Freq[freq_ExpManage$views_Part_ExpManage==0]
				
				views_Part_ExpManage <- ifelse(views_Part_ExpManage==0 , NA, views_Part_ExpManage)
				freq_ExpManage <- as.data.frame(prop.table(table(views_Part_ExpManage)))
				
				ExpManage_txt <- paste0(round(ExpManage_na,4)*100,"% n.s.; distribution of the rest:")
				
				for (i in 1:length(freq_ExpManage$views_Part_ExpManage)){
					if (freq_ExpManage$Freq[i]>=0.05) {
						ExpManage_txt <- paste0(ExpManage_txt, " ", round(freq_ExpManage$Freq[i],4)*100, "% ", ExpManage_id[as.numeric(as.character(freq_ExpManage$views_Part_ExpManage[i]))],"; others < 5%")
					}
				}
				
				manage_exp <- ExpManage_txt	
				
# exp area

freq_ExpArea <- as.data.frame(prop.table(table(views_Part_ExpArea)))
freq_ExpArea
ExpArea_na <- freq_ExpArea$Freq[freq_ExpArea$views_Part_ExpArea==0]

views_Part_ExpArea <- ifelse(views_Part_ExpArea==0 , NA, views_Part_ExpArea)
freq_ExpArea <- as.data.frame(prop.table(table(views_Part_ExpArea)))
freq_ExpArea

ExpArea_txt <- paste0(round(ExpArea_na,4)*100,"% n.s.; distribution of the rest:")

for (i in 1:length(freq_ExpArea$views_Part_ExpArea)){
	if (freq_ExpArea$Freq[i]>=0.05) {
		ExpArea_txt <- paste0(ExpArea_txt, " ", round(freq_ExpArea$Freq[i]*100,2), "% ", ExpArea_id[as.numeric(as.character(freq_ExpArea$views_Part_ExpArea[i]))],"; others < 5%")
	}
}

industries <- ExpArea_txt

# language

freq_language <- as.data.frame(prop.table(table(views_language)))
language_txt <- ""
for (i in 1:length(freq_language$views_language)){
	if (freq_language$Freq[i]>=0.05) {
		language_txt <- paste0(language_txt, "", round(freq_language$Freq[i]*100,2), "% ", Lang_id[as.numeric(as.character(freq_language$views_language[i]))],"; others < 5%")
	}
}

language <- language_txt

# country

freq_country <- as.data.frame(prop.table(table(views_country)))
freq_country
country_na <- freq_country$Freq[freq_country$views_country==0]

views_country <- ifelse(views_country==0 , NA, views_country)

freq_country <- as.data.frame(prop.table(table(views_country)))
freq_country

country_txt <- paste0(round(country_na*100,2),"% n.s.; distribution of the rest:")

for (i in 1:length(freq_country$views_country)){
	if (freq_country$Freq[i]>=0.05) {
		#ExpArea_5_perc_groups[i] <- as.numeric(as.character(freq_ExpArea$views_Part_ExpArea[i]))
		country_txt <- paste0(country_txt, " ", round(freq_country$Freq[i]*100,2), "% ", country_id[as.numeric(as.character(freq_country$views_country[i]))],"; others < 5%")
		#ExpArea_5_perc_sum <- ExpArea_5_perc_sum + freq_ExpArea$Freq[i]
	}
}

country <- country_txt

# bio info

bio_info <- c(N, age_text, gender, uni_degree, higher_edu, work_exp, manage_exp, industries, language, country)

views_bio <- as.data.frame(cbind(bio_titles,bio_info))

##############################################

# save data, each test to a separate sheet
save.xlsx("_data\\views.xlsx", views, views_deleted, views_norms, views_bio)

}

