### pivot table
# pivot table: score decimal is taken as the value for score_name

##Open the Useful packages

library(readxl)

##Import the right excel and assign to x
x <-read_excel(file=paste(INSERT NAME OF THE FILE))

##Select the useful variables and make a dataframe out of it
pivot_prep <- cbind(as.numeric(x$Part_ID),as.character(x$score_name),x$score_decimal)
colnames(pivot_prep)=c("Part_ID","score_name","value")
pivot_prep <- as.data.frame(pivot_prep)

##Use attach function, so objects in the database can be accessed
##by simply giving their names.

attach(pivot_prep)

##Make the columns becoming classes
class(Part_ID)
class(x$Part_ID)
class(score_name)
class(value)

##Make the pivot table through the cast function and attach this 
pivot <- cast(pivot_prep, Part_ID ~ score_name)
detach(pivot_prep)
attach(pivot)
names(pivot)


## write/ save xlsx-files begin###############################################
#getwd() #make sure you have the correct directory

#first make function usable by executing
save.xlsx <- function (file, ...)
{
  require(xlsx, quietly = TRUE)
  objects <- list(...)
  fargs <- as.list(match.call(expand.dots = TRUE))
  objnames <- as.character(fargs)[-c(1, 2)]
  nobjects <- length(objects)
  for (i in 1:nobjects) {
    if (i == 1)
      write.xlsx(objects[[i]], file, sheetName = objnames[i], row.names=FALSE, showNA=FALSE)
    else write.xlsx(objects[[i]], file, sheetName = objnames[i], row.names=FALSE,
                    append = TRUE, showNA=FALSE)
  }
  print(paste("Workbook", file, "has", nobjects, "worksheets."))
}
#################################################################		

##biodata - create columns with title and name good to read

bio_titles <- c("N", "age", "gender", "university degree", "higher education", "work experience", "management experience", "industries",
                "languages", "country")

# education time
EduTime_id <- ""
EduTime_id[1] <- "1 year"
EduTime_id[2] <- "2 years"
EduTime_id[3] <- "3 years"
EduTime_id[4] <- "4 years"
EduTime_id[5] <- "5 years"
EduTime_id[6] <- "6 years"
EduTime_id[7] <- "7 years"
EduTime_id[8] <- "8 years"
EduTime_id[9] <- "9 years"
EduTime_id[10] <- "10 years"

# experience time
ExpTime_id <- ""
ExpTime_id[1] <- "no relevant experience"
ExpTime_id[2] <- "casual and vacation work only"
ExpTime_id[3] <- "<1 year"
ExpTime_id[4] <- "1-2 years"
ExpTime_id[5] <- "3-5 years"
ExpTime_id[6] <- "6-10 years"
ExpTime_id[7] <- ">10 years"

# management experience
ExpManage_id <- ""
ExpManage_id[1] <- "none"
ExpManage_id[2] <- "<2 years"		
ExpManage_id[3] <- "2-4 years"
ExpManage_id[4] <- "5 years or more"

# experience area
ExpArea_id <- ""
#_id[0] <- "- Please select -"
ExpArea_id[1] <- "Call Center"
ExpArea_id[2] <- "Catering"
ExpArea_id[3] <- "Construction"
ExpArea_id[4] <- "Consulting"
ExpArea_id[5] <- "Education"
ExpArea_id[6] <- "Finances, Banking & Insurance"
ExpArea_id[7] <- "Consumer goods"
ExpArea_id[8] <- "Government and Public Sector"
ExpArea_id[9] <- "Health and Social work"
ExpArea_id[10] <- "Internet/New technologies"
ExpArea_id[11] <- "Manufacturing"
ExpArea_id[12] <- "Marketing"
ExpArea_id[13] <- "Tourism"
ExpArea_id[14] <- "Retail"
ExpArea_id[15] <- "Sales"
ExpArea_id[16] <- "Science & Research"
ExpArea_id[17] <- "Telecommunication"
ExpArea_id[18] <- "Transport & Logistics"
ExpArea_id[19] <- "Other"
ExpArea_id[20] <- "Media and Advertising"
ExpArea_id[21] <- "Services"
ExpArea_id[22] <- "Oil & Gas"

# language
Lang_id <- ""
#_id[0] <- "- Please select -"
Lang_id[1] <- "German"
Lang_id[2] <- "English"
Lang_id[3] <- "French"
Lang_id[4] <- "Italian"
Lang_id[5] <- "Turkish"
Lang_id[6] <- "Norwegian"
Lang_id[7] <- "Romanian"
Lang_id[8] <- "Dutch"
Lang_id[9] <- "Spanish"
Lang_id[10] <- "Portuguese"
Lang_id[11] <- "Swedish"
Lang_id[12] <- "Japanese"
Lang_id[13] <- "Polish"
Lang_id[14] <- "Czech"
Lang_id[15] <- "Russian"
Lang_id[16] <- "Danish"
Lang_id[17] <- "Finnish"
Lang_id[18] <- "Greek"
Lang_id[19] <- "Simpl. Chinese"
Lang_id[20] <- "Trad. Chinese"
Lang_id[21] <- "Arabic"
Lang_id[22] <- "Slovenian"
Lang_id[23] <- "Hungarian"
Lang_id[24] <- "Serbian"
Lang_id[25] <- "Croatian"
Lang_id[26] <- "Catalan"
Lang_id[27] <- "US English"
Lang_id[28] <- "Portuguese (BRA)"
Lang_id[29] <- "Slovak"
Lang_id[30] <- "Hindi"
Lang_id[31] <- "Korean"
Lang_id[32] <- "Thai"
Lang_id[33] <- "Filipino"
Lang_id[34] <- "Vietnamese"
Lang_id[35] <- "Indonesian"
Lang_id[36] <- "Albanian"
Lang_id[37] <- "Latvian"
Lang_id[38] <- "Lithuanian"

# country
country_id <- ""
#country_id[0] <- "- Please select -"
country_id[1] <- "Afghanistan"
country_id[2] <- "Albania"
country_id[3] <- "Algeria"
country_id[4] <- "Andorra"
country_id[5] <- "Angola"
country_id[6] <- "Argentina"
country_id[7] <- "Armenia"
country_id[8] <- "Australia"
country_id[9] <- "Austria"
country_id[10] <- "Azerbaijan"
country_id[11] <- "Bahamas"
country_id[12] <- "Bahrain"
country_id[13] <- "Bangladesh"
country_id[14] <- "Barbados"
country_id[15] <- "Belarus"
country_id[16] <- "Belgium"
country_id[17] <- "Belize"
country_id[18] <- "Benin"
country_id[19] <- "Bermuda"
country_id[20] <- "Bhutan"
country_id[21] <- "Bolivia"
country_id[22] <- "Bosnia-Herzegovina"
country_id[23] <- "Botswana"
country_id[24] <- "Brazil"
country_id[25] <- "Bulgaria"
country_id[26] <- "Burkina Faso"
country_id[27] <- "Burundi"
country_id[28] <- "Cambodia"
country_id[29] <- "Cameroon"
country_id[30] <- "Canada"
country_id[31] <- "Cape Verde"
country_id[32] <- "Cayman Islands"
country_id[33] <- "Central African Republic"
country_id[34] <- "Chad"
country_id[35] <- "Chile"
country_id[36] <- "China"
country_id[37] <- "Colombia"
country_id[38] <- "Congo"
country_id[39] <- "Costa Rica"
country_id[40] <- "Croatia"
country_id[41] <- "Cyprus"
country_id[42] <- "Czech Republic"
country_id[43] <- "Denmark"
country_id[44] <- "Djibouti"
country_id[45] <- "Ecuador"
country_id[46] <- "Egypt"
country_id[47] <- "El Salvador"
country_id[48] <- "Equatorial Guinea"
country_id[49] <- "Eritrea"
country_id[50] <- "Estonia"
country_id[51] <- "Ethiopia"
country_id[52] <- "Fiji"
country_id[53] <- "Finland"
country_id[54] <- "France"
country_id[55] <- "France (European Territory)"
country_id[56] <- "Gabon"
country_id[57] <- "Gambia"
country_id[58] <- "Georgia"
country_id[59] <- "Germany"
country_id[60] <- "Ghana"
country_id[61] <- "Greece"
country_id[62] <- "Guam"
country_id[63] <- "Guinea"
country_id[64] <- "Guinea Bissau"
country_id[65] <- "Guyana"
country_id[66] <- "Honduras"
country_id[67] <- "Hong Kong"
country_id[68] <- "Hungary"
country_id[69] <- "Iceland"
country_id[70] <- "India"
country_id[71] <- "Indonesia"
country_id[72] <- "Iran"
country_id[73] <- "Iraq"
country_id[74] <- "Ireland"
country_id[75] <- "Israel"
country_id[76] <- "Italy"
country_id[77] <- "Republic of C�te d'Ivoire"
country_id[78] <- "Japan"
country_id[79] <- "Jordan"
country_id[80] <- "Kazakhstan"
country_id[81] <- "Kenya"
country_id[82] <- "Kuwait"
country_id[83] <- "Kyrgyzstan"
country_id[84] <- "Laos"
country_id[85] <- "Latvia"
country_id[86] <- "Lebanon"
country_id[87] <- "Lesotho"
country_id[88] <- "Liberia"
country_id[89] <- "Libya"
country_id[90] <- "Liechtenstein"
country_id[91] <- "Lithuania"
country_id[92] <- "Luxembourg"
country_id[93] <- "Macedonia"
country_id[94] <- "Madagascar"
country_id[95] <- "Malawi"
country_id[96] <- "Malaysia"
country_id[97] <- "Maldives"
country_id[98] <- "Mali"
country_id[99] <- "Malta"
country_id[100] <- "Mauritania"
country_id[101] <- "Mexico"
country_id[102] <- "Moldova"
country_id[103] <- "Monaco"
country_id[104] <- "Mongolia"
country_id[105] <- "Morocco"
country_id[106] <- "Mozambique"
country_id[107] <- "Namibia"
country_id[108] <- "Nepal"
country_id[109] <- "Netherlands"
country_id[110] <- "Netherlands Antilles"
country_id[111] <- "New Zealand"
country_id[112] <- "Nicaragua"
country_id[113] <- "Niger"
country_id[114] <- "Nigeria"
country_id[115] <- "North Korea"
country_id[116] <- "Norway"
country_id[117] <- "Oman"
country_id[118] <- "Pakistan"
country_id[119] <- "Panama"
country_id[120] <- "Paraguay"
country_id[121] <- "Peru"
country_id[122] <- "Philippines"
country_id[123] <- "Poland"
country_id[124] <- "Portugal"
country_id[126] <- "Qatar"
country_id[127] <- "Re�nion"
country_id[128] <- "Romania"
country_id[129] <- "Russian Federation"
country_id[130] <- "Rwanda"
country_id[131] <- "Saint Tome and Principe"
country_id[132] <- "Samoa"
country_id[133] <- "Saudi Arabia"
country_id[134] <- "Senegal"
country_id[135] <- "Seychelles"
country_id[136] <- "Sierra Leone"
country_id[137] <- "Singapore"
country_id[138] <- "Slovakia"
country_id[139] <- "Slovenia"
country_id[140] <- "Somalia"
country_id[141] <- "South Africa"
country_id[142] <- "South Korea"
country_id[143] <- "Spain"
country_id[144] <- "Sri Lanka"
country_id[145] <- "Sudan"
country_id[146] <- "Suriname"
country_id[147] <- "Swaziland"
country_id[148] <- "Sweden"
country_id[149] <- "Switzerland"
country_id[150] <- "Syria"
country_id[151] <- "Tajikistan"
country_id[152] <- "Taiwan"
country_id[153] <- "Tanzania"
country_id[154] <- "Thailand"
country_id[155] <- "Togo"
country_id[156] <- "Tunisia"
country_id[157] <- "Turkey"
country_id[158] <- "Turkmenistan"
country_id[159] <- "Uganda"
country_id[160] <- "UK"
country_id[161] <- "Ukraine"
country_id[162] <- "United Arab Emirates"
country_id[163] <- "Uruguay"
country_id[164] <- "US"
country_id[165] <- "Uzbekistan"
country_id[166] <- "Vatican City"
country_id[167] <- "Venezuela"
country_id[168] <- "Vietnam"
country_id[169] <- "Virgin Islands (British)"
country_id[170] <- "Yemen"
country_id[171] <- "Yugoslavia"
country_id[172] <- "Zaire"
country_id[173] <- "Zambia"
country_id[174] <- "Zimbabwe"
country_id[175] <- "Antigua and Barbuda"
country_id[176] <- "Anguilla"
country_id[177] <- "Antartica"
country_id[178] <- "American Samoa"
country_id[179] <- "Aruba"
country_id[180] <- "Brunei Darussalam"
country_id[181] <- "Bouvet Island"
country_id[182] <- "Cocos (Keeling) Islands"
country_id[183] <- "Cook Islands"
country_id[184] <- "Cuba"
country_id[185] <- "Christmas Island"
country_id[186] <- "Dominica"
country_id[187] <- "Dominican Republic"
country_id[188] <- "Western Sahara"
country_id[189] <- "Falkland Islands"
country_id[190] <- "Micronesia"
country_id[191] <- "Faroe Islands"
country_id[192] <- "Grenada"
country_id[193] <- "French Guyana"
country_id[194] <- "Gibraltar"
country_id[195] <- "Greenland"
country_id[196] <- "Guadeloupe"
country_id[197] <- "S. Georgia & S. Sandwich Islands"
country_id[198] <- "Guatemala"
country_id[199] <- "Heard and McDonald Islands"
country_id[200] <- "Haiti"
country_id[201] <- "British Indian Ocean Territory"
country_id[202] <- "Jamaica"
country_id[203] <- "Kiribati"
country_id[204] <- "Comoros"
country_id[205] <- "Saint Kitts & Nevis Anguilla"
country_id[206] <- "Saint Lucia"
country_id[207] <- "Marshall Islands"
country_id[208] <- "Macau"
country_id[209] <- "Northern Mariana Islands"
country_id[210] <- "Martinique"
country_id[211] <- "Montserrat"
country_id[212] <- "Mauritius"
country_id[213] <- "New Caledonia"
country_id[214] <- "Norfolk Island"
country_id[215] <- "Nauru"
country_id[216] <- "Neutral Zone"
country_id[217] <- "Niue"
country_id[218] <- "Polynesia (French)"
country_id[219] <- "Papua New Guinea"
country_id[220] <- "Saint Pierre and Miquelon"
country_id[221] <- "Pitcairn Island"
country_id[222] <- "Palau"
country_id[223] <- "Solomon Islands"
country_id[224] <- "Saint Helena"
country_id[225] <- "Svalbard and Jan Mayen Islands"
country_id[226] <- "Slovak Republic"
country_id[227] <- "San Marino"
country_id[228] <- "Former USSR"
country_id[229] <- "Turks and Caicos Islands"
country_id[230] <- "French Southern Territories"
country_id[231] <- "Tokelau"
country_id[232] <- "Tonga"
country_id[233] <- "East Timor"
country_id[234] <- "Trinidad and Tobago"
country_id[235] <- "Tuvalu"
country_id[236] <- "USA Minor Outlying Islands"
country_id[237] <- "Saint Vincent & Grenadines"
country_id[238] <- "Virgin Islands (USA)"
country_id[239] <- "Vanuatu"
country_id[240] <- "Wallis and Futuna Islands"
country_id[241] <- "Mayotte"
country_id[242] <- "Myanmar"

# data conversion from factors to numericals

# shapes (management)
Part_ID <- as.numeric(as.character(pivot$Part_ID))
X102_consistencyraw <- as.numeric(as.character(pivot$'102_consistencyraw'))
X102_pointdistribution <- as.numeric(as.character(pivot$'102_pointdistribution'))
X102_shapes_01_directing <- as.numeric(as.character(pivot$'102_scalesraw_1'))
X102_shapes_02_persuasive <- as.numeric(as.character(pivot$'102_scalesraw_2'))
X102_shapes_03_socially_confident <- as.numeric(as.character(pivot$'102_scalesraw_3'))
X102_shapes_04_sociable <- as.numeric(as.character(pivot$'102_scalesraw_4'))
X102_shapes_05_agreeable <- as.numeric(as.character(pivot$'102_scalesraw_5'))
X102_shapes_06_behavioral <- as.numeric(as.character(pivot$'102_scalesraw_6'))
X102_shapes_07_prudent <- as.numeric(as.character(pivot$'102_scalesraw_7'))
X102_shapes_08_focused_on_results <- as.numeric(as.character(pivot$'102_scalesraw_8'))
X102_shapes_09_systematic <- as.numeric(as.character(pivot$'102_scalesraw_9'))
X102_shapes_10_conscientious <- as.numeric(as.character(pivot$'102_scalesraw_10'))
X102_shapes_11_analytical <- as.numeric(as.character(pivot$'102_scalesraw_11'))
X102_shapes_12_conceptual <- as.numeric(as.character(pivot$'102_scalesraw_12'))
X102_shapes_13_imaginative <- as.numeric(as.character(pivot$'102_scalesraw_13'))
X102_shapes_14_open_to_change <- as.numeric(as.character(pivot$'102_scalesraw_14'))
X102_shapes_15_autonomous <- as.numeric(as.character(pivot$'102_scalesraw_15'))
X102_shapes_16_achieving <- as.numeric(as.character(pivot$'102_scalesraw_16'))
X102_shapes_17_competitive <- as.numeric(as.character(pivot$'102_scalesraw_17'))
X102_shapes_18_energetic <- as.numeric(as.character(pivot$'102_scalesraw_18'))
X102_shapes_testtime <- as.numeric(as.character(pivot$'102_testtime'))

##numerical (cons.compact)

num_accuracy <- as.numeric(as.character(pivot$'311_accuracy'))
num_overallperformance <- as.numeric(as.character(pivot$'311_overallperformance'))
num_speed <-  as.numeric(as.character(pivot$'311_speed'))

##verbal (cons.compact)

ver_accuracy <- as.numeric(as.character(pivot$'312_accuracy'))
ver_overallperformance <- as.numeric(as.character(pivot$'312_overallperformance'))
ver_speed <- as.numeric(as.character(pivot$'312_speed'))	

pivot_all <- cbind(Part_ID, 
                   X102_consistencyraw,
                   X102_pointdistribution,
                   X102_shapes_01_directing,
                   X102_shapes_02_persuasive,
                   X102_shapes_03_socially_confident,
                   X102_shapes_04_sociable,
                   X102_shapes_05_agreeable,
                   X102_shapes_06_behavioral,
                   X102_shapes_07_prudent,
                   X102_shapes_08_focused_on_results,
                   X102_shapes_09_systematic,
                   X102_shapes_10_conscientious,
                   X102_shapes_11_analytical,
                   X102_shapes_12_conceptual,
                   X102_shapes_13_imaginative,
                   X102_shapes_14_open_to_change,
                   X102_shapes_15_autonomous,
                   X102_shapes_16_achieving,
                   X102_shapes_17_competitive,
                   X102_shapes_18_energetic,
                   X102_shapes_testtime,
                   X103_consistencyraw, 
                   X103_pointdistribution, 
                   X103_shapes_01_Professional_Challenge, 
                   X103_shapes_02_Identification, 
                   X103_shapes_03_Conscientiousness, 
                   X103_shapes_04_Creativity, 
                   X103_shapes_05_Circumspection, 
                   X103_shapes_06_Fun_at_Work, 
                   X103_shapes_07_Striving_for_Harmony, 
                   X103_shapes_08_Sociable_Skills, 
                   X103_shapes_09_Cooperation, 
                   X103_shapes_10_Autonomy, 
                   X103_shapes_11_Flexibility, 
                   X103_shapes_12_Recognition, 
                   X103_shapes_13_Self_efficacy, 
                   X103_shapes_14_Perseverance, 
                   X103_shapes_15_Keenness, 
                   X103_shapes_testtime, 
                   num_overallperformance, 
                   num_speed,
                   num_accuracy,
                   verb_overallperformance, 
                   verb_speed,
                   verb_accuracy
                   
) 

colnames(pivot_all)
class(pivot_all)
pivot_all <- as.data.frame(pivot_all)
#names(pivot_all)

# save pivot table
pivot <- pivot_all
names(pivot)

save.xlsx("_data\\pivot.xlsx", pivot) ##feel free to change the name here

############################################